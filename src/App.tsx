/* eslint-disable react-hooks/exhaustive-deps */
import { CssBaseline } from "@mui/material";
import ThemeProvider from "./themes/ThemeProvider";
import AdminRoutes from "./routes/AdminRoutes";
import NotSignInRoutes from "./routes/NotSignInRoutes";
import UserRoutes from "./routes/UserRoutes";
import { checkLogin } from "./utils/helper";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { authContext } from "./context/AuthContext";
import secureLocalStorage from "react-secure-storage";

function App() {
  const navigate = useNavigate();

  const checkRole = () => {
    switch (localStorage.getItem("role")) {
      case "true":
        return <AdminRoutes />;
      case "false":
        return <UserRoutes />;
      default:
        return <NotSignInRoutes />;
    }
  };

  useEffect(() => {
    authContext(navigate);

    const handleBeforeUnload = () => {
      secureLocalStorage.clear();
      localStorage.clear();
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, []);

  return (
    <ThemeProvider>
      <CssBaseline />
      <div className="App">
        {checkLogin() ? checkRole() : <NotSignInRoutes />}
      </div>
    </ThemeProvider>
  );
}
export default App;
