// Define your API base URL
export const API_USER = "https://eposhdeploy-production.up.railway.app/api/v1";
export const API_ADMIN =
  "https://eposhbookingadminapi-production.up.railway.app/api/v1";

export const ApiUrl = {
  LOGIN: "/account/login",
  LOGOUT: "/account/logout",
  GET_ALL_ROOMS: "/rooms/getAll",
  GET_ALL_GALLERIES: "/galleries/getAll",

  // ----------------------------------- User
  REGISTER: "/account/register",
  CHANGE_PASSWORD: "/account/changPassword",
  UPDATE_NEW_PASS: "/account/updateNewPassword",
  GET_PROFILE_BY_ACCOUNT_ID: "/profile/getProfileByID",
  UPDATE_PROFILE: "/profile/updateProfileByID",
  GET_ROOM_BY_ROOM_ID: "/rooms/getByID",
  GET_BOOKING_BY_ACCOUNT_ID: "/booking/getByAccountID",

  // ----------------------------------- Admin
  GET_ALL_ACCOUNT: "/account/getAll",
  GET_ACCOUNT_BY_NAME: "/account/searchFullName",
  GET_ROOM_BY_NAME: "/rooms/searchName",
  DELETE_ROOM: "/rooms/deleteByID",
  GET_ALL_CATEGORY: "/category/getAll",
  ADD_CATEGORY: "/category/addCategory",
  DELETE_CATEGORY: "/category/deleteByID",
  DELETE_GALLERY: "/galleries/deleteByID",
  GET_ALL_BOOKING: "/booking/getAll",
};
