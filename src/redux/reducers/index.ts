import { combineReducers } from "redux";
import homeReducer, { HomeState } from "../../modules/Home/Home.Reducer";
import adminReducer, { AdminState } from "../../modules/Admin/Admin.Reducer";

const rootReducer = combineReducers({
  home: homeReducer,
  admin: adminReducer,
});

export default rootReducer;

export interface RootState {
  home: HomeState;
  admin: AdminState;
}
