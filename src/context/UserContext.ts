import { routes } from "../routes";

export const userContext = (navigate: any) => {
  switch (window.location.pathname) {
    case "/":
      navigate(routes.home.root);
      return;
    case "/rooms":
      navigate(routes.home.roomPage);
      break;
    case "/gallery":
      navigate(routes.home.galleryPage);
      break;
    case "/about-us":
      navigate(routes.home.aboutUsPage);
      break;
    case "/check-out":
      navigate(routes.home.checkoutPage);
      break;
    case "/account/profile":
      navigate(routes.home.profilePage);
      break;
    case "/account/my-booking":
      navigate(routes.home.historyBookingPage);
      break;
    case "/account/change-password":
      navigate(routes.home.changePassPage);
      break;
    default:
      navigate(routes.home.notFoundPage);
      break;
  }
};
