import { routes } from "../routes";

export const adminContext = (navigate: any) => {
  switch (window.location.pathname) {
    case "/admin":
      navigate(routes.admin.root);
      break;
    case "/admin/rooms":
      navigate(routes.admin.roomManagementPage);
      break;
    case "/admin/categories":
      navigate(routes.admin.categoryManagementPage);
      break;
    case "/admin/galleries":
      navigate(routes.admin.galleryManagementPage);
      break;
    case "/admin/booking":
      navigate(routes.admin.bookingManagementPage);
      break;
    default:
      navigate(routes.home.notFoundPage);
      break;
  }
};
