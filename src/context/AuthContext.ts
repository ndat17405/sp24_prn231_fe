import { routes } from "../routes";
import { checkLogin } from "../utils/helper";
import { adminContext } from "./AdminContext";
import { userContext } from "./UserContext";

export const authContext = (navigate: any) => {
  if (checkLogin()) {
    switch (localStorage.getItem("role")) {
      case "true":
        adminContext(navigate);
        break;
      case "false":
        userContext(navigate);
        break;
      default:
        navigate(routes.home.root);
        break;
    }
  } else {
    switch (window.location.pathname) {
      case "/admin":
        navigate(routes.home.loginPage);
        break;
      case "/login":
        navigate(routes.home.loginPage);
        break;
      case "/register":
        navigate(routes.home.registerPage);
        break;
      case "/forgot-password/step-1":
        navigate(routes.home.forgotPassStepOnePage);
        break;
      case "/forgot-password/step-2":
        navigate(routes.home.forgotPassStepTwoPage);
        break;
      case "/forgot-password/step-3":
        navigate(routes.home.forgotPassStepThreePage);
        break;
      case "/":
        navigate(routes.home.root);
        break;
      case "/rooms":
        navigate(routes.home.roomPage);
        break;
      case "/gallery":
        navigate(routes.home.galleryPage);
        break;
      case "/about-us":
        navigate(routes.home.aboutUsPage);
        break;
      default:
        navigate(routes.home.notFoundPage);
        break;
    }
  }
};
