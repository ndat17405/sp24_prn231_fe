import { Route, Routes } from "react-router-dom";
import { routes } from ".";
import { Suspense, lazy } from "react";

// Home
const HomePage = lazy(() => import("../modules/Home/Home.Container"));
const RoomPage = lazy(() => import("../modules/Home/Rooms/Room.Container"));
const GalleryPage = lazy(
  () => import("../modules/Home/Gallery/Gallery.Container")
);
const AboutUsPage = lazy(
  () => import("../modules/Home/AboutUs/AboutUs.Container")
);

// Auth
const LoginPage = lazy(() => import("../modules/Auth/SignIn/SignIn.Page"));
const RegisterPage = lazy(() => import("../modules/Auth/SignUp/SignUp.Page"));
const StepOnePage = lazy(
  () => import("../modules/Auth/ForgotPass/StepOne/StepOne.Page")
);
const StepTwoPage = lazy(
  () => import("../modules/Auth/ForgotPass/StepTwo/StepTwo.Page")
);
const StepThreePage = lazy(
  () => import("../modules/Auth/ForgotPass/StepThree/StepThree.Page")
);

// Not found
const NotFoundPage = lazy(() => import("../modules/Error/NotFound.Container"));

export default function NotSignInRoutes() {
  return (
    <Suspense
      fallback={
        <h1 style={{ width: "100%", textAlign: "center" }}>Loading...</h1>
      }
    >
      <Routes>
        {/* Home */}
        <Route path={routes.home.root} element={<HomePage />} />
        <Route path={routes.home.roomPage} element={<RoomPage />} />
        <Route path={routes.home.galleryPage} element={<GalleryPage />} />
        <Route path={routes.home.aboutUsPage} element={<AboutUsPage />} />

        {/* Authentication */}
        <Route path={routes.home.loginPage} element={<LoginPage />} />
        <Route path={routes.home.registerPage} element={<RegisterPage />} />
        <Route
          path={routes.home.forgotPassStepOnePage}
          element={<StepOnePage />}
        />

        <Route
          path={routes.home.forgotPassStepTwoPage}
          element={<StepTwoPage />}
        />

        <Route
          path={routes.home.forgotPassStepThreePage}
          element={<StepThreePage />}
        />

        {/* Not found */}
        <Route path={"*"} element={<NotFoundPage />} />
      </Routes>
    </Suspense>
  );
}
