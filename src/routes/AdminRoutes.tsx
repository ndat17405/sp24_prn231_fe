import { Route, Routes } from "react-router-dom";
import { routes } from ".";
import { Suspense, lazy } from "react";

const AccountManagement = lazy(
  () => import("../modules/Admin/AccountManagement/AccountManagement.Container")
);
const RoomManagement = lazy(
  () => import("../modules/Admin/RoomManagement/RoomManagement.Container")
);
const CategoryManagement = lazy(
  () =>
    import("../modules/Admin/CategoryManagement/CategoryManagement.Container")
);
const GalleryManagement = lazy(
  () => import("../modules/Admin/GalleryManagement/GalleryManagement.Container")
);
const BookingManagement = lazy(
  () => import("../modules/Admin/BookingManagement/BookingManagement.Container")
);

export default function AdminRoutes() {
  return (
    <Suspense
      fallback={
        <h1 style={{ width: "100%", textAlign: "center" }}>Loading...</h1>
      }
    >
      <Routes>
        <Route path={routes.admin.root} element={<AccountManagement />} />

        <Route
          path={routes.admin.roomManagementPage}
          element={<RoomManagement />}
        />

        <Route
          path={routes.admin.categoryManagementPage}
          element={<CategoryManagement />}
        />

        <Route
          path={routes.admin.galleryManagementPage}
          element={<GalleryManagement />}
        />

        <Route
          path={routes.admin.bookingManagementPage}
          element={<BookingManagement />}
        />
      </Routes>
    </Suspense>
  );
}
