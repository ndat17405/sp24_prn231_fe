export const routes = {
  home: {
    notFoundPage: "/not-found",

    // When user not logged
    root: "/",
    loginPage: "/login",
    registerPage: "/register",
    forgotPassStepOnePage: "/forgot-password/step-1",
    forgotPassStepTwoPage: "/forgot-password/step-2",
    forgotPassStepThreePage: "/forgot-password/step-3",
    roomPage: "/rooms",
    galleryPage: "/gallery",
    aboutUsPage: "/about-us",

    // When user logged successfully
    checkoutPage: "/check-out",
    profilePage: "/account/profile",
    historyBookingPage: "/account/my-booking",
    changePassPage: "/account/change-password",
  },
  admin: {
    root: "/admin",
    roomManagementPage: "/admin/rooms",
    categoryManagementPage: "/admin/categories",
    galleryManagementPage: "/admin/galleries",
    bookingManagementPage: "/admin/booking",
  },
};
