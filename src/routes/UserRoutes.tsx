import { Route, Routes } from "react-router-dom";
import { routes } from ".";
import { Suspense, lazy } from "react";

// Home
const HomePage = lazy(() => import("../modules/Home/Home.Container"));
const RoomPage = lazy(() => import("../modules/Home/Rooms/Room.Container"));
const GalleryPage = lazy(
  () => import("../modules/Home/Gallery/Gallery.Container")
);
const AboutUsPage = lazy(
  () => import("../modules/Home/AboutUs/AboutUs.Container")
);
const CheckoutPage = lazy(
  () => import("../modules/Home/isLogged/Checkout/Checkout.Page")
);
const ProfilePage = lazy(
  () => import("../modules/Home/isLogged/Profile/Profile.Container")
);
const HistoryPage = lazy(
  () =>
    import("../modules/Home/isLogged/HistoryBooking/HistoryBooking.Container")
);
const ChangePassPage = lazy(
  () => import("../modules/Home/isLogged/ChangePass/ChangePass.Container")
);

// Not found
const NotFoundPage = lazy(() => import("../modules/Error/NotFound.Container"));

export default function UserRoutes() {
  return (
    <Suspense
      fallback={
        <h1 style={{ width: "100%", textAlign: "center" }}>Loading...</h1>
      }
    >
      <Routes>
        {/* Home */}
        <Route path={routes.home.root} element={<HomePage />} />
        <Route path={routes.home.roomPage} element={<RoomPage />} />
        <Route path={routes.home.galleryPage} element={<GalleryPage />} />
        <Route path={routes.home.aboutUsPage} element={<AboutUsPage />} />
        <Route path={routes.home.checkoutPage} element={<CheckoutPage />} />
        <Route path={routes.home.profilePage} element={<ProfilePage />} />
        <Route
          path={routes.home.historyBookingPage}
          element={<HistoryPage />}
        />
        <Route path={routes.home.changePassPage} element={<ChangePassPage />} />

        {/* Not found */}
        <Route path={"*"} element={<NotFoundPage />} />
      </Routes>
    </Suspense>
  );
}
