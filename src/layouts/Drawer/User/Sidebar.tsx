import {
  Box,
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  Stack,
  Typography,
} from "@mui/material";
import { AssetImages } from "../../../utils/images";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { routes } from "../../../routes";
import { changeSidebar } from "../../../modules/Home/Home.Action";
import { useEffect } from "react";
import Logout from "../../../components/Logout/Logout.Container";
import { logout } from "../../../modules/Auth/Auth.Api";
import secureLocalStorage from "react-secure-storage";

const items = [
  {
    title: "Profile",
  },
  {
    title: "My bookings",
  },
  {
    title: "Change password",
  },
  {
    title: "Logout",
  },
];

const Sidebar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const indexSidebar = useSelector((state: any) => state.home.indexSidebar);

  const goToProfile = () => {
    navigate(routes.home.profilePage);
  };

  const goToHistoryBooking = () => {
    navigate(routes.home.historyBookingPage);
  };

  const goToChangePass = () => {
    navigate(routes.home.changePassPage);
  };

  const goToRoot = () => {
    navigate(routes.home.root);
    document.documentElement.scrollTop = 0;
  };

  const handleItemClicked = async (index: any) => {
    dispatch(changeSidebar(index));
    if (index === 0) {
      goToProfile();
    } else if (index === 1) {
      goToHistoryBooking();
    } else if (index === 2) {
      goToChangePass();
    } else if (index === 3) {
      await logout(secureLocalStorage.getItem("accountID"));
      goToRoot();
    }
  };

  useEffect(() => {}, [indexSidebar]);

  return (
    <Box
      className="sidebar--container"
      sx={{
        width: "25%",
        minHeight: "700px",
        maxHeight: "700px",
        border: "1px solid black",
        borderRadius: "8px",
        backgroundColor: themeColors.sideBar,
        boxShadow: themeColors.boxShadow,
      }}
    >
      <List disablePadding>
        <Stack
          direction="column"
          sx={{
            p: "30px",
          }}
          divider={
            <Divider
              orientation="horizontal"
              flexItem
              sx={{ backgroundColor: themeColors.divider, m: "15px 0" }}
            />
          }
        >
          {items.map((item, index) => (
            <ListItem key={item.title} disablePadding>
              <ListItemButton
                sx={{
                  minHeight: "60px",
                  p: "15px",
                  backgroundColor:
                    index === indexSidebar
                      ? themeColors.clickedItem
                      : "inherit",
                  borderRadius: "8px",
                  transition: "all ease-in .2s",
                  "&:hover": {
                    backgroundColor: themeColors.disable,
                  },
                }}
                onClick={() => handleItemClicked(index)}
              >
                {index === 0 ? (
                  <ListItemIcon
                    sx={{ minWidth: "50px", height: "50px", mr: "30px" }}
                  >
                    <img src={AssetImages.ICONS.SIDE_BAR.USER.PROFILE} alt="" />
                  </ListItemIcon>
                ) : null}
                {index === 1 ? (
                  <ListItemIcon
                    sx={{ minWidth: "50px", height: "50px", mr: "30px" }}
                  >
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.USER.HISTORY_BOOKING}
                      alt=""
                    />
                  </ListItemIcon>
                ) : null}
                {index === 2 ? (
                  <ListItemIcon
                    sx={{ minWidth: "50px", height: "50px", mr: "30px" }}
                  >
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.USER.CHANGE_PASS}
                      alt=""
                    />
                  </ListItemIcon>
                ) : null}
                {index === 3 ? (
                  <ListItemIcon
                    sx={{ minWidth: "50px", height: "50px", mr: "30px" }}
                  >
                    <Logout />
                  </ListItemIcon>
                ) : null}
                <Typography
                  sx={{
                    width: "100%",
                    fontSize: "20px",
                    color: themeColors.white,
                  }}
                >
                  {item.title}
                </Typography>
              </ListItemButton>
            </ListItem>
          ))}
        </Stack>
      </List>
    </Box>
  );
};

export default Sidebar;
