import {
  Avatar,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  Stack,
  Typography,
} from "@mui/material";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { AssetImages } from "../../../utils/images";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { routes } from "../../../routes";
import { changeSidebar } from "../../../modules/Admin/Admin.Action";

const titles = ["Accounts", "Rooms", "Categories", "Galleries", "Booking"];

const SidebarComponents = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const indexSidebar = useSelector((state: any) => state.admin.indexSidebar);

  const goToRoot = () => {
    navigate(routes.admin.root);
    document.documentElement.scrollTop = 0;
  };

  const goToRoomManagement = () => {
    navigate(routes.admin.roomManagementPage);
    document.documentElement.scrollTop = 0;
  };

  const goToCategoryManagement = () => {
    navigate(routes.admin.categoryManagementPage);
    document.documentElement.scrollTop = 0;
  };

  const goToGalleryManagement = () => {
    navigate(routes.admin.galleryManagementPage);
    document.documentElement.scrollTop = 0;
  };

  const goToBookingManagement = () => {
    navigate(routes.admin.bookingManagementPage);
    document.documentElement.scrollTop = 0;
  };

  const handleItemClicked = (index: any) => {
    dispatch(changeSidebar(index));
    if (index === 0) {
      goToRoot();
    } else if (index === 1) {
      goToRoomManagement();
    } else if (index === 2) {
      goToCategoryManagement();
    } else if (index === 3) {
      goToGalleryManagement();
    } else if (index === 4) {
      goToBookingManagement();
    }
  };

  return (
    <Drawer
      className="seller__sidebar"
      variant="permanent"
      sx={{
        width: "250px",
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: {
          width: "250px",
          boxSizing: "border-box",
          background: themeColors.adminSideBar,
          color: themeColors.white,
        },
      }}
    >
      <List disablePadding>
        <Stack direction="row" justifyContent="center">
          <Avatar
            sx={{
              width: "220px",
              height: "220px",
              mt: "20px",
            }}
            src={AssetImages.LOGO}
          />
        </Stack>
        <Stack
          direction="column"
          sx={{
            mt: "30px",
          }}
          divider={
            <Divider
              orientation="horizontal"
              flexItem
              sx={{ backgroundColor: themeColors.divider, m: "10px 0" }}
            />
          }
        >
          {titles.map((title, index) => (
            <ListItem key={title} disablePadding>
              <ListItemButton
                sx={{
                  borderRadius: 0,
                  gap: "15px",
                  backgroundColor:
                    index === indexSidebar ? themeColors.thirdary : "inherit",
                  transition: "all ease-in .2s",
                  "&:hover": {
                    backgroundColor: themeColors.thirdary,
                  },
                }}
                onClick={() => handleItemClicked(index)}
              >
                {index === 0 ? (
                  <ListItemIcon sx={{ minWidth: "45px", maxWidth: "45px" }}>
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.ADMIN.ACCOUNT}
                      alt=""
                      style={{ width: "100%" }}
                    />
                  </ListItemIcon>
                ) : null}
                {index === 1 ? (
                  <ListItemIcon sx={{ minWidth: "45px", maxWidth: "45px" }}>
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.ADMIN.ROOM}
                      alt=""
                      style={{ width: "100%" }}
                    />
                  </ListItemIcon>
                ) : null}
                {index === 2 ? (
                  <ListItemIcon sx={{ minWidth: "45px", maxWidth: "45px" }}>
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.ADMIN.CATEGORY}
                      alt=""
                      style={{ width: "100%" }}
                    />
                  </ListItemIcon>
                ) : null}
                {index === 3 ? (
                  <ListItemIcon sx={{ minWidth: "45px", maxWidth: "45px" }}>
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.ADMIN.GALLERY}
                      alt=""
                      style={{ width: "100%" }}
                    />
                  </ListItemIcon>
                ) : null}
                {index === 4 ? (
                  <ListItemIcon sx={{ minWidth: "45px", maxWidth: "45px" }}>
                    <img
                      src={AssetImages.ICONS.SIDE_BAR.ADMIN.BOOKING}
                      alt=""
                      style={{ width: "100%" }}
                    />
                  </ListItemIcon>
                ) : null}
                <Typography
                  sx={{
                    color: themeColors.white,
                    fontSize: "20px",
                  }}
                >
                  {title}
                </Typography>
              </ListItemButton>
            </ListItem>
          ))}
        </Stack>
      </List>
    </Drawer>
  );
};

export default SidebarComponents;
