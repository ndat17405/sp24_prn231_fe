import React from "react";
import "./FooterComponent.scss";
import BoxContainer from "../../components/Box/Box.Container";
import { Avatar, Box, Button, Divider, Typography } from "@mui/material";
import { AssetImages } from "../../utils/images";
import { themeColors } from "../../themes/schemes/PureLightTheme";
import { useNavigate } from "react-router-dom";
import { routes } from "../../routes";

function FooterComponent() {
  const navigate = useNavigate();

  const goToRoot = () => {
    navigate(routes.home.root);
    document.documentElement.scrollTop = 0;
  };

  const goToRoom = () => {
    navigate(routes.home.roomPage);
    document.documentElement.scrollTop = 0;
  };

  const goToGallery = () => {
    navigate(routes.home.galleryPage);
    document.documentElement.scrollTop = 0;
  };

  const goToAboutUs = () => {
    navigate(routes.home.aboutUsPage);
    document.documentElement.scrollTop = 0;
  };

  return (
    <BoxContainer property="footer--container">
      <Box className="footer__logo">
        <Divider
          sx={{
            width: "40%",
            height: "1px",
            backgroundColor: themeColors.btnSecondary,
          }}
        />
        <Avatar
          src={AssetImages.LOGO}
          alt="logo"
          sx={{ width: "150px", height: "auto" }}
        />
        <Divider
          sx={{
            width: "40%",
            height: "1px",
            backgroundColor: themeColors.btnSecondary,
          }}
        />
      </Box>

      <Box className="footer__center">
        <Box className="center__subcribe">
          <Typography className="subcribe__title">Exclusive offers</Typography>
          <Box className="subcribe__desc">
            <Typography className="desc">
              Sign up to our newsletter to receive our latest offers
            </Typography>
            <Box className="subcribes">
              <input
                type="text"
                name="email"
                id="email"
                placeholder="example@gmail.com"
              />
              <Button className="btn-subcribe">Subcribes</Button>
            </Box>
          </Box>
        </Box>

        <Box className="center__about-us">
          <Typography className="title">About us</Typography>
          <p className="description">
            EPOSH Hotel - Luxury and comfort in the center of Can Tho city.
            Classy rooms, fully equipped with modern amenities. Staff available
            24/7. Booking is easy via our convenient website. Great stay
            experience at EPOSH Hotel.
          </p>
        </Box>
      </Box>

      <Divider
        variant="middle"
        sx={{
          backgroundColor: themeColors.disable,
        }}
      />

      <Box className="footer__bottom--wrapper">
        <Box className="bottom--box">
          <Typography className="title">Contact Info</Typography>
          <Box className="contact__item">
            <img src={AssetImages.ICONS.FOOTER.PHONE} alt="phone" />
            <Typography className="info__item">
              +84 - 982 - 004 - 055
            </Typography>
          </Box>
          <Box className="contact__item">
            <img src={AssetImages.ICONS.FOOTER.PHONE} alt="phone" />
            <Typography className="info__item">
              +84 - 907 - 625 - 917
            </Typography>
          </Box>
          <Box className="contact__item">
            <img src={AssetImages.ICONS.FOOTER.MAIL} alt="mail" />
            <Typography className="info__item">eposhhotel@gmail.com</Typography>
          </Box>
          <Box className="contact__item">
            <img src={AssetImages.ICONS.FOOTER.LOCATION} alt="location" />
            <Typography className="info__item">
              EPOSH Hotel, Ninh Kieu, Can Tho
            </Typography>
          </Box>
        </Box>

        <Box className="bottom--box">
          <Typography className="title">Navigation</Typography>
          <Box className="navigation__item" onClick={goToRoot}>
            <img src={AssetImages.ICONS.FOOTER.RIGHT} alt="right" />
            <Typography className="quick-links">Home</Typography>
          </Box>
          <Box className="navigation__item" onClick={goToRoom}>
            <img src={AssetImages.ICONS.FOOTER.RIGHT} alt="right" />
            <Typography className="quick-links">Rooms</Typography>
          </Box>
          <Box className="navigation__item" onClick={goToGallery}>
            <img src={AssetImages.ICONS.FOOTER.RIGHT} alt="right" />
            <Typography className="quick-links">Gallery</Typography>
          </Box>
          <Box className="navigation__item" onClick={goToAboutUs}>
            <img src={AssetImages.ICONS.FOOTER.RIGHT} alt="right" />
            <Typography className="quick-links">About Us</Typography>
          </Box>
        </Box>

        <Box className="bottom--box">
          <Typography className="title">Social Media</Typography>
          <Box className="social-media--container">
            <Box className="social-media__qr-code">
              <img src={AssetImages.QR} alt="qr-code" />
            </Box>

            <Box className="social-media__item">
              <Box className="item__infor">
                <Box className="item__infor--link">
                  <a
                    href="https://www.facebook.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AssetImages.ICONS.FACEBOOK} alt="facebook" />
                  </a>
                  <a
                    href="https://www.instagram.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img
                      src={AssetImages.ICONS.FOOTER.INSTAGRAM}
                      alt="instagram"
                    />
                  </a>
                  <a
                    href="https://www.tiktok.com/en/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AssetImages.ICONS.FOOTER.TIKTOK} alt="tik-tok" />
                  </a>
                </Box>
                <Box className="item__infor--link">
                  <a
                    href="https://twitter.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={AssetImages.ICONS.FOOTER.TWITTER} alt="twitter" />
                  </a>
                  <a
                    href="https://www.linkedin.com/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img
                      src={AssetImages.ICONS.FOOTER.LINKEDIN}
                      alt="linkedin"
                    />
                  </a>
                  <a
                    href="https://web.telegram.org/"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img
                      src={AssetImages.ICONS.FOOTER.TELEGRAM}
                      alt="telegram"
                    />
                  </a>
                </Box>
              </Box>
              <Typography className="item__infor--hotline">
                Hotline: 0386 040 060
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </BoxContainer>
  );
}

export default FooterComponent;
