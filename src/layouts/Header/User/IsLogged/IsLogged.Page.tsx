/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Typography } from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import { AssetImages } from "../../../../utils/images";
import { routes } from "../../../../routes";
import { useNavigate } from "react-router-dom";
import "./IsLogged.Style.scss";
import { useEffect, useState } from "react";
import { getProfileByAccountId } from "../../../../modules/Home/Home.Api";
import secureLocalStorage from "react-secure-storage";

const IsLogged = () => {
  const navigate = useNavigate();
  const [data, setData] = useState<any>([]);

  const goToRoot = () => {
    navigate(routes.home.root);
  };

  const goToRoom = () => {
    navigate(routes.home.roomPage);
  };

  const goToGallery = () => {
    navigate(routes.home.galleryPage);
  };

  const goToAboutUs = () => {
    navigate(routes.home.aboutUsPage);
  };

  const navToProfile = () => {
    navigate(routes.home.profilePage);
  };

  const init = async () => {
    const res = await getProfileByAccountId(
      secureLocalStorage.getItem("accountID")
    );
    if (res) {
      setData(res);
    }
  };

  useEffect(() => {
    init();
  }, [data]);

  return (
    <BoxContainer property="header--is-logged">
      <Box className="header__logo">
        <img src={AssetImages.LOGO} alt="logo" />
      </Box>

      <Box className="header__nav">
        <Typography className="nav__item" onClick={goToRoot}>
          Home
        </Typography>
        <Typography className="nav__item" onClick={goToRoom}>
          Rooms
        </Typography>
        <Typography className="nav__item" onClick={goToGallery}>
          Gallery
        </Typography>
        <Typography className="nav__item" onClick={goToAboutUs}>
          About Us
        </Typography>
      </Box>

      <Box className="header__btn" onClick={navToProfile}>
        <Box className="btn--user">
          <img src={AssetImages.ICONS.USER} alt="" />
          <Typography className="name-user">
            {data?.profile?.fullName}
          </Typography>
        </Box>
      </Box>
    </BoxContainer>
  );
};

export default IsLogged;
