import IsLogged from "./IsLogged.Page";

const IsLoggedContainer = () => {
  return <IsLogged />;
};

export default IsLoggedContainer;
