import NotLogged from "./NotLogged.Page";

const NotLoggedContainer = () => {
  return <NotLogged />;
};

export default NotLoggedContainer;
