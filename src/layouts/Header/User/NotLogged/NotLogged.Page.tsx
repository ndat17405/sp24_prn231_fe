import "./NotLogged.Style.scss";
import { Box, Button, Typography } from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import { AssetImages } from "../../../../utils/images";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../../routes";

export default function NotLogged() {
  const navigate = useNavigate();

  const goToRoot = () => {
    navigate(routes.home.root);
  };

  const goToRoom = () => {
    navigate(routes.home.roomPage);
  };

  const goToGallery = () => {
    navigate(routes.home.galleryPage);
  };

  const goToAboutUs = () => {
    navigate(routes.home.aboutUsPage);
  };

  const goToLogin = () => {
    navigate(routes.home.loginPage);
  };

  const goToRegister = () => {
    navigate(routes.home.registerPage);
  };

  return (
    <BoxContainer property="header">
      <Box className="header__logo">
        <img src={AssetImages.LOGO} alt="logo" />
      </Box>

      <Box className="header__nav">
        <Typography className="nav__item" onClick={goToRoot}>
          Home
        </Typography>
        <Typography className="nav__item" onClick={goToRoom}>
          Rooms
        </Typography>
        <Typography className="nav__item" onClick={goToGallery}>
          Gallery
        </Typography>
        <Typography className="nav__item" onClick={goToAboutUs}>
          About Us
        </Typography>
      </Box>

      <Box className="header__btn">
        <Button className="btn--login" onClick={goToLogin}>
          Login
        </Button>

        <Button className="btn-register" onClick={goToRegister}>
          Register
        </Button>
      </Box>
    </BoxContainer>
  );
}
