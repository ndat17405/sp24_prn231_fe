import "./Header.Admin.Style.scss";
import { Avatar, Box, Typography } from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import Logout from "../../../components/Logout/Logout.Container";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../routes";
import { logout } from "../../../modules/Auth/Auth.Api";
import secureLocalStorage from "react-secure-storage";

export default function Header() {
  const navigate = useNavigate();

  const goToLogin = async () => {
    await logout(secureLocalStorage.getItem("accountID"));
    navigate(routes.home.loginPage);
  };

  return (
    <BoxContainer property="header--admin">
      <Typography className="header__title">Admin Panel</Typography>

      <Box className="header__exit">
        <Avatar
          sx={{
            width: "70px",
            height: "70px",
            background: themeColors.adminSecondary,
            fontSize: "26px",
            fontWeight: "700",
            border: `1px solid ${themeColors.black}`,
          }}
        >
          A
        </Avatar>
        <Box className="header--btn" onClick={goToLogin}>
          <Typography
            sx={{
              color: themeColors.white,
              fontSize: "28px",
              fontWeight: "500",
            }}
          >
            Logout
          </Typography>
          <Logout />
        </Box>
      </Box>
    </BoxContainer>
  );
}
