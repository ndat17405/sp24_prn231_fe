import { routes } from "../routes";

export const capitalizeString = (str: string): string => {
  if (!str) return "";
  return str.charAt(0).toUpperCase() + str.slice(1);
};

export const formatNumber = (num: number): string => {
  if (!num) return "0";
  return num.toLocaleString();
};

export const formatDate = (date: Date): string => {
  if (!date) return "";
  return date.toLocaleDateString();
};

export const checkLogin = () => {
  const token = localStorage.getItem("token");
  if (token) {
    return true;
  } else {
    return false;
  }
};

export const checkPermission = (navigate: any) => {
  switch (localStorage.getItem("role")) {
    case "true":
      navigate(routes.admin.root);
      break;
    case "false":
      navigate(routes.home.root);
      break;
    default:
      navigate(routes.home.root);
      break;
  }
};

// Filter list room
// 1. Is empty
export const filterRoomIsEmptyList = (listRoom: any) => {
  let listEmpty: any = [];
  listRoom?.forEach((element: any) => {
    if (element.status === true) {
      listEmpty.push(element);
    }
  });
  if (listEmpty) {
    return listEmpty;
  } else {
    return false;
  }
};

// 2. Is out of room
export const filterRoomIsNotEmpty = (listRoom: any) => {
  let listNotEmpty: any = [];
  listRoom?.forEach((element: any) => {
    if (element.status === false) {
      listNotEmpty.push(element);
    }
  });
  return listNotEmpty;
};

// Filter list galleries
// 1. Hotel View
export const filterImageIsHotelView = (listImage: any) => {
  let listHotelView: any = [];
  listImage?.forEach((element: any) => {
    if (element.title === "Hotel View") {
      listHotelView.push(element);
    }
  });
  return listHotelView;
};

// 2. Guest Room
export const filterImageIsGuestRoom = (listImage: any) => {
  let listGuestRoom: any = [];
  listImage?.forEach((element: any) => {
    if (element.title === "Rooms") {
      listGuestRoom.push(element);
    }
  });
  return listGuestRoom;
};

// 3. Spa
export const filterImageIsSpa = (listImage: any) => {
  let listSpa: any = [];
  listImage?.forEach((element: any) => {
    if (element.title === "Spa") {
      listSpa.push(element);
    }
  });
  return listSpa;
};

// 4. Dining
export const filterImageIsDining = (listImage: any) => {
  let listDining: any = [];
  listImage?.forEach((element: any) => {
    if (element.title === "Dining") {
      listDining.push(element);
    }
  });
  return listDining;
};

// 5. Wedding
export const filterImageIsWedding = (listImage: any) => {
  let listWedding: any = [];
  listImage?.forEach((element: any) => {
    if (element.title === "Weddings") {
      listWedding.push(element);
    }
  });
  return listWedding;
};

// Filter booking
// 1. Peding
export const filterBookingIsWaitting = (listBooking: any) => {
  let listWaiting: any = [];
  listBooking?.forEach((element: any) => {
    if (element.status === false) {
      listWaiting.push(element);
    }
  });
  return listWaiting;
};

// 2. Completed
export const filterBookingIsCompleted = (listBooking: any) => {
  let listCompleted: any = [];
  listBooking?.forEach((element: any) => {
    if (element.status === true) {
      listCompleted.push(element);
    }
  });
  return listCompleted;
};

// Filter category
// 1. Size small
export const filterCategoryWithSizeSmall = (listCategory: any) => {
  let listSizeSmall: any = [];
  listCategory?.forEach((element: any) => {
    if (element.sizeOfRooms === "Small") {
      listSizeSmall.push(element);
    }
  });
  return listSizeSmall;
};

// 2. Size medium
export const filterCategoryWithSizeMedium = (listCategory: any) => {
  let listSizeMedium: any = [];
  listCategory?.forEach((element: any) => {
    if (element.sizeOfRooms === "Medium") {
      listSizeMedium.push(element);
    }
  });
  return listSizeMedium;
};

// 3. Size large
export const filterCategoryWithSizeLarge = (listCategory: any) => {
  let listSizeLarge: any = [];
  listCategory?.forEach((element: any) => {
    if (element.sizeOfRooms === "Large") {
      listSizeLarge.push(element);
    }
  });
  return listSizeLarge;
};

const Helper = {
  capitalizeString,
  formatNumber,
  formatDate,
};

export default Helper;
