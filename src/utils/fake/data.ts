import { AssetImages } from "../images";

export const customerReviews = [
  {
    id: 1,
    name: "Tran Quoc Bao",
    date: "08/11/2023",
    subject: "Excellent Service by all staff",
    content:
      "Located in the heart of Saigon, the hotel offers to the costumers the possibilities to explore the city. The staff was very opened to help and to offer assistance if needed. The rooms are clean, large and with all the facilities. The politic of the hotel is to empty the minibar for the costumers safety, but if want to consume something a nice bar is located at the first floor. At the breakfast the offer is very wide with local specialities but also european/international ones - like in all inclusive systems.",
  },
  {
    id: 2,
    name: "Doan Minh Hieu",
    date: "23/11/2023",
    subject: "Great experiences",
    content:
      "Memorable experience at EPOSH hotel! Large, luxurious room, friendly staff. The restaurant serves excellent cuisine, the spa is of high quality. Discover a warm atmosphere and excellent service. Will definitely come back!",
  },
  {
    id: 3,
    name: "Nguyen Tien Dat",
    date: "21/12/2023",
    subject: "Great Experience at Quality Hotels",
    content:
      "My trip to this hotel was a memorable experience. The rooms are comfortable, the staff is helpful, and the restaurant serves excellent cuisine. Professional room service and luxurious space create a wonderful stay. I will definitely come back!",
  },
  {
    id: 4,
    name: "Le Phuoc Duy",
    date: "08/01/2023",
    subject: "Unique and Wonderful Experience at a Quality Hotel",
    content:
      "This hotel was truly an outstanding experience! Rooms are spacious and comfortable, staff are friendly and service is prompt. The restaurant has great cuisine and a luxurious space. Amenities like spa and swimming pool are big plus points. Will definitely come back next time!",
  },
  {
    id: 5,
    name: "Nguyen Hoang Qui",
    date: "01/02/2023",
    subject: "Outstanding Hotel Experience",
    content:
      "Our trip to this hotel left a deep and happy impression. From the moment I stepped foot to the reception, everything was absolutely perfect. The rooms are spacious, clean and fully equipped, giving us a comfortable feeling at home. The staff is always smiling and ready to help, making the resting space cozy. The hotel's restaurant serves delicious, diverse and quality dishes. We had a wonderful culinary experience every day and were very satisfied. The spa services here are truly excellent. Professional staff and top-notch relaxation treatments. We had wonderful moments of relaxation after exciting days of visiting. Overall, this hotel is an ideal destination for those looking for comfort, quality and dedicated service. We are very happy with our experience and will definitely come back when the opportunity arises. Thank you hotel team for creating a wonderful vacation for us!",
  },
  {
    id: 6,
    name: "Nguyen Thanh Phong",
    date: "20/02/2023",
    subject: "Wonderful Stay at this Outstanding Hotel!",
    content:
      "I had a wonderful stay at this hotel and couldn't be more pleased with everything. The rooms are very comfortable and clean, with elegant space and full amenities. The staff here are truly excellent, extremely enthusiastic and thoughtful. They are always willing to help and create a friendly and professional atmosphere. The hotel's restaurant offers a unique culinary experience. Every meal is a flavorful adventure, from a dedicated and creative chef. The spa service here is a special feature. I completely relaxed in a quiet space and was served by a professional care team. With a convenient location and luxurious space, this hotel is truly the perfect choice for any trip. I will definitely come back and recommend to friends and family. Thank you hotel team for creating this wonderful stay!",
  },
  {
    id: 7,
    name: "Nguyen Van Hieu",
    date: "11/03/2023",
    subject: "Great Vacation at Ideal Hotel",
    content:
      "The trip to this hotel was truly a wonderful experience from start to finish! Rooms are comfortable and clean, with great views of the city. The staff is friendly and attentive, always making us feel welcomed like family. Quality room service, with little touches like sophisticated nightlights and stylish breakfasts. The hotel's restaurant serves excellent cuisine, from local to international dishes, all enjoyed in a luxurious atmosphere. The swimming pool and gym are both very good, providing entertainment and relaxation experiences for all family members. In particular, we enjoyed the hotel's shuttle service and tour booking support, helping us easily discover our ideal destination. Overall feeling, this is an ideal destination for a vacation. The hotel is not just a place to stay but also a memorable experience with excellent service and amenities. Will definitely recommend to friends and come back again!",
  },
];

export const reason = [
  "Price could not be better",
  "Book securely",
  "Manage reservations online",
  "Great amenities and location",
  "Staff speaks Vietnamese & English",
];

export const leftService = [
  {
    id: 1,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.SWIMMING_POOL,
    name: "Swimming Pool",
  },
  {
    id: 2,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.WIFI,
    name: "Wifi",
  },
  {
    id: 3,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.RESTAURANT,
    name: "Restaurant",
  },
  {
    id: 4,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.BAR,
    name: "Bar",
  },
];

export const rightService = [
  {
    id: 1,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.CAR_PARK,
    name: "Car park",
  },
  {
    id: 2,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.ACCESSIBILITY,
    name: "Wheelchair accessible hotel",
  },
  {
    id: 3,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.AIR_CONDITION,
    name: "Air conditioning",
  },
  {
    id: 4,
    img: AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.BREAKFAST,
    name: "Breakfast",
  },
];

export const detailNav = ["Description", "Services", "Reviews"];

export const serviceNav = [
  "Restaurant",
  "Bars",
  "Breakfast",
  "Pools",
  "Fitness Center",
  "Spa",
];

export const ratingNav = [
  "All Profiles",
  "Couples",
  "Families",
  "Solo Travelers",
  "Business Travelers",
];

export const ratingDataAllProfile = [
  {
    leftData: [
      {
        id: 1,
        name: "Location",
        reviews: 304,
        percent: 92,
      },
      {
        id: 2,
        name: "Service",
        reviews: 782,
        percent: 93,
      },
      {
        id: 3,
        name: "Breakfast",
        reviews: 281,
        percent: 93,
      },
      {
        id: 4,
        name: "Amenities",
        reviews: 234,
        percent: 80,
      },
    ],
  },
  {
    rightData: [
      {
        id: 1,
        name: "Room",
        reviews: 501,
        percent: 77,
      },
      {
        id: 2,
        name: "Wifi",
        reviews: 12,
        percent: 79,
      },
      {
        id: 3,
        name: "Food",
        reviews: 271,
        percent: 86,
      },
      {
        id: 4,
        name: "Cleanliness",
        reviews: 142,
        percent: 82,
      },
    ],
  },
];

export const ratingDataCouples = [
  {
    leftData: [
      {
        id: 1,
        name: "Location",
        reviews: 121,
        percent: 16,
      },
      {
        id: 2,
        name: "Service",
        reviews: 567,
        percent: 68,
      },
      {
        id: 3,
        name: "Breakfast",
        reviews: 105,
        percent: 16,
      },
      {
        id: 4,
        name: "Amenities",
        reviews: 144,
        percent: 32,
      },
    ],
  },
  {
    rightData: [
      {
        id: 1,
        name: "Room",
        reviews: 292,
        percent: 17,
      },
      {
        id: 2,
        name: "Wifi",
        reviews: 1,
        percent: 50,
      },
      {
        id: 3,
        name: "Food",
        reviews: 222,
        percent: 74,
      },
      {
        id: 4,
        name: "Cleanliness",
        reviews: 48,
        percent: 79,
      },
    ],
  },
];

export const ratingDataFamily = [
  {
    leftData: [
      {
        id: 1,
        name: "Location",
        reviews: 252,
        percent: 40,
      },
      {
        id: 2,
        name: "Service",
        reviews: 214,
        percent: 58,
      },
      {
        id: 3,
        name: "Breakfast",
        reviews: 190,
        percent: 55,
      },
      {
        id: 4,
        name: "Amenities",
        reviews: 118,
        percent: 62,
      },
    ],
  },
  {
    rightData: [
      {
        id: 1,
        name: "Room",
        reviews: 409,
        percent: 69,
      },
      {
        id: 2,
        name: "Wifi",
        reviews: 5,
        percent: 35,
      },
      {
        id: 3,
        name: "Food",
        reviews: 162,
        percent: 66,
      },
      {
        id: 4,
        name: "Cleanliness",
        reviews: 62,
        percent: 90,
      },
    ],
  },
];

export const ratingDataSolo = [
  {
    leftData: [
      {
        id: 1,
        name: "Location",
        reviews: 119,
        percent: 24,
      },
      {
        id: 2,
        name: "Service",
        reviews: 364,
        percent: 28,
      },
      {
        id: 3,
        name: "Breakfast",
        reviews: 231,
        percent: 55,
      },
      {
        id: 4,
        name: "Amenities",
        reviews: 85,
        percent: 92,
      },
    ],
  },
  {
    rightData: [
      {
        id: 1,
        name: "Room",
        reviews: 498,
        percent: 77,
      },
      {
        id: 2,
        name: "Wifi",
        reviews: 10,
        percent: 35,
      },
      {
        id: 3,
        name: "Food",
        reviews: 157,
        percent: 92,
      },
      {
        id: 4,
        name: "Cleanliness",
        reviews: 142,
        percent: 31,
      },
    ],
  },
];

export const ratingDataBusiness = [
  {
    leftData: [
      {
        id: 1,
        name: "Location",
        reviews: 265,
        percent: 38,
      },
      {
        id: 2,
        name: "Service",
        reviews: 585,
        percent: 58,
      },
      {
        id: 3,
        name: "Breakfast",
        reviews: 116,
        percent: 57,
      },
      {
        id: 4,
        name: "Amenities",
        reviews: 209,
        percent: 80,
      },
    ],
  },
  {
    rightData: [
      {
        id: 1,
        name: "Room",
        reviews: 240,
        percent: 60,
      },
      {
        id: 2,
        name: "Wifi",
        reviews: 11,
        percent: 46,
      },
      {
        id: 3,
        name: "Food",
        reviews: 252,
        percent: 72,
      },
      {
        id: 4,
        name: "Cleanliness",
        reviews: 79,
        percent: 54,
      },
    ],
  },
];

export const serviceDataRestaurant = [
  {
    id: 1,
    img: AssetImages.SERVICE.RESTAURANT,
    title: "Hotel Dining",
    description:
      "In the center of our hotel, experience exquisite dining at our restaurant. Indulge in culinary delights crafted with passion and finesse. Enjoy a fusion of flavors amidst elegant surroundings. Elevate your stay with memorable dining experiences.",
  },
  {
    id: 2,
    img: AssetImages.SERVICE.WEDDING,
    title: "Hotel Weddings",
    description:
      "Transform your special day into an unforgettable celebration with our exquisite wedding restaurant nestled within our hotel. Our elegant venue and exceptional service ensure that your wedding is nothing short of magical. Experience the perfect blend of luxury and romance as we cater to your every need, creating memories to last a lifetime.",
  },
];

export const serviceDataBars = [
  {
    id: 1,
    img: AssetImages.SERVICE.INNER_HOTEL_BAR,
    title: "Hotel Bar",
    description:
      "Unwind and indulge at the bar in our hotel, where every sip tells a story of relaxation and enjoyment. Our inviting ambiance, crafted cocktails, and attentive staff ensure a memorable experience for every guest. Whether you're unwinding after a long day or celebrating with friends, our bar offers the perfect setting to create unforgettable moments. Join us and raise a glass to good times.",
  },
  {
    id: 2,
    img: AssetImages.SERVICE.ROOF_TOP_BAR,
    title: "Sky Lounge",
    description:
      "Elevate your experience at the rooftop bar in our hotel, where panoramic views and crafted cocktails await. Perched above the city, our Sky Lounge offers a captivating setting for relaxation and socializing. Savor handcrafted drinks and delectable bites as you soak in the breathtaking skyline.",
  },
];

export const serviceDataBreakfast = [
  {
    id: 1,
    img: AssetImages.SERVICE.BREAKFAST_1,
    title: "Morning Delight",
    description:
      "Wake up to a delightful breakfast experience at our hotel, where flavors come to life with every bite. Our breakfast menu is thoughtfully curated to satisfy every palate, offering a variety of options from traditional favorites to healthy alternatives. Enjoy the perfect start to your day at our inviting breakfast venue.",
  },
  {
    id: 2,
    img: AssetImages.SERVICE.BREAKFAST_2,
    title: "Sunrise Feasts",
    description:
      "Start your day with a burst of flavor at our hotel's breakfast venue. Delight in an array of morning delights, from savory to sweet, crafted to tantalize your taste buds. With fresh ingredients and attentive service, our breakfast experience promises to elevate your morning routine. Join us for a sunrise feast that sets the tone for a day of possibilities.",
  },
];

export const serviceDataPool = [
  {
    id: 1,
    img: AssetImages.SERVICE.POOL_1,
    title: "Sky Pool",
    description:
      "Experience luxury elevated at the rooftop swimming pool in our hotel. Perched high above the city, our Sky Pool offers breathtaking views and a serene atmosphere for relaxation. Dive into tranquility or bask in the sun while enjoying unparalleled vistas. Join us for an unforgettable swimming experience amidst the clouds.",
  },
  {
    id: 2,
    img: AssetImages.SERVICE.POOL_2,
    title: "Pool Oasis",
    description:
      "Dive into relaxation at the swimming pool in our hotel, where serenity meets luxury. Our tranquil oasis offers a refreshing escape from the bustle of the city, providing a perfect setting for rejuvenation and enjoyment. Whether you seek a leisurely swim or simply wish to soak up the sun, our pool awaits to offer you a blissful retreat.",
  },
];

export const serviceDataFitnessCenter = [
  {
    id: 1,
    img: AssetImages.SERVICE.FITNESS_CENTER_1,
    title: "Gym Haven",
    description:
      "Elevate your fitness journey at the fitness center in our hotel. Our state-of-the-art facilities offer everything you need for a rewarding workout experience. From cutting-edge equipment to personalized training options, we're committed to helping you achieve your wellness goals. Join us and embark on a path to vitality and strength at our hotel's fitness center.",
  },
  {
    id: 2,
    img: AssetImages.SERVICE.FITNESS_CENTER_2,
    title: "Fit Oasis",
    description:
      "Recharge your body and mind at the fitness center within our hotel. Our modern facilities provide a sanctuary for your workout routine, equipped with top-of-the-line amenities and expert guidance. Whether you prefer cardio, strength training, or group classes, we offer diverse options to suit your needs.",
  },
];

export const serviceDataSpa = [
  {
    id: 1,
    img: AssetImages.SERVICE.SPA_1,
    title: "Luxury Spa",
    description:
      "Indulge in opulence at the luxury hotel spa of our establishment. Our sanctuary offers a haven of relaxation and rejuvenation, where skilled therapists provide tailored treatments to pamper your body and soothe your soul. Immerse yourself in tranquility as you escape the stresses of everyday life.",
  },
  {
    id: 2,
    img: AssetImages.SERVICE.SPA_2,
    title: "Opulent Spa",
    description:
      "Step into a world of indulgence at the opulent spa of our luxury hotel. With a blend of refined elegance and serene ambiance, our spa beckons you to unwind and replenish your senses. From rejuvenating massages to revitalizing facials, our expert therapists are dedicated to providing an unforgettable experience tailored to your needs.",
  },
];

export const optionsData = [
  {
    id: 1,
    img: AssetImages.OPTION_BREAKFAST,
    title: "Breakfast",
    detail: "Per adult/stay",
    price: 15.27,
  },
  {
    id: 2,
    img: AssetImages.OPTION_TRANSFER,
    title: "Private transfer service from the airport to the hotel",
    detail: "Each room",
    price: 34.22,
  },
  {
    id: 3,
    img: AssetImages.OPTION_CHAMPAGNE,
    title: "Champagne",
    detail: "Each room",
    price: 15.27,
  },
  {
    id: 4,
    img: AssetImages.OPTION_DINNER,
    title: "Dinner",
    detail: "Per adult/stay",
    price: 15.27,
  },
];
