export const formatMoneyToNumber = (money: any) => {
  let newNum = money.split(".").join("");
  return newNum;
};

export const formatNumberToMoney = (num: any) => {
  if (num !== null) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }
  return "";
};
