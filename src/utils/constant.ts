export const AssetContants = {
    LINK_FB: "https://www.facebook.com/",
    LINK_IG: "https://www.instagram.com/",
    LINK_TW: "https://twitter.com/?lang=en",
    LINK_IN: "https://www.linkedin.com/feed/",
    LINK_TOPTOP: "https://www.tiktok.com/en/",
    LINK_TELE: "https://web.telegram.org/a/"
};