import { CSSProperties } from "react";

export interface MyCustomCSS extends CSSProperties {
  "--percent": any;
}
