/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import SidebarComponents from "../../../layouts/Drawer/Admin/Sidebar.Components";
import "./CategoryManagement.Style.scss";
import Header from "../../../layouts/Header/Admin/Header.Admin";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { useEffect, useState } from "react";
import { EditOutlined, DeleteOutlined } from "@mui/icons-material";
import { AssetImages } from "../../../utils/images";
import AddCategoryModal from "../Modal/Category/AddCategory/AddCategory.Modal";
import UpdateCategoryModal from "../Modal/Category/UpdateCategory/UpdateCategory.Modal";
import DeleteCategoryModal from "../Modal/Category/DeleteCategory/DeleteCategory.Modal";
import { getAllCategory } from "../Admin.Api";
import {
  filterCategoryWithSizeLarge,
  filterCategoryWithSizeMedium,
  filterCategoryWithSizeSmall,
} from "../../../utils/helper";
import { LoadingButton } from "@mui/lab";

const valueSize = ["Small", "Medium", "Large"];

const CategoryManagementPage = () => {
  const [open, setOpen] = useState(false);
  const [data, setData] = useState([]);
  const [size, setSize] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  const categoryPerPage = 6;
  const indexOfLastCategory = currentPage * categoryPerPage;
  const indexOfFirstCategory = indexOfLastCategory - categoryPerPage;
  const currentCategory = data.slice(indexOfFirstCategory, indexOfLastCategory);
  const totalPageCategory = Math.ceil(data?.length / categoryPerPage);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleSizeChange = (e: any) => {
    setSize(e.target.value);
  };

  const init = async () => {
    const res = await getAllCategory();
    if (res) {
      switch (size) {
        case "Small":
          setData(filterCategoryWithSizeSmall(res));
          break;
        case "Medium":
          setData(filterCategoryWithSizeMedium(res));
          break;
        case "Large":
          setData(filterCategoryWithSizeLarge(res));
          break;
        default:
          setData(res);
          break;
      }
    }
  };

  const paginate = (event: any, value: any) => {
    event.preventDefault();
    setCurrentPage(value);
    document.documentElement.scrollTop = 200;
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [open, currentCategory]);

  return (
    <BoxContainer property="category-management--container">
      <SidebarComponents />
      <Box className="content--container">
        <Header />
        <BoxContainer property="content--wrapper">
          <Typography className="content__title--category">
            Category Management
          </Typography>
          <Divider sx={{ backgroundColor: "#BBBBBF", m: "30px 0" }} />
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <FormControl sx={{ minWidth: "30%", ml: "30px" }}>
              <Select
                value={size}
                onChange={handleSizeChange}
                displayEmpty
                size="small"
                sx={{ borderRadius: "8px" }}
              >
                <MenuItem value="">
                  <em style={{ color: "#B2B2B2" }}>
                    -- Choose size for filter --
                  </em>
                </MenuItem>
                {valueSize?.map((item: any) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <AddCategoryModal>
              <Box
                onClick={handleOpen}
                sx={{
                  backgroundColor: themeColors.btnAdd,
                  borderRadius: "12px",
                  display: "flex",
                  alignItems: "center",
                  gap: "20px",
                  "&:hover": {
                    opacity: ".8",
                    cursor: "pointer",
                  },
                }}
              >
                <img
                  src={AssetImages.ICONS.CRUD.ADD}
                  alt=""
                  style={{ width: "30px", height: "30px" }}
                />
                Add Category
              </Box>
            </AddCategoryModal>
          </Box>
          <Typography className="content__number--category">
            {data?.length > 1
              ? `${data?.length} Categories`
              : `${data?.length} Category`}
          </Typography>
          <Box className="table--category">
            <TableContainer
              component={Paper}
              sx={{
                m: "0 30px 30px",
                width: "auto",
                boxShadow: "none",
                border: "none",
                borderRadius: 0,
              }}
            >
              <Table>
                <TableHead>
                  <TableRow
                    sx={{
                      background: themeColors.white,
                    }}
                  >
                    <TableCell
                      align="center"
                      className="table__title--category"
                    >
                      Size of room
                    </TableCell>
                    <TableCell
                      align="center"
                      className="table__title--category"
                    >
                      Type of bed
                    </TableCell>
                    <TableCell
                      align="center"
                      className="table__title--category"
                    >
                      Maximum Capacity
                    </TableCell>
                    <TableCell
                      align="center"
                      className="table__title--category"
                    >
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {currentCategory?.map((data: any) => (
                    <TableRow
                      key={data.categoriesID}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell align="center" sx={{ fontSize: "16px" }}>
                        {data?.sizeOfRooms}
                      </TableCell>
                      <TableCell align="center" sx={{ fontSize: "16px" }}>
                        {data?.typeOfBed}
                      </TableCell>
                      <TableCell align="center" sx={{ fontSize: "16px" }}>
                        {data?.numberOfMember}
                      </TableCell>
                      <TableCell>
                        <Box
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-evenly",
                          }}
                        >
                          <UpdateCategoryModal
                            data={{
                              id: data?.categoriesID,
                              size: data?.sizeOfRooms,
                              type: data?.typeOfBed,
                              number: data?.numberOfMember,
                            }}
                          >
                            <Button>
                              <EditOutlined />
                            </Button>
                          </UpdateCategoryModal>

                          <DeleteCategoryModal
                            data={{
                              id: data?.categoriesID,
                              size: data?.sizeOfRooms,
                              type: data?.typeOfBed,
                            }}
                          >
                            <Button>
                              <DeleteOutlined />
                            </Button>
                          </DeleteCategoryModal>
                        </Box>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <Box
            sx={{
              marginLeft: "30px",
              marginRight: "30px",
              marginTop: "20px",
              paddingBottom: "20px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {data?.length > 0 && (
                <Grid item xs={6} sx={{ alignItems: "center" }}>
                  <label>
                    <b>
                      Showing {currentPage} of {totalPageCategory}{" "}
                      {totalPageCategory > 1 ? "pages" : "page"}
                    </b>
                  </label>
                </Grid>
              )}
              <Stack sx={{ alignItems: "center" }}>
                {data?.length > 0 && (
                  <Pagination
                    color="standard"
                    variant="outlined"
                    defaultPage={1}
                    count={totalPageCategory}
                    page={currentPage}
                    onChange={paginate}
                    size="medium"
                    showFirstButton
                    showLastButton
                  />
                )}
              </Stack>
              {data?.length === 0 && (
                <Box
                  width="100%"
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <LoadingButton
                    loading
                    variant="outlined"
                    sx={{ border: "0 !important" }}
                  />
                  <Typography
                    className="color-change-3x"
                    sx={{
                      fontSize: "20px",
                      fontWeight: 400,
                    }}
                  >
                    Loading...
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default CategoryManagementPage;
