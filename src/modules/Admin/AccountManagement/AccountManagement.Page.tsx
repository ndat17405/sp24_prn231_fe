/* eslint-disable react-hooks/exhaustive-deps */
import {
  Avatar,
  Box,
  Divider,
  Grid,
  InputAdornment,
  Pagination,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import SidebarComponents from "../../../layouts/Drawer/Admin/Sidebar.Components";
import Header from "../../../layouts/Header/Admin/Header.Admin";
import "./AccountManagement.Style.scss";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { AssetImages } from "../../../utils/images";
import { useEffect, useState } from "react";
import { getAllAccount, searchAccount } from "../Admin.Api";
import { SearchOutlined } from "@mui/icons-material";
import { LoadingButton } from "@mui/lab";

const AccountManagementPage: React.FC<{}> = () => {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [valueInput, setValueInput] = useState("");

  const accountPerPage = 6;
  const indexOfLastAccount = currentPage * accountPerPage;
  const indexOfFirstAccount = indexOfLastAccount - accountPerPage;
  const currentAccount = data.slice(indexOfFirstAccount, indexOfLastAccount);
  const totalPageAccount = Math.ceil(data?.length / accountPerPage);

  const handleChangeInput = (e: any) => {
    setValueInput(e.target.value);
  };

  const init = async () => {
    const res = await getAllAccount();
    if (res) {
      if (valueInput === "") {
        setTimeout(() => {
          setData(res);
        }, 100);
      } else {
        const searchRes = await searchAccount(valueInput.toLowerCase());
        if (searchRes) {
          setTimeout(() => {
            setData(searchRes);
          }, 1);
        }
      }
    }
  };

  const paginate = (event: any, value: any) => {
    event.preventDefault();
    setCurrentPage(value);
    document.documentElement.scrollTop = 200;
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [currentAccount, valueInput]);

  return (
    <BoxContainer property="account-management--container">
      <SidebarComponents />
      <Box className="content--container">
        <Header />
        <BoxContainer property="content--wrapper">
          <Typography className="content__title--account">
            Account Management
          </Typography>
          <Divider sx={{ backgroundColor: "#BBBBBF", m: "30px 0" }} />
          <TextField
            label="Search"
            placeholder="Search here..."
            value={valueInput}
            onChange={handleChangeInput}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <SearchOutlined />
                </InputAdornment>
              ),
            }}
            sx={{
              width: "35%",
              m: "0 30px 30px 30px",
            }}
            size="small"
          />

          <Typography className="content__number--account">
            {data?.length > 1
              ? `${data?.length} Users`
              : `${data?.length} User`}
          </Typography>
          <Box className="table--account">
            <TableContainer
              component={Paper}
              sx={{
                m: "0 30px 30px",
                width: "auto",
                boxShadow: "none",
                border: "none",
                borderRadius: 0,
              }}
            >
              <Table>
                <TableHead>
                  <TableRow
                    sx={{
                      background: themeColors.white,
                    }}
                  >
                    <TableCell className="table__title--account">
                      User's Fullname
                    </TableCell>
                    <TableCell className="table__title--account">
                      Email
                    </TableCell>
                    <TableCell className="table__title--account">
                      Phone
                    </TableCell>
                    <TableCell className="table__title--account">
                      Address
                    </TableCell>
                    <TableCell className="table__title--account">
                      Status
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {currentAccount?.map((data: any) => (
                    <TableRow
                      key={data.id}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell
                        sx={{
                          p: "16px 7rem 16px 0",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: "20px",
                          }}
                        >
                          <Avatar
                            src={AssetImages.LOGO_HOTEL}
                            sx={{
                              width: "50px",
                              height: "50px",
                              border: `1px solid ${themeColors.black}`,
                            }}
                          />
                          <Typography sx={{ width: "100%", fontSize: "16px" }}>
                            {data?.profile?.fullName
                              ? data?.profile?.fullName
                              : "Admin"}
                          </Typography>
                        </Box>
                      </TableCell>
                      <TableCell sx={{ fontSize: "16px" }}>
                        {data?.email}
                      </TableCell>
                      <TableCell sx={{ fontSize: "16px" }}>
                        {data?.profile?.phone}
                      </TableCell>
                      <TableCell sx={{ fontSize: "16px" }}>
                        {data?.profile?.address}
                      </TableCell>
                      <TableCell
                        sx={{
                          fontSize: "16px",
                          color: data?.status
                            ? themeColors.statusPositive
                            : themeColors.adminNegative,
                          fontWeight: "bold",
                        }}
                      >
                        {data?.status ? "Active" : "Inactive"}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <Box
            sx={{
              marginLeft: "30px",
              marginRight: "30px",
              marginTop: "20px",
              paddingBottom: "20px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {data?.length > 0 && (
                <Grid item xs={6} sx={{ alignItems: "center" }}>
                  <label>
                    <b>
                      Showing {currentPage} of {totalPageAccount}{" "}
                      {totalPageAccount > 1 ? "pages" : "page"}
                    </b>
                  </label>
                </Grid>
              )}
              <Stack sx={{ alignItems: "center" }}>
                {data?.length > 0 && (
                  <Pagination
                    color="standard"
                    variant="outlined"
                    defaultPage={1}
                    count={totalPageAccount}
                    page={currentPage}
                    onChange={paginate}
                    size="medium"
                    showFirstButton
                    showLastButton
                  />
                )}
              </Stack>
              {data?.length === 0 && (
                <Box
                  width="100%"
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <LoadingButton
                    loading
                    variant="outlined"
                    sx={{ border: "0 !important" }}
                  />
                  <Typography
                    className="color-change-3x"
                    sx={{
                      fontSize: "20px",
                      fontWeight: 400,
                    }}
                  >
                    Loading...
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default AccountManagementPage;
