import AccountManagementPage from "./AccountManagement.Page";

const AccountManagementContainer: React.FC<{}> = () => {
  return <AccountManagementPage />;
};
export default AccountManagementContainer;
