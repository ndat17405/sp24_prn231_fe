import { AdminActions } from "../../redux/actions";

export interface ChangeSidebarAction {
  type: AdminActions.CHANGE_SIDEBAR;
  payload: any;
}

export type AdminAction = ChangeSidebarAction;

export const changeSidebar = (index: any): ChangeSidebarAction => {
  return {
    type: AdminActions.CHANGE_SIDEBAR,
    payload: index,
  };
};
