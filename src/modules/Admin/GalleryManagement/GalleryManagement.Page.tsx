/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import SidebarComponents from "../../../layouts/Drawer/Admin/Sidebar.Components";
import Header from "../../../layouts/Header/Admin/Header.Admin";
import "./GalleryManagement.Style.scss";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { AssetImages } from "../../../utils/images";
import { useState, useEffect } from "react";
import AddImageModal from "../Modal/Gallery/AddImage/AddImage.Modal";
import DeleteImageModal from "../Modal/Gallery/DeleteImage/DeleteImage.Modal";
import { getAllGalleries } from "../Admin.Api";
import {
  filterImageIsDining,
  filterImageIsGuestRoom,
  filterImageIsHotelView,
  filterImageIsSpa,
  filterImageIsWedding,
} from "../../../utils/helper";
import { LoadingButton } from "@mui/lab";

const titleFilter = ["Hotel View", "Rooms", "Spa", "Dining", "Weddings"];

const GalleryManagementPage = () => {
  const [open, setOpen] = useState(false);
  const [data, setData] = useState([]);
  const [title, setTitle] = useState("");

  const [currentPage, setCurrentPage] = useState(1);

  const imagesPerPage = 4;
  const indexOfLastImage = currentPage * imagesPerPage;
  const indexOfFirstImage = indexOfLastImage - imagesPerPage;
  const currentImage = data.slice(indexOfFirstImage, indexOfLastImage);
  const totalPageImage = Math.ceil(data?.length / imagesPerPage);

  const handleFilterGallery = (e: any) => {
    setTitle(e.target.value);
  };

  const getAllGallery = async () => {
    const res = await getAllGalleries();
    if (res) {
      switch (title) {
        case "Hotel View":
          setData(filterImageIsHotelView(res));
          break;
        case "Spa":
          setData(filterImageIsSpa(res));
          break;
        case "Dining":
          setData(filterImageIsDining(res));
          break;
        case "Rooms":
          setData(filterImageIsGuestRoom(res));
          break;
        case "Weddings":
          setData(filterImageIsWedding(res));
          break;
        default:
          setData(res);
          break;
      }
    }
  };

  const paginate = (event: any, value: any) => {
    event.preventDefault();
    setCurrentPage(value);
    document.documentElement.scrollTop = 200;
  };

  const handleOpen = () => {
    setOpen(true);
  };

  useEffect(() => {
    getAllGallery();
  }, [data]);

  useEffect(() => {}, [open, currentPage]);

  return (
    <BoxContainer property="gallery-management--container">
      <SidebarComponents />
      <Box className="gallery-content--container">
        <Header />
        <BoxContainer property="gallery-content--wrapper">
          <Typography className="content__title--gallery">
            Gallery Management
          </Typography>
          <Divider sx={{ backgroundColor: "#BBBBBF", m: "30px 0" }} />
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography className="content__number--image">
              {data?.length > 1
                ? `${data?.length} Images`
                : `${data?.length} Image`}
            </Typography>

            <FormControl sx={{ minWidth: "30%", mb: "30px" }}>
              <Select
                value={title}
                onChange={handleFilterGallery}
                displayEmpty
                size="small"
                sx={{ borderRadius: "8px" }}
              >
                <MenuItem value="">
                  <em style={{ color: "#B2B2B2" }}>
                    -- Choose title to filter --
                  </em>
                </MenuItem>
                {titleFilter?.map((item: any) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <AddImageModal>
              <Box
                onClick={handleOpen}
                sx={{
                  backgroundColor: themeColors.btnAdd,
                  borderRadius: "12px",
                  display: "flex",
                  alignItems: "center",
                  gap: "20px",
                  p: "10px 30px",
                  m: "0 30px 30px",
                  "&:hover": {
                    opacity: ".8",
                    cursor: "pointer",
                  },
                }}
              >
                <img
                  src={AssetImages.ICONS.CRUD.ADD}
                  alt=""
                  style={{ width: "30px", height: "30px" }}
                />
                Add Image
              </Box>
            </AddImageModal>
          </Box>
          <Box className="table--gallery">
            <TableContainer
              component={Paper}
              sx={{
                m: "0 30px 30px",
                width: "auto",
                boxShadow: "none",
                border: "none",
                borderRadius: 0,
              }}
            >
              <Table>
                <TableHead>
                  <TableRow
                    sx={{
                      background: themeColors.white,
                    }}
                  >
                    <TableCell className="table__title--gallery">
                      Image
                    </TableCell>
                    <TableCell className="table__title--gallery">
                      Title
                    </TableCell>
                    <TableCell align="center" className="table__title--gallery">
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {currentImage?.map((item: any) => (
                    <TableRow
                      key={item.id}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell
                        align="left"
                        sx={{
                          p: "16px 0 16px 0",
                        }}
                      >
                        <img
                          src={`data:image/*;base64,${item.images}`}
                          alt=""
                          style={{
                            width: "380px",
                            height: "230px",
                            borderRadius: "8px",
                            boxShadow: themeColors.boxShadow,
                          }}
                        />
                      </TableCell>
                      <TableCell sx={{ fontSize: "16px" }}>
                        {item.title}
                      </TableCell>
                      <TableCell align="center">
                        <DeleteImageModal
                          data={{ id: item?.id, title: item?.title }}
                        >
                          <Button className="table--btn-delete" size="medium">
                            Delete
                          </Button>
                        </DeleteImageModal>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <Box
            sx={{
              marginLeft: "30px",
              marginRight: "30px",
              marginTop: "20px",
              paddingBottom: "20px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {data?.length > 0 && (
                <Grid item xs={6} sx={{ alignItems: "center" }}>
                  <label>
                    <b>
                      Showing {currentPage} of {totalPageImage}{" "}
                      {totalPageImage > 1 ? "pages" : "page"}
                    </b>
                  </label>
                </Grid>
              )}
              <Stack sx={{ alignItems: "center" }}>
                {data?.length > 0 && (
                  <Pagination
                    color="standard"
                    variant="outlined"
                    defaultPage={1}
                    count={totalPageImage}
                    page={currentPage}
                    onChange={paginate}
                    size="medium"
                    showFirstButton
                    showLastButton
                  />
                )}
              </Stack>
              {data?.length === 0 && (
                <Box
                  width="100%"
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <LoadingButton
                    loading
                    variant="outlined"
                    sx={{ border: "0 !important" }}
                  />
                  <Typography
                    className="color-change-3x"
                    sx={{
                      fontSize: "20px",
                      fontWeight: 400,
                    }}
                  >
                    Loading...
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default GalleryManagementPage;
