/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Button,
  Grid,
  Pagination,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { themeColors } from "../../../../../themes/schemes/PureLightTheme";
import "./PendingBooking.Style.scss";
import ConfirmBookingModal from "../../../Modal/Booking/ConfirmBooking.Modal";
import { useEffect, useState } from "react";
import { filterBookingIsWaitting } from "../../../../../utils/helper";
import { getAllBooking } from "../../../Admin.Api";
import { LoadingButton } from "@mui/lab";

const PendingBookingComponent = () => {
  const [data, setData] = useState<any>([]);
  const [currentPage, setCurrentPage] = useState(1);

  const bookingPerPage = 6;
  const indexOfLastBooking = currentPage * bookingPerPage;
  const indexOfFirstBooking = indexOfLastBooking - bookingPerPage;
  const currentBooking = data.slice(indexOfFirstBooking, indexOfLastBooking);
  const totalPageBooking = Math.ceil(data?.length / bookingPerPage);

  const paginate = (event: any, value: any) => {
    event.preventDefault();
    setCurrentPage(value);
    document.documentElement.scrollTop = 200;
  };

  // Hàm tự định dạng ngày tháng
  const formatDate = (dateString: string | undefined): string => {
    if (!dateString) return ""; // Trả về chuỗi rỗng nếu không có ngày tháng

    const date = new Date(dateString); // Chuyển đổi chuỗi ngày tháng thành đối tượng Date
    if (isNaN(date.getTime())) return ""; // Trả về chuỗi rỗng nếu chuỗi không hợp lệ

    const day = date.getDate().toString().padStart(2, "0"); // Lấy ngày và thêm '0' phía trước nếu cần
    const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Lấy tháng và thêm '0' phía trước nếu cần
    const year = date.getFullYear();

    return `${day}/${month}/${year}`;
  };

  const init = async () => {
    const res = await getAllBooking();
    if (res) {
      setData(filterBookingIsWaitting(res));
    }
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [currentBooking, formatDate, currentPage]);

  return (
    <>
      <Typography
        sx={{
          width: "fit-content",
          background: themeColors.bookingReverse,
          backgroundClip: "text",
          WebkitBackgroundClip: "text",
          WebkitTextFillColor: "transparent",
          p: "0 30px 30px",
          fontSize: "28px",
          fontWeight: 700,
        }}
      >
        {`${data?.length} Booking`}
      </Typography>
      <Box className="table--booking">
        <TableContainer
          component={Paper}
          sx={{
            m: "0 30px 30px",
            width: "auto",
            boxShadow: "none",
            border: "none",
            borderRadius: 0,
          }}
        >
          <Table>
            <TableHead>
              <TableRow
                sx={{
                  background: themeColors.white,
                }}
              >
                <TableCell
                  sx={{
                    width: "23%",
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                    p: "16px 16px 16px 0",
                  }}
                >
                  Room
                </TableCell>
                <TableCell
                  sx={{
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                    p: "10px",
                  }}
                >
                  Check-in
                </TableCell>
                <TableCell
                  sx={{
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                    p: "10px",
                  }}
                >
                  Check-out
                </TableCell>
                <TableCell
                  sx={{
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                    p: "10px",
                  }}
                >
                  Name
                </TableCell>
                <TableCell
                  sx={{
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                    p: "10px",
                  }}
                >
                  Email
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                    p: "10px",
                  }}
                >
                  Status
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    color: themeColors.black,
                    fontSize: "18px",
                    fontWeight: 700,
                  }}
                >
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {currentBooking.map((item: any) => (
                <TableRow
                  key={item?.bookingID}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell
                    sx={{
                      p: "16px 0 16px 0",
                    }}
                  >
                    <Box
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        gap: "20px",
                      }}
                    >
                      <img
                        src={`data:image/*;base64,${item?.rooms.images}`}
                        alt=""
                        style={{
                          minWidth: "120px",
                          maxWidth: "120px",
                          minHeight: "80px",
                          maxHeight: "80px",
                          borderRadius: "8px",
                          boxShadow: themeColors.boxShadow,
                        }}
                      />
                      <Typography
                        sx={{
                          width: "100%",
                          fontSize: "16px",
                          display: "-webkit-box",
                          overflow: "hidden",
                          WebkitLineClamp: "2",
                          WebkitBoxOrient: "vertical",
                          transition: "-webkit-line-clamp 0.1s",
                          lineHeight: "1.6rem",
                          "&:hover": {
                            overflow: "visible",
                            whiteSpace: "normal",
                            WebkitLineClamp: "unset",
                          },
                        }}
                      >
                        {item?.rooms.roomsName}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell sx={{ fontSize: "16px", p: "10px" }}>
                    {formatDate(item?.checkInDate)}
                  </TableCell>
                  <TableCell sx={{ fontSize: "16px", p: "10px" }}>
                    {formatDate(item?.checkOutDate)}
                  </TableCell>
                  <TableCell sx={{ fontSize: "16px", p: "10px" }}>
                    {item?.account?.profile?.fullName}
                  </TableCell>
                  <TableCell sx={{ fontSize: "16px", p: "10px" }}>
                    {item?.account?.email}
                  </TableCell>
                  <TableCell
                    align="center"
                    sx={{
                      color: item?.status
                        ? themeColors.statusPositive
                        : themeColors.adminNegative,
                      fontSize: "16px",
                      fontWeight: 700,
                      p: "10px",
                    }}
                  >
                    {item?.status ? "Completed" : "Wait for confirm"}
                  </TableCell>
                  <TableCell>
                    <ConfirmBookingModal
                      data={{
                        id: `${item?.bookingID}`,
                        name: `${item?.account?.profile?.fullName}`,
                      }}
                    >
                      <Button
                        sx={{
                          background: themeColors.header,
                          p: "10px 40px",
                          color: themeColors.white,
                          fontWeight: "700",
                          borderRadius: "8px",
                          "&:hover": {
                            background: themeColors.btnAccountReverse,
                          },
                        }}
                        size="medium"
                      >
                        Confirm
                      </Button>
                    </ConfirmBookingModal>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
      <Box
        sx={{
          marginLeft: "30px",
          marginRight: "30px",
          marginTop: "20px",
          paddingBottom: "20px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          {data?.length > 0 && (
            <Grid item xs={6} sx={{ alignItems: "center" }}>
              <label>
                <b>
                  Showing {currentPage} of {totalPageBooking}{" "}
                  {totalPageBooking > 1 ? "pages" : "page"}
                </b>
              </label>
            </Grid>
          )}
          <Stack sx={{ alignItems: "center" }}>
            {data?.length > 0 && (
              <Pagination
                color="standard"
                variant="outlined"
                defaultPage={1}
                count={totalPageBooking}
                page={currentPage}
                onChange={paginate}
                size="medium"
                showFirstButton
                showLastButton
              />
            )}
          </Stack>
          {data?.length === 0 && (
            <Box
              width="100%"
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <LoadingButton
                loading
                variant="outlined"
                sx={{ border: "0 !important" }}
              />
              <Typography
                className="color-change-3x"
                sx={{
                  fontSize: "20px",
                  fontWeight: 400,
                }}
              >
                Loading...
              </Typography>
            </Box>
          )}
        </Box>
      </Box>
    </>
  );
};

export default PendingBookingComponent;
