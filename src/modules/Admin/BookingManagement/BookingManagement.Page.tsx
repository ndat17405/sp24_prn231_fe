import { Box, Button, Divider, Typography } from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import SidebarComponents from "../../../layouts/Drawer/Admin/Sidebar.Components";
import Header from "../../../layouts/Header/Admin/Header.Admin";
import "./BookingManagement.Style.scss";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { useEffect, useState } from "react";
import AllBookingComponents from "./BookingList/AllBooking/AllBooking.Components";
import PendingBookingComponents from "./BookingList/PendingBooking/PendingBooking.Components";
import CompletedBookingComponents from "./BookingList/CompletedBooking/CompletedBooking.Components";

const filterItems = ["All", "Wait for confirm", "Confirm"];

const BookingManagementPage = () => {
  const [isSelectFilterItem, setIsSelectFilterItem] = useState(0);

  const handleFilterItemClicked = (index: any) => {
    setIsSelectFilterItem(index);
  };

  useEffect(() => {}, [isSelectFilterItem]);

  return (
    <BoxContainer property="booking-management--container">
      <SidebarComponents />
      <Box className="booking-content--container">
        <Header />
        <BoxContainer property="booking-content--wrapper">
          <Typography className="content__title--booking">
            Booking Management
          </Typography>
          <Divider sx={{ backgroundColor: "#BBBBBF", m: "30px 0" }} />
          <Box
            sx={{
              bgcolor: themeColors.navBooking,
              display: "flex",
              justifyContent: "space-around",
              m: "0 30px 30px",
              borderRadius: "8px",
            }}
          >
            {filterItems.map((item, index) => (
              <Button
                key={item}
                sx={{
                  color: "white",
                  bgcolor: index === isSelectFilterItem ? "#5EBFE6" : "inherit",
                  fontSize: "18px",
                  fontWeight: "normal",
                  borderRadius: "0",
                  transition: "all ease-in .2s",
                  "&:hover": {
                    bgcolor: themeColors.btnAdd,
                    color: "black",
                  },
                }}
                onClick={() => handleFilterItemClicked(index)}
              >
                {item}
              </Button>
            ))}
          </Box>
          {isSelectFilterItem === 0 ? <AllBookingComponents /> : null}
          {isSelectFilterItem === 1 ? <PendingBookingComponents /> : null}
          {isSelectFilterItem === 2 ? <CompletedBookingComponents /> : null}
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default BookingManagementPage;
