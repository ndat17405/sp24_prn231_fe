import { AdminActions } from "../../redux/actions";

export interface AdminState {
  indexSidebar: any | 0;
}

const initialState = {
  indexSidebar: 0,
};

function adminReducer(state = initialState, action: any) {
  switch (action.type) {
    case AdminActions.CHANGE_SIDEBAR:
      return {
        ...state,
        indexSidebar: action.payload,
      };
    default:
      return state;
  }
}

export default adminReducer;
