/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  InputAdornment,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import SidebarComponents from "../../../layouts/Drawer/Admin/Sidebar.Components";
import Header from "../../../layouts/Header/Admin/Header.Admin";
import "./RoomManagement.Style.scss";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import {
  DeleteOutlined,
  EditOutlined,
  SearchOutlined,
} from "@mui/icons-material";
import { useEffect, useState } from "react";
import UpdateRoomModal from "../Modal/Room/UpdateRoom/StepOne/UpdateRoom.Modal";
import DeleteRoomModal from "../Modal/Room/DeleteRoom/DeleteRoom.Modal";
import { getAllRoom, searchRoomByName } from "../Admin.Api";
import { AssetImages } from "../../../utils/images";
import AddRoomModal from "../Modal/Room/AddRoom/StepOne/AddRoom.Modal";
import {
  filterRoomIsEmptyList,
  filterRoomIsNotEmpty,
} from "../../../utils/helper";
import { LoadingButton } from "@mui/lab";

const valueStatus = ["Empty", "Out of room"];

const RoomManagementPage = () => {
  const [data, setData] = useState<any>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [status, setStatus] = useState("");
  const [searchValue, setSearchValue] = useState("");

  const roomsPerPage = 5;
  const indexOfLastRoom = currentPage * roomsPerPage;
  const indexOfFirstRoom = indexOfLastRoom - roomsPerPage;
  const currentRoom = data.slice(indexOfFirstRoom, indexOfLastRoom);
  const totalPageRoom = Math.ceil(data?.length / roomsPerPage);

  const handleChangeInput = (e: any) => {
    setSearchValue(e.target.value);
  };

  const handleStatusChange = (e: any) => {
    setStatus(e.target.value);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const init = async () => {
    const res = await getAllRoom();

    if (res) {
      if (searchValue === "") {
        switch (status) {
          case "Empty":
            setTimeout(() => {
              setData(filterRoomIsEmptyList(res));
            }, 100);
            break;
          case "Out of room":
            setTimeout(() => {
              setData(filterRoomIsNotEmpty(res));
            }, 100);
            break;
          default:
            setTimeout(() => {
              setData(res);
            }, 100);
            break;
        }
      } else if (searchValue !== "") {
        const searchRes = await searchRoomByName(searchValue.toLowerCase());
        if (status === "") {
          setTimeout(() => {
            setData(searchRes);
          }, 1);
        } else {
          switch (status) {
            case "Empty":
              setTimeout(() => {
                setData(filterRoomIsEmptyList(searchRes));
              }, 10);
              break;
            case "Out of room":
              setTimeout(() => {
                setData(filterRoomIsNotEmpty(searchRes));
              }, 10);
              break;
            default:
              setTimeout(() => {
                setData(searchRes);
              }, 100);
              break;
          }
        }
      }
    }
  };

  const paginate = (event: any, value: any) => {
    event.preventDefault();
    setCurrentPage(value);
    document.documentElement.scrollTop = 200;
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [currentPage, open]);

  return (
    <BoxContainer property="room-management--container">
      <SidebarComponents />
      <Box className="room-content--container">
        <Header />
        <BoxContainer property="room-content--wrapper">
          <Typography className="content__title--room">
            Rooms Management
          </Typography>
          <Divider sx={{ backgroundColor: "#BBBBBF", m: "30px 0" }} />
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <TextField
              label="Search"
              placeholder="Search here..."
              value={searchValue}
              onChange={handleChangeInput}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <SearchOutlined />
                  </InputAdornment>
                ),
              }}
              sx={{
                width: "30%",
                m: "0 30px 30px 30px",
              }}
              size="small"
            />

            <FormControl sx={{ minWidth: "30%", mb: "30px" }}>
              <Select
                value={status}
                onChange={handleStatusChange}
                displayEmpty
                size="small"
                sx={{ borderRadius: "8px" }}
              >
                <MenuItem value="">
                  <em style={{ color: "#B2B2B2" }}>
                    -- Choose status for filter --
                  </em>
                </MenuItem>
                {valueStatus?.map((item: any) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <AddRoomModal>
              <Box
                onClick={handleOpen}
                sx={{
                  backgroundColor: themeColors.btnAdd,
                  borderRadius: "12px",
                  display: "flex",
                  alignItems: "center",
                  gap: "20px",
                  "&:hover": {
                    opacity: ".8",
                    cursor: "pointer",
                  },
                }}
              >
                <img
                  src={AssetImages.ICONS.CRUD.ADD}
                  alt=""
                  style={{ width: "30px", height: "30px" }}
                />
                Add Room
              </Box>
            </AddRoomModal>
          </Box>
          <Typography className="content__number--room">
            {data?.length > 1
              ? `${data?.length} Rooms`
              : `${data?.length} Room`}
          </Typography>
          <Box className="table--room">
            <TableContainer
              component={Paper}
              sx={{
                m: "0 30px 30px",
                width: "auto",
                boxShadow: "none",
                border: "none",
                borderRadius: 0,
              }}
            >
              <Table>
                <TableHead>
                  <TableRow
                    sx={{
                      background: themeColors.white,
                    }}
                  >
                    <TableCell className="table__title--room">Room</TableCell>
                    <TableCell className="table__title--room">Price</TableCell>
                    <TableCell className="table__title--room">Status</TableCell>
                    <TableCell align="center" className="table__title--room">
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {currentRoom.map((room: any) => (
                    <TableRow
                      key={room.roomsID}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell
                        sx={{
                          p: "16px 0 16px 0",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: "20px",
                          }}
                        >
                          <img
                            src={`data:image/*;base64,${room?.images}`}
                            alt=""
                            style={{
                              minWidth: "120px",
                              maxWidth: "120px",
                              minHeight: "80px",
                              maxHeight: "80px",
                              borderRadius: "8px",
                              boxShadow: themeColors.boxShadow,
                            }}
                          />
                          <Typography sx={{ width: "100%", fontSize: "16px" }}>
                            {room?.roomsName}
                          </Typography>
                        </Box>
                      </TableCell>
                      <TableCell sx={{ fontSize: "16px" }}>
                        $ {room?.price}.00
                      </TableCell>
                      <TableCell
                        sx={{
                          color: room?.status
                            ? `${themeColors.statusPositive}`
                            : `${themeColors.adminNegative}`,
                          fontSize: "16px",
                          fontWeight: 700,
                        }}
                      >
                        {room?.status ? "Empty" : "Out of room"}
                      </TableCell>
                      <TableCell>
                        <Box
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "space-evenly",
                          }}
                        >
                          <UpdateRoomModal
                            data={{
                              id: room?.roomsID,
                              img: room?.images,
                              name: room?.roomsName,
                              price: room?.price,
                              status: room?.status,
                              category: room?.category?.categoriesID,
                              bedsAndBeding: room?.bedsAndBeding,
                              internetAndPhones: room?.internetAndPhones,
                              bathAndBathroom: room?.bathAndBathroom,
                              entertainment: room?.entertainment,
                            }}
                          >
                            <Button>
                              <EditOutlined />
                            </Button>
                          </UpdateRoomModal>

                          <DeleteRoomModal
                            data={{ id: room?.roomsID, name: room?.roomsName }}
                          >
                            <Button>
                              <DeleteOutlined />
                            </Button>
                          </DeleteRoomModal>
                        </Box>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <Box
            sx={{
              marginLeft: "30px",
              marginRight: "30px",
              marginTop: "20px",
              paddingBottom: "20px",
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {data?.length > 0 && (
                <Grid item xs={6} sx={{ alignItems: "center" }}>
                  <label>
                    <b>
                      Showing {currentPage} of {totalPageRoom}{" "}
                      {totalPageRoom > 1 ? "pages" : "page"}
                    </b>
                  </label>
                </Grid>
              )}

              <Stack sx={{ alignItems: "center" }}>
                {data?.length > 0 && (
                  <Pagination
                    color="standard"
                    variant="outlined"
                    defaultPage={1}
                    count={totalPageRoom}
                    page={currentPage}
                    onChange={paginate}
                    size="medium"
                    showFirstButton
                    showLastButton
                  />
                )}
              </Stack>
              {data?.length === 0 && (
                <Box
                  width="100%"
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <LoadingButton
                    loading
                    variant="outlined"
                    sx={{ border: "0 !important" }}
                  />
                  <Typography
                    className="color-change-3x"
                    sx={{
                      fontSize: "20px",
                      fontWeight: 400,
                    }}
                  >
                    Loading...
                  </Typography>
                </Box>
              )}
            </Box>
          </Box>
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default RoomManagementPage;
