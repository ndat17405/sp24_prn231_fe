import axios from "axios";
import { ApiUrl } from "../../services/ApiUrl";
import axiosAdmin from "../../utils/axiosAdmin";

export const getAllAccount = async () => {
  try {
    const response = await axiosAdmin.get(`${ApiUrl.GET_ALL_ACCOUNT}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const searchAccount = async (input: any) => {
  try {
    const response = await axiosAdmin.get(
      `${ApiUrl.GET_ACCOUNT_BY_NAME}?fullName=${input}`
    );
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    return [];
  }
};

export const getAllRoom = async () => {
  try {
    const response = await axiosAdmin.get(`${ApiUrl.GET_ALL_ROOMS}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const searchRoomByName = async (input: any) => {
  try {
    const response = await axiosAdmin.get(
      `${ApiUrl.GET_ROOM_BY_NAME}?name=${input}`
    );
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    return [];
  }
};

export const addRoom = async (
  name: any,
  price: any,
  category: any,
  bedService: any,
  internetService: any,
  bathService: any,
  entertainment: any,
  file: any
) => {
  try {
    let formData = new FormData();

    formData.append("RoomsName", name);
    formData.append("Price", price);
    formData.append("categoryId", category);
    formData.append("BedsAndBeding", bedService);
    formData.append("InternetAndPhones", internetService);
    formData.append("BathAndBathroom", bathService);
    formData.append("Entertainment", entertainment);
    formData.append("image", file);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.post(
      "https://eposhbookingadminapi-production.up.railway.app/api/v1/rooms/addRoom",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const updateRoom = async (
  id: any,
  name: any,
  price: any,
  category: any,
  bedService: any,
  internetService: any,
  bathService: any,
  entertainment: any,
  file: any
) => {
  try {
    let formData = new FormData();

    formData.append("id", id);
    formData.append("RoomsName", name);
    formData.append("Price", price);
    formData.append("categoryId", category);
    formData.append("BedsAndBeding", bedService);
    formData.append("InternetAndPhones", internetService);
    formData.append("BathAndBathroom", bathService);
    formData.append("Entertainment", entertainment);
    formData.append("image", file);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.put(
      "https://eposhbookingadminapi-production.up.railway.app/api/v1/rooms/updateByID",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const deleteRoom = async (id: any) => {
  try {
    const response = await axiosAdmin.delete(`${ApiUrl.DELETE_ROOM}?id=${id}`);
    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const getAllCategory = async () => {
  try {
    const response = await axiosAdmin.get(`${ApiUrl.GET_ALL_CATEGORY}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const addCategory = async (categoryData: any) => {
  try {
    const response = await axiosAdmin.post(
      `${ApiUrl.ADD_CATEGORY}`,
      categoryData
    );
    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const updateCategory = async (
  id: any,
  size: any,
  type: any,
  number: any
) => {
  try {
    let formData = new FormData();

    formData.append("id", id);
    formData.append("SizeOfRooms", size);
    formData.append("TypeOfBed", type);
    formData.append("NumberOfMember", number);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.put(
      "https://eposhbookingadminapi-production.up.railway.app/api/v1/category/updateByID",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const deleteCategory = async (id: any) => {
  try {
    const response = await axiosAdmin.delete(
      `${ApiUrl.DELETE_CATEGORY}?id=${id}`
    );
    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
  }
};

export const getAllGalleries = async () => {
  try {
    const response = await axiosAdmin.get(`${ApiUrl.GET_ALL_GALLERIES}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const addImage = async (title: any, file: any) => {
  try {
    let fromData = new FormData();

    fromData.append("images", file);
    fromData.append("title", title);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.post(
      "https://eposhbookingadminapi-production.up.railway.app/api/v1/galleries/addGalleries",
      fromData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const deleteImage = async (id: any) => {
  try {
    const response = await axiosAdmin.delete(
      `${ApiUrl.DELETE_GALLERY}?id=${id}`
    );
    if (response.data.data) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const getAllBooking = async () => {
  try {
    const response = await axiosAdmin.get(`${ApiUrl.GET_ALL_BOOKING}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    return false;
  }
};

export const confirmBooking = async (bookingId: any, bookingStatus: any) => {
  try {
    let formData = new FormData();

    formData.append("id", bookingId);
    formData.append("status", bookingStatus);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.put(
      "https://eposhbookingadminapi-production.up.railway.app/api/v1/booking/confirmBooking",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};
