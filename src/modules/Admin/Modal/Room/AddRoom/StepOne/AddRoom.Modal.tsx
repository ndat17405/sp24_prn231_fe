import { AddPhotoAlternateOutlined } from "@mui/icons-material";
import {
  Box,
  Button,
  ButtonProps,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { themeColors } from "../../../../../../themes/schemes/PureLightTheme";
import "./AddRoom.Style.scss";
import AddRoomChildModal from "../StepTwo/AddRoom.ChildModal";
import { getAllCategory } from "../../../../Admin.Api";

const AddRoomModal = ({ children }: { children: any }) => {
  const [dataCategory, setDataCategory] = useState<any>([]);
  const [open, setOpen] = useState(false);
  const fileInputRef = useRef<HTMLInputElement>(null);

  const [image, imageChange] = useState<string | ArrayBuffer | null>("");
  const [imageRoom, setImageRoom] = useState(null);
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [category, setCategory] = useState(0);

  const init = async () => {
    const res = await getAllCategory();
    if (res) {
      setDataCategory(res);
    }
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUploadImage = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleFileInputChange = (event: any) => {
    const file = event.target.files?.[0];
    if (file) {
      setImageRoom(file);

      const reader = new FileReader();

      reader.onloadend = () => {
        imageChange(reader.result);
      };

      reader.readAsDataURL(file);
    }
  };

  const handleNameChange = (e: any) => {
    setName(e.target.value);
  };

  const handlePriceChange = (e: any) => {
    setPrice(e.target.value);
  };

  const handleCategoryChange = (e: any) => {
    setCategory(e.target.value);
  };

  useEffect(() => {
    init();
  }, [dataCategory]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
        style: {
          padding: "10px 30px",
          margin: "0 30px 30px",
        },
      })}
      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        fullWidth
        maxWidth="md"
      >
        <DialogTitle display="flex" justifyContent="center">
          <Typography
            sx={{
              width: "fit-content",
              background: themeColors.room,
              backgroundClip: "text",
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "transparent",
              fontSize: "30px",
              fontWeight: 700,
            }}
          >
            Add Room
          </Typography>
        </DialogTitle>

        <DialogContent dividers>
          <Box sx={{ display: "flex", flexDirection: "column", gap: "20px" }}>
            <Typography>Image:</Typography>
            <Box
              onClick={handleUploadImage}
              sx={{
                width: "100%",
                height: "auto",
                border: "1px dashed #bbbbbf",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                gap: "5px",
                p: "5px",
                "&:hover": {
                  cursor: "pointer",
                },
              }}
            >
              {image ? (
                <img
                  src={image as string}
                  alt="Uploaded"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                    borderRadius: "8px",
                    boxShadow: themeColors.boxShadow,
                  }}
                />
              ) : (
                <>
                  <AddPhotoAlternateOutlined />
                  <Typography sx={{ fontSize: "16px" }}>Add Image</Typography>
                </>
              )}
              <input
                ref={fileInputRef}
                type="file"
                style={{ display: "none" }}
                onChange={handleFileInputChange}
              />
            </Box>

            <Box className="modal-form--control">
              <Typography className="modal-form--label">Name:</Typography>
              <input
                type="text"
                className="modal-form--input"
                placeholder="Enter name of room here..."
                onChange={handleNameChange}
              />
            </Box>

            <Box className="modal-form--control">
              <Typography className="modal-form--label">Price:</Typography>
              <input
                type="number"
                className="modal-form--input"
                placeholder="Enter price of room here..."
                onChange={handlePriceChange}
              />
            </Box>

            <Box className="modal-form--control">
              <Typography className="modal-form--label">Category:</Typography>
              <FormControl sx={{ flex: 1 }} fullWidth>
                <Select
                  value={category}
                  onChange={handleCategoryChange}
                  displayEmpty
                  size="small"
                  sx={{ borderRadius: "8px" }}
                >
                  <MenuItem value={0}>
                    <em style={{ color: "#B2B2B2" }}>
                      -- Choose category for rooms --
                    </em>
                  </MenuItem>
                  {dataCategory?.map((item: any) => (
                    <MenuItem
                      key={item?.categoriesID}
                      value={item?.categoriesID}
                    >
                      Size: {item?.sizeOfRooms}, Type: {item?.typeOfBed},
                      Maximum capacity: {item?.numberOfMember}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </Box>
        </DialogContent>
        <DialogActions sx={{ justifyContent: "space-evenly", gap: "30px" }}>
          <Button
            onClick={handleClose}
            sx={{
              background: themeColors.adminSecondary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.adminSecondaryReverse,
              },
            }}
          >
            Cancel
          </Button>
          <AddRoomChildModal
            data={{
              img: imageRoom,
              name: name,
              price: price,
              category: category,
            }}
            handleCloseParentModal={handleClose}
          >
            <Button
              sx={{
                background: themeColors.adminPrimary,
                color: themeColors.white,
                border: `1px solid ${themeColors.black}`,
                fontWeight: "100",
                p: "5px 50px",
                fontSize: "15px",
                borderRadius: "8px",
                "&:hover": {
                  background: themeColors.numberAccount,
                },
              }}
            >
              Next
            </Button>
          </AddRoomChildModal>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AddRoomModal;
