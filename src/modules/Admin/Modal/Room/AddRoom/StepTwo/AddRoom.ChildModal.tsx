/* eslint-disable react-hooks/exhaustive-deps */
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  ButtonProps,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { themeColors } from "../../../../../../themes/schemes/PureLightTheme";
import "./ChildModal.Style.scss";
import { addRoom } from "../../../../Admin.Api";

const AddRoomChildModal = ({
  children,
  data,
  handleCloseParentModal,
}: {
  children: any;
  data: any;
  handleCloseParentModal: any;
}) => {
  const [open, setOpen] = useState(false);
  const [bedService, setBedService] = useState("");
  const [internetService, setInternetService] = useState("");
  const [bathService, setBathService] = useState("");
  const [entertainment, setEntertainment] = useState("");

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleBedServiceChange = (e: any) => {
    setBedService(e.target.value);
  };

  const handleInternetServiceChange = (e: any) => {
    setInternetService(e.target.value);
  };

  const handleBathServiceChange = (e: any) => {
    setBathService(e.target.value);
  };

  const handleEntertainmentChange = (e: any) => {
    setEntertainment(e.target.value);
  };

  const handleAddRoom = async () => {
    try {
      const nameRoom = data?.name;
      const priceRoom = data?.price;
      const categoryRoom = data?.category;
      const imageRoom = data?.img;

      if (
        nameRoom === "" ||
        priceRoom === "" ||
        categoryRoom === "" ||
        imageRoom === null ||
        bedService === "" ||
        internetService === "" ||
        bathService === "" ||
        entertainment === ""
      ) {
        setDisplayErrorWhenNull(true);
      } else {
        const res = await addRoom(
          nameRoom,
          priceRoom,
          categoryRoom,
          bedService,
          internetService,
          bathService,
          entertainment,
          imageRoom
        );

        if (res) {
          setDisplaySuccess(true);
          setTimeout(() => {
            setOpen(false);
            handleCloseParentModal();
          }, 2000);
        } else {
          setDisplayError(true);
          setOpen(true);
        }
      }
    } catch (error) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {}, [handleAddRoom]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}
      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        fullWidth
        maxWidth="md"
      >
        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            width: "22%",
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Add fail!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Add successfully!
        </Alert>

        <DialogTitle display="flex" justifyContent="center">
          <Typography
            sx={{
              width: "fit-content",
              background: themeColors.roomReverse,
              backgroundClip: "text",
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "transparent",
              fontSize: "30px",
              fontWeight: 700,
            }}
          >
            Add Service For Room
          </Typography>
        </DialogTitle>
        <DialogContent dividers>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "20px",
              alignItems: "flex-start",
            }}
          >
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Beds and Bedding:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                placeholder="Enter description here..."
                onChange={handleBedServiceChange}
              />
            </Box>
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Internet and Phones:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                placeholder="Enter description here..."
                onChange={handleInternetServiceChange}
              />
            </Box>
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Bath and Bathroom features:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                placeholder="Enter description here..."
                onChange={handleBathServiceChange}
              />
            </Box>
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Entertainment:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                placeholder="Enter description here..."
                onChange={handleEntertainmentChange}
              />
            </Box>
          </Box>
        </DialogContent>
        <DialogActions sx={{ justifyContent: "space-evenly", gap: "30px" }}>
          <Button
            onClick={handleClose}
            sx={{
              background: themeColors.adminSecondary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.adminSecondaryReverse,
              },
            }}
          >
            Back
          </Button>
          <Button
            onClick={handleAddRoom}
            sx={{
              background: themeColors.adminPrimary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.numberAccount,
              },
            }}
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AddRoomChildModal;
