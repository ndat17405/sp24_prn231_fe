/* eslint-disable react-hooks/exhaustive-deps */
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  ButtonProps,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { themeColors } from "../../../../../../themes/schemes/PureLightTheme";
import "./ChildModal.Style.scss";
import { updateRoom } from "../../../../Admin.Api";

const UpdateRoomChildModal = ({
  children,
  data,
  childData,
  handleCloseParentModal,
}: {
  children: any;
  data: any;
  childData: any;
  handleCloseParentModal: any;
}) => {
  const [open, setOpen] = useState(false);
  const [bedService, setBedService] = useState("" || childData?.bed);
  const [internetService, setInternetService] = useState(
    "" || childData?.internet
  );
  const [bathService, setBathService] = useState("" || childData?.bath);
  const [entertainment, setEntertainment] = useState(
    "" || childData?.entertainment
  );

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleBedServiceChange = (e: any) => {
    setBedService(e.target.value);
  };

  const handleInternetServiceChange = (e: any) => {
    setInternetService(e.target.value);
  };

  const handleBathServiceChange = (e: any) => {
    setBathService(e.target.value);
  };

  const handleEntertainmentChange = (e: any) => {
    setEntertainment(e.target.value);
  };

  const handleUpdate = async () => {
    try {
      const id = data?.id;
      const nameRoom = data?.name;
      const price = data?.price;
      const category = data?.category;
      const imageRoom = data?.img;

      if (
        nameRoom === "" ||
        price === "" ||
        imageRoom === null ||
        bedService === "" ||
        internetService === "" ||
        bathService === "" ||
        entertainment === ""
      ) {
        setDisplayErrorWhenNull(true);
      } else {
        const res = await updateRoom(
          id,
          nameRoom,
          price,
          category,
          bedService,
          internetService,
          bathService,
          entertainment,
          imageRoom
        );

        if (res) {
          setDisplaySuccess(true);
          setTimeout(() => {
            setOpen(false);
            handleCloseParentModal();
          }, 2000);
        } else {
          setDisplayError(true);
          setOpen(true);
        }
      }
    } catch (error) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {}, [handleUpdate]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}
      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        fullWidth
        maxWidth="md"
      >
        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Update fail. All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            width: "30%",
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Update fail!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Update successfully!
        </Alert>

        <DialogTitle display="flex" justifyContent="center">
          <Typography
            sx={{
              width: "fit-content",
              background: themeColors.roomReverse,
              backgroundClip: "text",
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "transparent",
              fontSize: "30px",
              fontWeight: 700,
            }}
          >
            Update Service For Room
          </Typography>
        </DialogTitle>
        <DialogContent dividers>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "20px",
              alignItems: "flex-start",
            }}
          >
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Beds and Bedding:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                defaultValue={childData?.bed}
                onChange={handleBedServiceChange}
              />
            </Box>
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Internet and Phones:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                defaultValue={childData?.internet}
                onChange={handleInternetServiceChange}
              />
            </Box>
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Bath and Bathroom features:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                defaultValue={childData?.bath}
                onChange={handleBathServiceChange}
              />
            </Box>
            <Box className="child-modal-form--control">
              <Typography className="child-modal-form--label">
                Entertainment:
              </Typography>
              <input
                type="text"
                className="child-modal-form--input"
                defaultValue={childData?.entertainment}
                onChange={handleEntertainmentChange}
              />
            </Box>
          </Box>
        </DialogContent>
        <DialogActions sx={{ justifyContent: "space-evenly", gap: "30px" }}>
          <Button
            onClick={handleClose}
            sx={{
              background: themeColors.adminSecondary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.adminSecondaryReverse,
              },
            }}
          >
            Back
          </Button>
          <Button
            onClick={handleUpdate}
            sx={{
              background: themeColors.adminPrimary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.numberAccount,
              },
            }}
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default UpdateRoomChildModal;
