/* eslint-disable react-hooks/exhaustive-deps */
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  ButtonProps,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { themeColors } from "../../../../../themes/schemes/PureLightTheme";
import { AddPhotoAlternateOutlined } from "@mui/icons-material";
import { addImage } from "../../../Admin.Api";
import { useSelector } from "react-redux";

const AddImageModal = ({ children }: { children: any }) => {
  const listImages = useSelector((state: any) => state.admin.listImages);
  const [open, setOpen] = useState(false);
  const fileInputRef = useRef<HTMLInputElement>(null);

  const [image, imageChange] = useState<string | ArrayBuffer | null>("");
  const [images, setImages] = useState(null);
  const [title, setTitle] = useState("");

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUploadImage = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleFileInputChange = (event: any) => {
    const file = event.target.files?.[0];
    if (file) {
      setImages(file);

      const reader = new FileReader();

      reader.onloadend = () => {
        imageChange(reader.result);
      };

      reader.readAsDataURL(file);
    }
  };

  const handleTitleChange = (e: any) => {
    setTitle(e.target.value);
  };

  const handleAddImage = async () => {
    let data = {
      images: images,
      title: title,
    };

    try {
      const res = await addImage(data.title, data.images);

      if (res) {
        setDisplaySuccess(true);
        setTimeout(() => {
          setOpen(false);
        }, 2000);
      } else {
        setDisplayError(true);
        setOpen(true);
      }
    } catch (error) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {}, [handleAddImage, fileInputRef, listImages]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}

      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        fullWidth
        maxWidth="md"
      >
        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Add fail. All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            width: "25%",
            position: "absolute",
            right: "10px",
            top: "10px",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Add successfully!
        </Alert>

        <DialogTitle display="flex" justifyContent="center">
          <Typography
            sx={{
              width: "fit-content",
              background: themeColors.gallery,
              backgroundClip: "text",
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "transparent",
              fontSize: "30px",
              fontWeight: 700,
            }}
          >
            Add Image To Gallery
          </Typography>
        </DialogTitle>

        <DialogContent dividers>
          <Box sx={{ display: "flex", flexDirection: "column", gap: "20px" }}>
            <Typography>Image:</Typography>
            <Box
              onClick={handleUploadImage}
              sx={{
                width: "100%",
                height: "auto",
                border: "1px dashed #bbbbbf",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                gap: "5px",
                p: "5px",
                "&:hover": {
                  cursor: "pointer",
                },
              }}
            >
              {image ? (
                <img
                  src={image as string}
                  alt="Uploaded"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                    borderRadius: "8px",
                    boxShadow: themeColors.boxShadow,
                  }}
                />
              ) : (
                <>
                  <AddPhotoAlternateOutlined />
                  <Typography sx={{ fontSize: "16px" }}>Add Image</Typography>
                </>
              )}
              <input
                ref={fileInputRef}
                type="file"
                style={{ display: "none" }}
                onChange={handleFileInputChange}
              />
            </Box>

            <Box className="modal-form--control">
              <Typography className="modal-form--label">Status:</Typography>
              <FormControl sx={{ flex: 1 }} fullWidth>
                <Select
                  value={title}
                  onChange={handleTitleChange}
                  displayEmpty
                  size="small"
                  sx={{ borderRadius: "8px" }}
                >
                  <MenuItem value="">
                    <em style={{ color: "#B2B2B2" }}>
                      -- Choose title for image --
                    </em>
                  </MenuItem>
                  <MenuItem value="Hotel View">Hotel View</MenuItem>
                  <MenuItem value="Rooms">Rooms</MenuItem>
                  <MenuItem value="Weddings">Weddings</MenuItem>
                  <MenuItem value="Spa">Spa</MenuItem>
                  <MenuItem value="Dining">Dining</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Box>
        </DialogContent>

        <DialogActions sx={{ justifyContent: "space-evenly", gap: "30px" }}>
          <Button
            onClick={handleClose}
            sx={{
              background: themeColors.adminSecondary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.adminSecondaryReverse,
              },
            }}
          >
            Cancel
          </Button>

          <Button
            onClick={handleAddImage}
            sx={{
              background: themeColors.adminPrimary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                background: themeColors.numberAccount,
              },
            }}
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AddImageModal;
