/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { themeColors } from "../../../../../themes/schemes/PureLightTheme";
import {
  ButtonProps,
  Modal,
  Box,
  Typography,
  Button,
  FormControl,
  MenuItem,
  Select,
  Alert,
  AlertTitle,
} from "@mui/material";
import "./UpdateCategory.Style.scss";
import { updateCategory } from "../../../Admin.Api";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  border: `1px solid ${themeColors.black}`,
  borderRadius: "8px",
  boxShadow: themeColors.boxShadow,
  p: 4,
  display: "flex",
  flexDirection: "column",
  gap: "30px",
};

const valueSize = ["Small", "Medium", "Large"];
const valueType = ["Single Bed", "Double Bed", "King Size Bed"];
const valueNumberCapacity = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const UpdateCategoryModal = ({
  children,
  data,
}: {
  children: any;
  data: any;
}) => {
  const [open, setOpen] = useState(false);
  const id = data?.id;
  const [size, setSize] = useState("");
  const [type, setType] = useState("");
  const [numberCapacity, setNumberCapacity] = useState(0);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorExists, setDisplayErrorExists] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSizeChange = (e: any) => {
    setSize(e.target.value);
  };

  const handleTypeChange = (e: any) => {
    setType(e.target.value);
  };

  const handleNumberCapacityChange = (e: any) => {
    setNumberCapacity(e.target.value);
  };

  const handleUpdate = async () => {
    try {
      const newSize = size === "" ? data?.size : size;
      const newType = type === "" ? data?.type : type;
      const newNumberCapacity =
        numberCapacity === 0 ? data?.number : numberCapacity;

      const res = await updateCategory(id, newSize, newType, newNumberCapacity);
      if (res) {
        setDisplaySuccess(true);
        setTimeout(() => {
          setOpen(false);
        }, 2000);
      } else {
        setDisplayErrorExists(true);
        setOpen(true);
      }
    } catch (error) {
      setDisplayError(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayErrorExists) {
      setTimeout(() => {
        setDisplayErrorExists(false);
      }, 5000);
    }
  }, [displayErrorExists]);

  useEffect(() => {}, [handleUpdate]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <Alert
            onClose={() => setDisplayErrorExists(false)}
            sx={{
              position: "absolute",
              right: "10px",
              top: "10px",
              display: displayErrorExists ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Update Fail</AlertTitle>
            Category already exists!
          </Alert>

          <Alert
            onClose={() => setDisplayError(false)}
            sx={{
              width: "30%",
              position: "absolute",
              right: "10px",
              top: "10px",
              display: displayError ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            Update fail!
          </Alert>

          <Alert
            onClose={() => setDisplaySuccess(false)}
            sx={{
              position: "absolute",
              right: "10px",
              top: "10px",
              display: displaySuccess ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="success"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
            Update successfully!
          </Alert>

          <Box display="flex" justifyContent="center">
            <Typography
              sx={{
                width: "fit-content",
                background: themeColors.categoryReverse,
                backgroundClip: "text",
                WebkitBackgroundClip: "text",
                WebkitTextFillColor: "transparent",
                fontSize: "30px",
                fontWeight: 700,
              }}
            >
              Update Category
            </Typography>
          </Box>

          <Box className="modal-form--control">
            <Typography className="form-modal--label">Size Of Room:</Typography>
            <FormControl sx={{ flex: 1 }} fullWidth>
              <Select
                defaultValue={data?.size}
                onChange={handleSizeChange}
                displayEmpty
                size="small"
                sx={{ borderRadius: "8px" }}
              >
                {valueSize?.map((item: any) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>

          <Box className="modal-form--control">
            <Typography className="form-modal--label">Type Of Bed:</Typography>
            <FormControl sx={{ flex: 1 }} fullWidth>
              <Select
                defaultValue={data?.type}
                onChange={handleTypeChange}
                displayEmpty
                size="small"
                sx={{ borderRadius: "8px" }}
              >
                {valueType?.map((item: any) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>

          <Box className="modal-form--control">
            <Typography className="form-modal--label">
              Maximum Capacity:
            </Typography>
            <FormControl sx={{ flex: 1 }} fullWidth>
              <Select
                defaultValue={data?.number}
                onChange={handleNumberCapacityChange}
                displayEmpty
                size="small"
                sx={{ borderRadius: "8px" }}
              >
                {valueNumberCapacity?.map((item: any) => (
                  <MenuItem key={item} value={item}>
                    {item}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Button
              onClick={handleClose}
              sx={{
                background: themeColors.adminSecondary,
                color: themeColors.white,
                border: `1px solid ${themeColors.black}`,
                fontWeight: "100",
                p: "5px 50px",
                fontSize: "15px",
                borderRadius: "8px",
                "&:hover": {
                  background: themeColors.adminSecondaryReverse,
                },
              }}
            >
              Cancel
            </Button>

            <Button
              onClick={handleUpdate}
              sx={{
                background: themeColors.adminPrimary,
                color: themeColors.white,
                border: `1px solid ${themeColors.black}`,
                fontWeight: "100",
                p: "5px 50px",
                fontSize: "15px",
                borderRadius: "8px",
                "&:hover": {
                  background: themeColors.numberAccount,
                },
              }}
            >
              Save
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default UpdateCategoryModal;
