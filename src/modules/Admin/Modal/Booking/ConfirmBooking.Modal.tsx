/* eslint-disable react-hooks/exhaustive-deps */
import {
  Alert,
  AlertTitle,
  Box,
  Button,
  ButtonProps,
  Modal,
  Typography,
} from "@mui/material";
import React, { useEffect } from "react";
import { useState } from "react";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import { confirmBooking } from "../../Admin.Api";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 700,
  bgcolor: "background.paper",
  border: `1px solid ${themeColors.black}`,
  borderRadius: "8px",
  boxShadow: themeColors.boxShadow,
  p: 4,
  display: "flex",
  flexDirection: "column",
  gap: "30px",
};

const ConfirmBookingModal = ({
  children,
  data,
}: {
  children: any;
  data: any;
}) => {
  const [open, setOpen] = useState(false);
  const id = data?.id;
  const status = true;

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleConfirmBooking = async () => {
    const res = await confirmBooking(id, status);
    if (res) {
      setDisplaySuccess(true);
      setTimeout(() => {
        setOpen(false);
      }, 2000);
    } else {
      setDisplayError(true);
      setOpen(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <Alert
            onClose={() => setDisplayError(false)}
            sx={{
              position: "absolute",
              right: "10px",
              top: "10px",
              display: displayError ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            Confirm booking fail!
          </Alert>

          <Alert
            onClose={() => setDisplaySuccess(false)}
            sx={{
              position: "absolute",
              right: "10px",
              top: "10px",
              display: displaySuccess ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="success"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
            Confirm booking successfully!
          </Alert>

          <Box display="flex" justifyContent="center">
            <Typography
              sx={{
                width: "fit-content",
                background: themeColors.bookingReverse,
                backgroundClip: "text",
                WebkitBackgroundClip: "text",
                WebkitTextFillColor: "transparent",
                fontSize: "30px",
                fontWeight: 700,
              }}
            >
              Confirm Booking
            </Typography>
          </Box>

          <Box>
            <Typography>
              Are you sure to confirm this booking with guest name is "
              {data?.name}" ?
            </Typography>
          </Box>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Button
              onClick={handleClose}
              sx={{
                background: themeColors.adminSecondary,
                color: themeColors.white,
                border: `1px solid ${themeColors.black}`,
                fontWeight: "100",
                p: "5px 50px",
                fontSize: "15px",
                borderRadius: "8px",
                "&:hover": {
                  background: themeColors.adminSecondaryReverse,
                },
              }}
            >
              Cancel
            </Button>

            <Button
              onClick={handleConfirmBooking}
              sx={{
                background: themeColors.adminPrimary,
                color: themeColors.white,
                border: `1px solid ${themeColors.black}`,
                fontWeight: "100",
                p: "5px 50px",
                fontSize: "15px",
                borderRadius: "8px",
                "&:hover": {
                  background: themeColors.numberAccount,
                },
              }}
            >
              Confirm
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ConfirmBookingModal;
