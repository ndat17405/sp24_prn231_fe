import {
  Alert,
  AlertTitle,
  Box,
  Button,
  FormControl,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import { useEffect, useState } from "react";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import "./Search.Style.scss";
import { getRoomBySearchAnd, getRoomBySearchOr } from "../Home.Api";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { saveListRoomBySearch } from "../Home.Action";
import { routes } from "../../../routes";
import { filterRoomIsEmptyList } from "../../../utils/helper";

const valueSize = ["Small", "Medium", "Large"];
const valueType = ["Single Bed", "Double Bed", "King Size Bed"];

const SearchPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [sizeRoom, setSizeRoom] = useState("");
  const [typeBed, setTypeBed] = useState("");

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayNotFound, setDisplayNotFound] = useState(false);
  const [displayError, setDisplayError] = useState(false);

  const handleSizeRoomChange = (e: any) => {
    setSizeRoom(e.target.value);
  };

  const handleTypeBedChange = (e: any) => {
    setTypeBed(e.target.value);
  };

  const handleSearch = async () => {
    try {
      if (sizeRoom === "" && typeBed === "") {
        const res = await getRoomBySearchAnd(sizeRoom, typeBed, dispatch);
        if (
          !filterRoomIsEmptyList(res) &&
          filterRoomIsEmptyList(res).length === 0
        ) {
          saveListRoomBySearch([]);
          setDisplayNotFound(true);
        }
      } else if (sizeRoom !== "" && typeBed === "") {
        const res = await getRoomBySearchOr(sizeRoom, dispatch);
        if (
          filterRoomIsEmptyList(res) &&
          filterRoomIsEmptyList(res).length !== 0
        ) {
          setDisplaySuccess(true);
          switch (window.location.pathname) {
            case "/":
              saveListRoomBySearch(filterRoomIsEmptyList(res));
              setTimeout(() => {
                navigate(routes.home.roomPage);
              }, 1500);
              break;
            case "/rooms":
              saveListRoomBySearch([]);
              break;
            default:
              saveListRoomBySearch([]);
              break;
          }
        } else {
          saveListRoomBySearch([]);
          setDisplayNotFound(true);
        }
      } else if (sizeRoom === "" && typeBed !== "") {
        const res = await getRoomBySearchOr(typeBed, dispatch);
        if (
          filterRoomIsEmptyList(res) &&
          filterRoomIsEmptyList(res).length !== 0
        ) {
          setDisplaySuccess(true);
          switch (window.location.pathname) {
            case "/":
              saveListRoomBySearch(filterRoomIsEmptyList(res));
              setTimeout(() => {
                navigate(routes.home.roomPage);
              }, 1500);
              break;
            case "/rooms":
              saveListRoomBySearch([]);
              break;
            default:
              saveListRoomBySearch([]);
              break;
          }
        } else {
          saveListRoomBySearch([]);
          setDisplayNotFound(true);
        }
      } else {
        const res = await getRoomBySearchAnd(sizeRoom, typeBed, dispatch);
        if (
          filterRoomIsEmptyList(res) &&
          filterRoomIsEmptyList(res).length !== 0
        ) {
          setDisplaySuccess(true);
          switch (window.location.pathname) {
            case "/":
              saveListRoomBySearch(filterRoomIsEmptyList(res));
              setTimeout(() => {
                navigate(routes.home.roomPage);
              }, 1500);
              break;
            case "/rooms":
              saveListRoomBySearch([]);
              break;
            default:
              saveListRoomBySearch([]);
              break;
          }
        } else {
          saveListRoomBySearch([]);
          setDisplayNotFound(true);
        }
      }
    } catch (error) {
      saveListRoomBySearch([]);
      setDisplayNotFound(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 2000);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayNotFound) {
      setTimeout(() => {
        setDisplayNotFound(false);
      }, 3000);
    }
  }, [displayNotFound]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 3000);
    }
  }, [displayError]);

  return (
    <BoxContainer property="search--container">
      <Alert
        onClose={() => setDisplaySuccess(false)}
        sx={{
          position: "absolute",
          right: "3%",
          top: "25%",
          display: displaySuccess ? "flex" : "none",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="success"
      >
        <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
        Search successfully!
      </Alert>

      <Alert
        onClose={() => setDisplayNotFound(false)}
        sx={{
          position: "absolute",
          right: "3%",
          top: "25%",
          display: displayNotFound ? "flex" : "none",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="info"
      >
        <AlertTitle sx={{ fontWeight: "bold" }}>Info</AlertTitle>
        Data not found!
      </Alert>

      <Alert
        onClose={() => setDisplayError(false)}
        sx={{
          position: "absolute",
          right: "3%",
          top: "25%",
          display: displayError ? "flex" : "none",
          alignItems: "center",
          borderRadius: "8px",
        }}
        variant="filled"
        severity="error"
      >
        <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
        Search fail!
      </Alert>

      <Typography className="search__title">
        Your Perfect Stay at the Best Price
      </Typography>

      <Box className="search__form--container">
        <Box className="search__form--select">
          <Typography className="label">Size of rooms</Typography>
          <FormControl fullWidth>
            <Select
              value={sizeRoom}
              onChange={handleSizeRoomChange}
              displayEmpty
              size="small"
              sx={{ borderRadius: "8px" }}
            >
              <MenuItem value="">
                <em style={{ color: "#B2B2B2" }}>-- Choose size of rooms --</em>
              </MenuItem>
              {valueSize?.map((item: any) => (
                <MenuItem key={item} value={item}>
                  {item}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>

        <Box className="search__form--select">
          <Typography className="label">Type of beds</Typography>
          <FormControl fullWidth>
            <Select
              value={typeBed}
              onChange={handleTypeBedChange}
              displayEmpty
              size="small"
              sx={{ borderRadius: "8px" }}
            >
              <MenuItem value="">
                <em style={{ color: "#B2B2B2" }}>-- Choose type of beds --</em>
              </MenuItem>
              {valueType?.map((item: any) => (
                <MenuItem key={item} value={item}>
                  {item}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>

        <Button
          onClick={handleSearch}
          sx={{
            height: "calc(73.125px - (21px + 15px))",
            backgroundColor: themeColors.btnPrimary,
            color: themeColors.white,
            p: "0 50px 0 50px",
            fontWeight: 400,
            border: "1px solid black",
            borderRadius: "20px",
            "&:hover": {
              backgroundColor: themeColors.thirdary,
            },
          }}
        >
          Search
        </Button>
      </Box>
    </BoxContainer>
  );
};

export default SearchPage;
