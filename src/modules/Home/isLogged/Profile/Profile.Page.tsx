import { Alert, AlertTitle, Box, Button, Typography } from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import IsLoggedContainer from "../../../../layouts/Header/User/IsLogged/IsLogged.Container";
import FooterComponent from "../../../../layouts/Footer/FooterComponent";
import Sidebar from "../../../../layouts/Drawer/User/Sidebar";
import "./Profile.Style.scss";
import { useEffect, useState } from "react";
import { getProfileByAccountId, updateProfile } from "../../Home.Api";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../../routes";
import secureLocalStorage from "react-secure-storage";

const ProfilePage = () => {
  const navigate = useNavigate();
  const regexPhone = /^0[3|5|7|8|9]\d{8}$/;
  const [data, setData] = useState<any>([]);
  const [fname, setFname] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayAvoidSpam, setDisplayAvoidSpam] = useState(false);
  const [displayValidate, setDisplayValidate] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleFnameChange = (e: any) => {
    setFname(e.target.value);
  };

  const handlePhoneChange = (e: any) => {
    setPhone(e.target.value);
  };

  const handleAddressChange = (e: any) => {
    setAddress(e.target.value);
  };

  const backToHome = () => {
    navigate(routes.home.root);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleUpdateProfile();
    }
  };

  const init = async () => {
    const res = await getProfileByAccountId(
      secureLocalStorage.getItem("accountID")
    );
    if (res) {
      setData(res);
    }
  };

  const handleUpdateProfile = async () => {
    const newData = {
      fullName: fname || data?.profile?.fullName,
      phone: phone || data?.profile?.phone,
      address: address || data?.profile?.address,
    };

    if (
      newData?.fullName === "" ||
      newData?.phone === "" ||
      newData?.address === ""
    ) {
      setDisplayErrorWhenNull(true);
    } else if (
      newData?.fullName !== data?.profile?.fullName ||
      newData?.phone !== data?.profile?.phone ||
      newData?.address !== data?.profile?.address
    ) {
      if (!regexPhone.test(phone)) {
        setDisplayValidate(true);
      } else {
        try {
          const res = await updateProfile(
            secureLocalStorage.getItem("accountID"),
            newData
          );
          if (res) {
            setDisplaySuccess(true);
          } else {
            setDisplayError(true);
          }
        } catch (error) {
          setDisplayError(true);
        }
      }
    } else {
      setDisplayAvoidSpam(true);
    }
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {
    if (displayAvoidSpam) {
      setTimeout(() => {
        setDisplayAvoidSpam(false);
      }, 5000);
    }
  }, [displayAvoidSpam]);

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 2000);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayValidate) {
      setTimeout(() => {
        setDisplayValidate(false);
      }, 5000);
    }
  }, [displayValidate]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  return (
    <BoxContainer property="profile--container">
      <IsLoggedContainer />
      <Box className="profile--wrapper">
        <Sidebar />
        <Box className="wrapper__content">
          <Alert
            onClose={() => setDisplayValidate(false)}
            sx={{
              minWidth: "18%",
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayValidate ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Invalid phone</AlertTitle>
            Example:
            <ul>
              <li>03 + 8 digit</li>
              <li>05 + 8 digit</li>
              <li>07 + 8 digit</li>
              <li>08 + 8 digit</li>
              <li>09 + 8 digit</li>
            </ul>
          </Alert>

          <Alert
            onClose={() => setDisplayAvoidSpam(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayAvoidSpam ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            You must change profile before update!
          </Alert>

          <Alert
            onClose={() => setDisplayErrorWhenNull(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayErrorWhenNull ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            All field are required!
          </Alert>

          <Alert
            onClose={() => setDisplayError(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayError ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            Update profile fail!
          </Alert>

          <Alert
            onClose={() => setDisplaySuccess(false)}
            sx={{
              maxWidth: "25%",
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displaySuccess ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
            }}
            variant="filled"
            severity="success"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
            Update profile successfully!
          </Alert>
          <Typography className="content__title--profile">
            Information
          </Typography>
          <Box className="content__infor">
            <form action="" onKeyDown={handleKeyDown}>
              <Box className="infor--control">
                <label htmlFor="fname">Fullname</label>
                <input
                  type="text"
                  name="fname"
                  id="fname"
                  defaultValue={data?.profile?.fullName}
                  onChange={handleFnameChange}
                />
              </Box>

              <Box className="infor--control">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  name="email"
                  id="email"
                  defaultValue={data?.email}
                  disabled
                />
              </Box>

              <Box className="infor--control">
                <label htmlFor="phone">Phone</label>
                <input
                  type="number"
                  name="phone"
                  id="phone"
                  placeholder="09xxxxxxxx"
                  defaultValue={data?.profile?.phone}
                  onChange={handlePhoneChange}
                />
              </Box>

              <Box className="infor--control">
                <label htmlFor="address">Address</label>
                <input
                  type="text"
                  name="address"
                  id="address"
                  placeholder="Can Tho, Viet Nam"
                  defaultValue={data?.profile?.address}
                  onChange={handleAddressChange}
                />
              </Box>

              <Box className="info--btn">
                <Button className="btn--back" onClick={backToHome}>
                  Back to home
                </Button>
                <Button className="btn--save" onClick={handleUpdateProfile}>
                  Save
                </Button>
              </Box>
            </form>
          </Box>
        </Box>
      </Box>
      <FooterComponent />
    </BoxContainer>
  );
};

export default ProfilePage;
