import {
  Box,
  Typography,
  Stack,
  ListItem,
  ListItemButton,
} from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import Sidebar from "../../../../layouts/Drawer/User/Sidebar";
import FooterComponent from "../../../../layouts/Footer/FooterComponent";
import IsLoggedContainer from "../../../../layouts/Header/User/IsLogged/IsLogged.Container";
import "./HistoryBooking.Style.scss";
import { useEffect, useState } from "react";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import AllBookingComponent from "./BookingList/AllBooking/AllBooking.Component";
import CompletedBookingComponent from "./BookingList/CompletedBooking/CompletedBooking.Component";
import PendingBookingComponent from "./BookingList/PendingBooking/PendingBooking.Component";

const filterItems = ["All", "Pending", "Completed"];

const HistoryBookingPage = () => {
  const [selectFilterItems, setSelectFilterItems] = useState(0);

  const handleFilterItemClicked = (index: any) => {
    setSelectFilterItems(index);
  };

  useEffect(() => {}, [selectFilterItems]);

  return (
    <BoxContainer property="history-booking--container">
      <IsLoggedContainer />
      <Box className="history-booking--wrapper">
        <Sidebar />
        <Box className="wrapper__content">
          <Typography className="content__title--booking">
            History Booking
          </Typography>
          <Box className="info__booking--container">
            <Stack
              direction="row"
              justifyContent="space-between"
              p="20px 0"
              bgcolor={themeColors.navLinks}
              borderRadius="8px 8px 0 0"
              borderBottom={`1px solid ${themeColors.black}`}
            >
              {filterItems.map((item, index) => (
                <ListItem
                  key={item}
                  sx={{ width: "33.333333%", justifyContent: "center" }}
                >
                  <ListItemButton
                    onClick={() => handleFilterItemClicked(index)}
                    sx={{
                      maxWidth: "70%",
                      backgroundColor:
                        index === selectFilterItems
                          ? themeColors.sideBar
                          : "inherit",
                      borderRadius: "16px",
                    }}
                  >
                    <Typography
                      sx={{
                        width: "100%",
                        textAlign: "center",
                        color:
                          index === selectFilterItems
                            ? themeColors.white
                            : themeColors.black,
                        fontSize: "24px",
                      }}
                    >
                      {item}
                    </Typography>
                  </ListItemButton>
                </ListItem>
              ))}
            </Stack>
            {selectFilterItems === 0 ? <AllBookingComponent /> : null}
            {selectFilterItems === 1 ? <PendingBookingComponent /> : null}
            {selectFilterItems === 2 ? <CompletedBookingComponent /> : null}
          </Box>
        </Box>
      </Box>
      <FooterComponent />
    </BoxContainer>
  );
};

export default HistoryBookingPage;
