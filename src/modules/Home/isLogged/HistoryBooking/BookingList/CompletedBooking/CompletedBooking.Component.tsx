import {
  Box,
  Typography,
  Button,
  Grid,
  Stack,
  Pagination,
} from "@mui/material";
import { useState, useEffect } from "react";
import BoxContainer from "../../../../../../components/Box/Box.Container";
import { getBookingByAccountId } from "../../../../Home.Api";
import BookingDetailModal from "../../../../Modal/BookingDetail/BookingDetail.Modal";
import { filterBookingIsCompleted } from "../../../../../../utils/helper";
import { themeColors } from "../../../../../../themes/schemes/PureLightTheme";
import { LoadingButton } from "@mui/lab";
import secureLocalStorage from "react-secure-storage";

const CompletedBookingComponent = () => {
  const [data, setData] = useState<any>([]);
  const [currentPage, setCurrentPage] = useState(1);

  const bookingPerPage = 2;
  const indexOfLastBooking = currentPage * bookingPerPage;
  const indexOfFirstBooking = indexOfLastBooking - bookingPerPage;
  const currentBookings = data.slice(indexOfFirstBooking, indexOfLastBooking);
  const totalPageBooking = Math.ceil(data?.length / bookingPerPage);

  const paginate = (event: any, value: any) => {
    event.preventDefault();
    setCurrentPage(value);
    document.documentElement.scrollTop = 200;
  };

  // Hàm tự định dạng ngày tháng
  const formatDate = (dateString: string | undefined): string => {
    if (!dateString) return ""; // Trả về chuỗi rỗng nếu không có ngày tháng

    const date = new Date(dateString); // Chuyển đổi chuỗi ngày tháng thành đối tượng Date
    if (isNaN(date.getTime())) return ""; // Trả về chuỗi rỗng nếu chuỗi không hợp lệ

    const day = date.getDate().toString().padStart(2, "0"); // Lấy ngày và thêm '0' phía trước nếu cần
    const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Lấy tháng và thêm '0' phía trước nếu cần
    const year = date.getFullYear();

    return `${day}/${month}/${year}`;
  };

  const init = async () => {
    const res = await getBookingByAccountId(
      secureLocalStorage.getItem("accountID")
    );
    if (res) {
      setData(filterBookingIsCompleted(res));
    }
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [currentPage]);

  return (
    <BoxContainer property="all-booking--container">
      {currentBookings.map((data: any, index: any) => (
        <Box className="all-booking__box" key={index}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignContent: "center",
              p: "15px",
              borderBottom: "1px solid #AAAAAA",
              backgroundColor: "#E3EDFF",
              borderRadius: "8px 8px 0 0",
            }}
          >
            <Typography className="booking--code">
              Code: 744163{data?.bookingID}
            </Typography>
            <Typography
              className="booking--status"
              sx={{
                color: data?.status
                  ? themeColors.statusPositive
                  : themeColors.statusNegative,
              }}
            >
              {data?.status ? "Completed" : "Wait for confirm"}
            </Typography>
          </Box>

          <Box className="booking--info">
            <img src={`data:image/*;base64,${data?.rooms?.images}`} alt="" />
            <Box className="detail">
              <Typography className="room-name">
                {data?.rooms?.roomsName}
              </Typography>
              <Typography className="date">
                Check-in: {formatDate(data?.checkInDate)}
              </Typography>
              <Typography className="date">
                Check-out: {formatDate(data?.checkOutDate)}
              </Typography>
            </Box>
          </Box>

          <Box className="booking--view-detail">
            <BookingDetailModal
              data={{
                id: data?.bookingID,
                imgRoom: data?.rooms?.images,
                roomName: data?.rooms?.roomsName,
                checkInDate: formatDate(data?.checkInDate),
                checkOutDate: formatDate(data?.checkOutDate),
                price: data?.totalPrice,
                status: data?.status,
                nameCustomer: data?.account?.profile?.fullName,
                emailCustomer: data?.account?.email,
                phone: data?.account?.profile?.phone,
                numberMember: data?.rooms?.category?.numberOfMember,
              }}
            >
              <Button className="btn--view-detail">View Detail</Button>
            </BookingDetailModal>
          </Box>
        </Box>
      ))}
      <Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          {data?.length > 0 && (
            <Grid item xs={6} sx={{ alignItems: "center" }}>
              <label>
                <b>
                  Showing {currentPage} of {totalPageBooking}{" "}
                  {totalPageBooking > 1 ? "pages" : "page"}
                </b>
              </label>
            </Grid>
          )}
          <Stack sx={{ alignItems: "center" }}>
            {data?.length > 1 && (
              <Pagination
                color="standard"
                variant="outlined"
                defaultPage={1}
                count={totalPageBooking}
                page={currentPage}
                onChange={paginate}
                size="medium"
                showFirstButton
                showLastButton
              />
            )}
          </Stack>
          {data?.length === 0 && (
            <Box
              width="100%"
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <LoadingButton
                loading
                variant="outlined"
                sx={{ border: "0 !important" }}
              />
              <Typography
                className="color-change-3x"
                sx={{
                  fontSize: "20px",
                  fontWeight: 400,
                }}
              >
                Loading...
              </Typography>
            </Box>
          )}
        </Box>
      </Box>
    </BoxContainer>
  );
};

export default CompletedBookingComponent;
