import { Box, Typography, Button, Alert, AlertTitle } from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import Sidebar from "../../../../layouts/Drawer/User/Sidebar";
import FooterComponent from "../../../../layouts/Footer/FooterComponent";
import IsLoggedContainer from "../../../../layouts/Header/User/IsLogged/IsLogged.Container";
import "./ChangePass.Style.scss";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../../routes";
import { changePassword } from "../../Home.Api";
import { VisibilityOff, Visibility } from "@mui/icons-material";
import secureLocalStorage from "react-secure-storage";

const ChangePassPage = () => {
  const navigate = useNavigate();
  const regexPassword =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,16}$/;
  const [oldPass, setOldPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [confNewPass, setConfNewPass] = useState("");

  const [showOldPassword, setShowOldPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfPassword, setShowConfPassword] = useState(false);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorDuplicate, setDisplayErrorDuplicate] = useState(false);
  const [displayValidate, setDisplayValidate] = useState(false);
  const [displayErrorOldPass, setDisplayErrorOldPass] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleOldPassChange = (e: any) => {
    setOldPass(e.target.value);
  };

  const handleNewPassChange = (e: any) => {
    setNewPass(e.target.value);
  };

  const handleConfNewPassChange = (e: any) => {
    setConfNewPass(e.target.value);
  };

  const handleShowOldPassword = () => {
    setShowOldPassword((showOldPassword) => !showOldPassword);
  };

  const handleShowNewPassword = () => {
    setShowNewPassword((showNewPassword) => !showNewPassword);
  };

  const handleShowConfPassword = () => {
    setShowConfPassword((showConfPassword) => !showConfPassword);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleChangePassword();
    }
  };

  const handleChangePassword = async () => {
    if (oldPass === "" || newPass === "" || confNewPass === "") {
      setDisplayErrorWhenNull(true);
    } else {
      try {
        if (!regexPassword.test(newPass) || !regexPassword.test(confNewPass)) {
          setDisplayValidate(true);
        } else {
          if (confNewPass.trim() === newPass.trim()) {
            if (
              newPass.trim() === oldPass.trim() ||
              confNewPass.trim() === oldPass.trim()
            ) {
              setDisplayErrorDuplicate(true);
            } else {
              const data = {
                id: secureLocalStorage.getItem("accountID"),
                oldPassword: oldPass,
                newPassword: newPass,
              };

              const res = await changePassword(data);

              if (res) {
                setDisplaySuccess(true);
              } else {
                setDisplayErrorOldPass(true);
              }
            }
          } else {
            setDisplayError(true);
          }
        }
      } catch (error) {
        setDisplayErrorOldPass(true);
      }
    }
  };

  const backToHome = () => {
    navigate(routes.home.root);
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 2500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayValidate) {
      setTimeout(() => {
        setDisplayValidate(false);
      }, 6000);
    }
  }, [displayValidate]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayErrorDuplicate) {
      setTimeout(() => {
        setDisplayErrorDuplicate(false);
      }, 5000);
    }
  }, [displayErrorDuplicate]);

  useEffect(() => {
    if (displayErrorOldPass) {
      setTimeout(() => {
        setDisplayErrorOldPass(false);
      }, 5000);
    }
  }, [displayErrorOldPass]);

  return (
    <BoxContainer property="change-pass--container">
      <IsLoggedContainer />
      <Box className="change-pass--wrapper">
        <Sidebar />
        <Box className="wrapper__content">
          <Alert
            onClose={() => setDisplayValidate(false)}
            sx={{
              minWidth: "27%",
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayValidate ? "flex" : "none",
              alignItems: "center",
              borderRadius: "8px",
              zIndex: "99999",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>
              Invalid password
            </AlertTitle>
            Password must have at least{" "}
            <ul>
              <li>
                <strong>One lowercase letter</strong>
              </li>
              <li>
                <strong>One uppercase letter</strong>
              </li>
              <li>
                <strong>One number</strong>
              </li>
              <li>
                <strong>One special letter</strong>
              </li>
              <li>
                <strong>From 8 to 16 characters</strong>
              </li>
            </ul>
          </Alert>

          <Alert
            onClose={() => setDisplayErrorDuplicate(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayErrorDuplicate ? "flex" : "none",
              alignItems: "center",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            New password and Confirm password can not be equal Old password!
          </Alert>

          <Alert
            onClose={() => setDisplayErrorWhenNull(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayErrorWhenNull ? "flex" : "none",
              alignItems: "center",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            Are field are required!
          </Alert>

          <Alert
            onClose={() => setDisplayError(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayError ? "flex" : "none",
              alignItems: "center",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            Confirm password must be equal new password. Please enter again!
          </Alert>

          <Alert
            onClose={() => setDisplayErrorOldPass(false)}
            sx={{
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displayErrorOldPass ? "flex" : "none",
              alignItems: "center",
            }}
            variant="filled"
            severity="error"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
            Old password is incorrect. Please enter again!
          </Alert>

          <Alert
            onClose={() => setDisplaySuccess(false)}
            sx={{
              maxWidth: "25%",
              position: "absolute",
              right: "9%",
              top: "25%",
              display: displaySuccess ? "flex" : "none",
              alignItems: "center",
            }}
            variant="filled"
            severity="success"
          >
            <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
            Change password successfully!
          </Alert>
          <Typography className="content__title--change-pass">
            Change Password
          </Typography>
          <Box className="content__infor">
            <form action="" onKeyDown={handleKeyDown}>
              <Box className="infor--control control-pass">
                <label htmlFor="old-pass">Old Password</label>
                <input
                  type={showOldPassword ? "text" : "password"}
                  name="old-pass"
                  id="old-pass"
                  placeholder="Enter old password"
                  autoFocus
                  onChange={handleOldPassChange}
                />
                <div className="icon" onClick={handleShowOldPassword}>
                  {showOldPassword ? <VisibilityOff /> : <Visibility />}
                </div>
              </Box>

              <Box className="infor--control control-pass">
                <label htmlFor="new-pass">New Password</label>
                <input
                  type={showNewPassword ? "text" : "password"}
                  name="new-pass"
                  id="new-pass"
                  placeholder="Enter new password"
                  onChange={handleNewPassChange}
                />
                <div className="icon" onClick={handleShowNewPassword}>
                  {showNewPassword ? <VisibilityOff /> : <Visibility />}
                </div>
              </Box>

              <Box className="infor--control control-pass">
                <label htmlFor="confirm-pass">Confirm Password</label>
                <input
                  type={showConfPassword ? "text" : "password"}
                  name="confirm-pass"
                  id="confirm-pass"
                  placeholder="Enter again new password"
                  onChange={handleConfNewPassChange}
                />
                <div className="icon" onClick={handleShowConfPassword}>
                  {showConfPassword ? <VisibilityOff /> : <Visibility />}
                </div>
              </Box>

              <Box className="info--btn">
                <Button className="btn--back" onClick={backToHome}>
                  Back to home
                </Button>
                <Button className="btn--save" onClick={handleChangePassword}>
                  Save
                </Button>
              </Box>
            </form>
          </Box>
        </Box>
      </Box>
      <FooterComponent />
    </BoxContainer>
  );
};

export default ChangePassPage;
