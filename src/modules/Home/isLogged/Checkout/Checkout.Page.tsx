import {
  Alert,
  AlertTitle,
  Box,
  Button,
  Divider,
  Typography,
} from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import "./Checkout.Style.scss";
import IsLoggedContainer from "../../../../layouts/Header/User/IsLogged/IsLogged.Container";
import FooterComponent from "../../../../layouts/Footer/FooterComponent";
import { AssetImages } from "../../../../utils/images";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../../routes";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import {
  createBooking,
  getAllRooms,
  getProfileByAccountId,
} from "../../Home.Api";
import { filterRoomIsEmptyList } from "../../../../utils/helper";
import RoomDetailDialog from "../../Modal/RoomDetail/RoomDetail.Dialog";
import { differenceInCalendarDays } from "date-fns";
import secureLocalStorage from "react-secure-storage";
import { optionsData } from "../../../../utils/fake/data";

const CheckoutPage = () => {
  let numberOfNights = 0;

  const navigate = useNavigate();
  const room = useSelector((state: any) => state.home.room);
  const [dataAccount, setDataAccount] = useState<any>([]);
  const [fname, setFname] = useState("");
  const email = dataAccount?.email;
  const [phone, setPhone] = useState("");
  const [checkIn, setCheckIn] = useState("");
  const [checkOut, setCheckOut] = useState("");
  const [dataRoom, setDataRoom] = useState<any>([]);
  const listRoomsIsEmpty = filterRoomIsEmptyList(dataRoom);

  const sortedRooms = [...listRoomsIsEmpty].sort((a, b) => a.price - b.price);
  const threeCheapestRooms = sortedRooms.slice(0, 3);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayMustUpdateProfile, setDisplayMustUpdateProfile] =
    useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const formatTotal = (total: any) => parseFloat(total).toFixed(2);

  if (checkIn && checkOut) {
    numberOfNights = differenceInCalendarDays(
      new Date(checkOut),
      new Date(checkIn)
    );
  }

  const handleFnameChange = (e: any) => {
    setFname(e.target.value);
  };

  const handlePhoneChange = (e: any) => {
    setPhone(e.target.value);
  };

  const handleCheckInChange = (e: any) => {
    setCheckIn(e.target.value);
    if (checkOut && checkIn >= checkOut) {
      setCheckOut("");
    }
  };

  const handleCheckOutChange = (e: any) => {
    const selectedDate = e.target.value;
    const currentDate = new Date().toISOString().split("T")[0];
    if (selectedDate <= checkIn || selectedDate < currentDate) {
      setCheckOut("");
    } else {
      setCheckOut(selectedDate);
    }
  };

  const goToRoot = () => {
    navigate(routes.home.root);
  };

  const goToRoom = () => {
    navigate(routes.home.roomPage);
  };

  const init = async () => {
    const resAccount = await getProfileByAccountId(
      secureLocalStorage.getItem("accountID")
    );
    const resRoom = await getAllRooms();
    if (resAccount && resRoom) {
      setDataAccount(resAccount);
      setDataRoom(resRoom);
    }
  };

  const handleBooking = async () => {
    const check = {
      fullname: fname || dataAccount?.profile?.fullName,
      phone: phone || dataAccount?.profile?.phone,
      checkIn: checkIn,
      checkOut: checkOut,
    };

    if (check.checkIn === "" || check.checkOut === "") {
      document.documentElement.scrollTop = 0;
      setDisplayErrorWhenNull(true);
    } else {
      if (check.fullname === "" || check.phone === "") {
        document.documentElement.scrollTop = 0;
        setDisplayMustUpdateProfile(true);
        setTimeout(() => {
          navigate(routes.home.profilePage);
        }, 3000);
      } else {
        const response = await createBooking(
          secureLocalStorage.getItem("accountID"),
          room?.roomsID,
          checkIn,
          checkOut,
          formatTotal(
            numberOfNights * room?.price + numberOfNights * room?.price * 0.05
          )
        );

        if (response) {
          document.documentElement.scrollTop = 0;
          setDisplaySuccess(true);
          setTimeout(() => {
            navigate(routes.home.root);
          }, 2000);
        } else {
          document.documentElement.scrollTop = 0;
          setDisplayError(false);
        }
      }
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayMustUpdateProfile) {
      setTimeout(() => {
        setDisplayMustUpdateProfile(false);
      }, 3000);
    }
  }, [displayMustUpdateProfile]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    init();
  }, [dataAccount, dataRoom]);

  useEffect(() => {}, [listRoomsIsEmpty, threeCheapestRooms]);

  return (
    <BoxContainer property="checkout--container">
      <IsLoggedContainer />
      <Box className="checkout--wrapper">
        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "2.5%",
            top: "21%",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplayMustUpdateProfile(false)}
          sx={{
            position: "absolute",
            right: "2.5%",
            top: "21%",
            display: displayMustUpdateProfile ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="warning"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Warning</AlertTitle>
          You must update profile before booking!
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            position: "absolute",
            right: "2.5%",
            top: "21%",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Booking fail!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "2.5%",
            top: "21%",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Booking successfully!
        </Alert>

        <Box className="room--detail">
          <Box className="detail__nav">
            <Typography className="nav__item text" onClick={goToRoot}>
              Home
            </Typography>
            <Typography className="arrow text">&gt;</Typography>
            <Typography className="nav__item text" onClick={goToRoom}>
              Rooms
            </Typography>
            <Typography className="arrow text">&gt;</Typography>
            <Typography className="current-path text">Booking</Typography>
          </Box>

          <Box className="detail__title">
            <Typography
              sx={{
                color: themeColors.black,
                fontWeight: "bold",
                fontSize: 30,
              }}
            >
              Choose Your Room
            </Typography>
          </Box>

          <Box className="detail__room">
            <Box className="choosed-room">
              <img src={`data:image/*;base64,${room?.images}`} alt="" />
              <Box className="choosed-room__info">
                <Typography className="info__name">
                  {room?.roomsName}
                </Typography>
                <Box className="info__bed">
                  <img src={AssetImages.ICONS.CHECKOUT.BED} alt="" />
                  <Typography className="bed__number">
                    {room?.category?.typeOfBed === "Single Bed" ||
                    room?.category?.typeOfBed === "Double Bed"
                      ? "1 x"
                      : "2 x"}
                  </Typography>
                  <Typography className="bed__type">
                    {room?.category?.typeOfBed}
                  </Typography>
                </Box>

                <Box className="info__service">
                  <Box className="service__number-member">
                    {room?.category?.numberOfMember > 1
                      ? `${room?.category?.numberOfMember} pers max`
                      : `${room?.category?.numberOfMember} per max`}
                  </Box>

                  <Box className="service__view">City View</Box>
                </Box>

                <Box className="info__price">$ {formatTotal(room?.price)}</Box>
              </Box>
            </Box>

            <Box className="divider--options">
              <Divider
                sx={{
                  width: "40%",
                  height: "1px",
                  backgroundColor: themeColors.btnSecondary,
                }}
              />
              <Typography className="choose-option">
                Choose your options
              </Typography>
              <Divider
                sx={{
                  width: "40%",
                  height: "1px",
                  backgroundColor: themeColors.btnSecondary,
                }}
              />
            </Box>

            <Box className="options--wrapper">
              {optionsData.map((data: any, index: any) => (
                <Box className="options__item" key={index}>
                  <img src={data.img} alt="" />
                  <Box className="options__item--info">
                    <Typography className="item__title">
                      {data.title}
                    </Typography>
                    <Typography className="item__detail">
                      {data.detail}
                    </Typography>
                    <Box className="item__box">
                      <Typography className="box__price">
                        ${data.price}
                      </Typography>
                      <Box className="item__btn">
                        <input
                          type="checkbox"
                          name={data.title}
                          id={data.title}
                        />
                        <label htmlFor={data.title}>Add</label>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              ))}
            </Box>
          </Box>

          <Box className="detail__another-room">
            {threeCheapestRooms?.map((item: any, index: any) => {
              if (index > 3) {
                return <></>;
              } else {
                return (
                  <Box className="another-room__wrapper">
                    <Box className="wrapper__item">
                      <img src={`data:image/*;base64,${item?.images}`} alt="" />
                      <Box className="item__info">
                        <Typography className="item--name">
                          {item?.roomsName}
                        </Typography>

                        <Box className="item--bed">
                          <img src={AssetImages.ICONS.CHECKOUT.BED} alt="" />
                          <Typography className="bed__number">
                            {item?.category?.typeOfBed === "Single Bed" ||
                            item?.category?.typeOfBed === "Double Bed"
                              ? "1 x"
                              : "2 x"}
                          </Typography>
                          <Typography className="bed__type">
                            {item?.category?.typeOfBed}
                          </Typography>
                        </Box>

                        <Box className="item--service">
                          <Box className="service__number-member">
                            {item?.category?.numberOfMember > 1
                              ? `${item?.category?.numberOfMember} pers max`
                              : `${item?.category?.numberOfMember} per max`}
                          </Box>

                          <Box className="service__view">City View</Box>
                        </Box>

                        <Typography className="item--price">
                          $ {formatTotal(item?.price)}
                        </Typography>
                      </Box>
                    </Box>
                    <Box className="wrapper__btn">
                      <RoomDetailDialog
                        data={{
                          id: item?.roomsID,
                          name: item?.roomsName,
                          image: item?.images,
                          bedsAndBeding: item?.bedsAndBeding,
                          internetAndPhones: item?.internetAndPhones,
                          bathAndBathroom: item?.bathAndBathroom,
                          entertainment: item?.entertainment,
                          size: item?.category?.sizeOfRooms,
                        }}
                      >
                        <Button className="btn--detail">View Detail</Button>
                      </RoomDetailDialog>
                    </Box>
                  </Box>
                );
              }
            })}
          </Box>
        </Box>

        <Box className="checkout--detail">
          <Box className="checkout__hotel--container">
            <Box className="detail__hotel">
              <Box className="hotel__info">
                <img src={AssetImages.LOGO_HOTEL} alt="" />
                <Typography className="info__name">
                  EPOSH Hotel Luxury - Can Tho
                </Typography>
                <Box className="info__stars">
                  <img
                    src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR}
                    alt=""
                  />
                  <img
                    src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR}
                    alt=""
                  />
                  <img
                    src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR}
                    alt=""
                  />
                  <img
                    src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR}
                    alt=""
                  />
                  <img
                    src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR}
                    alt=""
                  />
                </Box>
              </Box>
              <img src={AssetImages.EPOSH_HOTEL} alt="" />
            </Box>

            <Box className="checkout__service--icons">
              <img src={AssetImages.ICONS.CHECKOUT.SWIMMING_POOL} alt="" />
              <img src={AssetImages.ICONS.CHECKOUT.CAR_PARK} alt="" />
              <img src={AssetImages.ICONS.CHECKOUT.RESTAURANT} alt="" />
              <img src={AssetImages.ICONS.CHECKOUT.ACCESSIBILITY} alt="" />
              <img src={AssetImages.ICONS.CHECKOUT.WIFI} alt="" />
            </Box>

            <Box className="checkout__calendar">
              <img src={AssetImages.ICONS.CHECKOUT.CLOCK} alt="" />
              <Typography
                sx={{ color: "white", fontSize: 18, fontWeight: 400 }}
              >
                Open in 00:00 AM | Closed 00:00 PM
              </Typography>
            </Box>
          </Box>

          <Divider
            sx={{
              backgroundColor: themeColors.divider,
              m: "20px 0",
            }}
          />

          <Box className="detail__booker--container">
            <Typography className="booker__title">
              Booker Information
            </Typography>
            <Box className="booker__input">
              <Box className="input--control">
                <label htmlFor="fname">Name</label>
                <input
                  type="text"
                  name="fname"
                  id="fname"
                  defaultValue={dataAccount?.profile?.fullName}
                  onChange={handleFnameChange}
                />
              </Box>

              <Box className="input--control">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  name="email"
                  id="email"
                  defaultValue={email}
                  disabled
                />
              </Box>

              <Box className="input--control">
                <label htmlFor="phone">Phone</label>
                <input
                  type="text"
                  name="phone"
                  id="phone"
                  placeholder="09xxxxxxxx"
                  defaultValue={dataAccount?.profile?.phone}
                  onChange={handlePhoneChange}
                />
              </Box>
            </Box>
          </Box>

          <Divider
            sx={{
              backgroundColor: themeColors.divider,
              m: "20px 0",
            }}
          />

          <Box className="detail__date--container">
            <Box className="date__input">
              <Box className="input-icon--control">
                <Box className="icon--label">
                  <img src={AssetImages.ICONS.CHECKOUT.CALENDAR} alt="" />
                  <label htmlFor="check-in">Check-in</label>
                </Box>
                <input
                  type="date"
                  name="check-in"
                  id="check-in"
                  value={checkIn}
                  min={new Date().toISOString().split("T")[0]}
                  onChange={handleCheckInChange}
                />
              </Box>

              <Box className="input-icon--control">
                <Box className="icon--label">
                  <img src={AssetImages.ICONS.CHECKOUT.CALENDAR} alt="" />
                  <label htmlFor="check-out">Check-out</label>
                </Box>
                <input
                  type="date"
                  name="check-out"
                  id="check-out"
                  value={checkOut}
                  min={checkIn}
                  onChange={handleCheckOutChange}
                />
              </Box>
            </Box>

            <div className="divider--date"></div>

            <Box className="date__number-member">
              <img src={AssetImages.ICONS.CHECKOUT.USER} alt="" />
              <Typography
                sx={{ color: "white", fontSize: 18, fontWeight: 400 }}
              >
                1 adult
              </Typography>
            </Box>
          </Box>

          <Divider
            sx={{
              backgroundColor: themeColors.divider,
              m: "20px 0",
            }}
          />

          <Box className="detail__prices">
            <Box className="price__item">
              <Typography className="price--title">Room</Typography>
              <Typography
                sx={{
                  color: themeColors.white,
                  fontSize: 18,
                  fontWeight: 400,
                }}
              >
                $ {formatTotal(numberOfNights * room?.price)}
              </Typography>
            </Box>

            <Divider sx={{ backgroundColor: themeColors.divider }} />

            <Box className="price__item">
              <Typography className="price--title">Taxes</Typography>
              <Typography
                sx={{
                  color: themeColors.white,
                  fontSize: 18,
                  fontWeight: 400,
                }}
              >
                $ {formatTotal(numberOfNights * room?.price * 0.05)}
              </Typography>
            </Box>

            <Divider sx={{ backgroundColor: themeColors.divider }} />

            <Box className="price__item--icon">
              <Typography className="price--title">Options</Typography>
              <img src={AssetImages.ICONS.CHECKOUT.DROPDOWN} alt="" />
            </Box>

            <Divider sx={{ backgroundColor: themeColors.divider }} />

            <Box className="price__item--icon">
              <Typography className="price--title">Voucher</Typography>
              <img src={AssetImages.ICONS.CHECKOUT.DROPDOWN} alt="" />
            </Box>

            <Divider sx={{ backgroundColor: themeColors.divider }} />
          </Box>

          <Box className="detail__total-price">
            <Typography className="total-price__title">Total</Typography>
            <Typography
              sx={{
                color: themeColors.white,
                fontSize: 26,
                fontWeight: "bold",
              }}
            >
              ${" "}
              {formatTotal(
                numberOfNights * room?.price +
                  numberOfNights * room?.price * 0.05
              )}
            </Typography>
          </Box>

          <Box className="detail__btn">
            <Button className="btn--checkout" onClick={handleBooking}>
              Check Out
            </Button>
          </Box>
        </Box>
      </Box>
      <FooterComponent />
    </BoxContainer>
  );
};

export default CheckoutPage;
