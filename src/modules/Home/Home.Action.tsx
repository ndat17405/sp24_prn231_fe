import { HomeActions } from "../../redux/actions";

export interface ChangeSidebarAction {
  type: HomeActions.CHANGE_SIDEBAR;
  payload: any;
}

export interface SaveRoomByIdAction {
  type: HomeActions.SAVE_ROOM_BY_ID;
  payload: any;
}

export interface SaveListImagesAction {
  type: HomeActions.SAVE_LIST_IMAGES;
  payload: any;
}

export interface SaveListRoomBySearchAction {
  type: HomeActions.SAVE_LIST_ROOM_WITH_SEARCH;
  payload: any;
}

export type HomeAction =
  | ChangeSidebarAction
  | SaveRoomByIdAction
  | SaveListImagesAction
  | SaveListRoomBySearchAction;

export const changeSidebar = (index: any): ChangeSidebarAction => {
  return {
    type: HomeActions.CHANGE_SIDEBAR,
    payload: index,
  };
};

export const saveRoomById = (room: any): SaveRoomByIdAction => {
  return {
    type: HomeActions.SAVE_ROOM_BY_ID,
    payload: room,
  };
};

export const saveListImages = (listImages: any): SaveListImagesAction => {
  return {
    type: HomeActions.SAVE_LIST_IMAGES,
    payload: listImages,
  };
};

export const saveListRoomBySearch = (
  listRoom: any
): SaveListRoomBySearchAction => {
  return {
    type: HomeActions.SAVE_LIST_ROOM_WITH_SEARCH,
    payload: listRoom,
  };
};
