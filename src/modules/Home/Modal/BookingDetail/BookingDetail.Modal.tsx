import { useState } from "react";
import "./BookingDetail.Style.scss";
import {
  Box,
  ButtonProps,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  Typography,
} from "@mui/material";
import React from "react";
import { CloseOutlined } from "@mui/icons-material";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";

const BookingDetailModal = ({
  children,
  data,
}: {
  children: any;
  data: any;
}) => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}
      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        fullWidth
        maxWidth="md"
      >
        <DialogTitle
          display="flex"
          alignItems="center"
          fontSize="20px"
          justifyContent="space-between"
        >
          Booking Detail
          <IconButton onClick={handleClose}>
            <CloseOutlined />
          </IconButton>
        </DialogTitle>

        <DialogContent dividers>
          <Box className="modal--room-detail">
            <img
              src={`data:image/*;base64,${data?.imgRoom}`}
              alt=""
              style={{
                width: "100%",
                borderRadius: "8px 8px 0 0",
                boxShadow: themeColors.boxShadow,
                marginBottom: "20px",
              }}
            />

            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Box
                sx={{
                  width: "70%",
                  display: "flex",
                  flexDirection: "column",
                  gap: "30px",
                }}
              >
                <Box
                  sx={{
                    height: "3rem",
                    borderBottom: `1px solid ${themeColors.divider}`,
                  }}
                >
                  <Typography
                    sx={{
                      color: themeColors.black,
                      fontSize: "22px",
                      pl: "10px",
                    }}
                  >
                    {data?.roomName}
                  </Typography>
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    p: "0 40px 20px 10px",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "15px",
                    }}
                  >
                    <Typography
                      sx={{ color: themeColors.btnSecondary, fontSize: "16px" }}
                    >
                      Check-in
                    </Typography>
                    <Typography
                      sx={{ color: themeColors.black, fontSize: "20px" }}
                    >
                      {data?.checkInDate}
                    </Typography>
                  </Box>

                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "15px",
                    }}
                  >
                    <Typography
                      sx={{ color: themeColors.btnSecondary, fontSize: "16px" }}
                    >
                      Check-out
                    </Typography>
                    <Typography
                      sx={{ color: themeColors.black, fontSize: "20px" }}
                    >
                      {data?.checkOutDate}
                    </Typography>
                  </Box>
                </Box>
              </Box>

              <Box
                sx={{
                  width: "30%",
                  borderLeft: `1px solid ${themeColors.divider}`,
                }}
              >
                <Box
                  sx={{
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                    p: "0 0 20px 10px",
                    justifyContent: "space-between",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      gap: "10px",
                      alignItems: "center",
                    }}
                  >
                    <Typography
                      sx={{
                        color: themeColors.black,
                        fontSize: "18px",
                        fontWeight: 700,
                      }}
                    >
                      Price:
                    </Typography>
                    <Typography sx={{ fontSize: "20px" }}>
                      ${data?.price}
                    </Typography>
                  </Box>

                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "10px",
                      alignItems: "center",
                    }}
                  >
                    <Typography
                      sx={{
                        color: themeColors.black,
                        fontSize: "18px",
                        fontWeight: 700,
                      }}
                    >
                      Status:
                    </Typography>
                    <Typography
                      sx={{
                        color: data?.status
                          ? themeColors.statusPositive
                          : themeColors.statusNegative,
                        fontSize: "20px",
                        fontWeight: 700,
                      }}
                    >
                      {data?.status ? "Completed" : "Wait for confirm"}
                    </Typography>
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>

          <Box className="modal--guest--infor">
            <Typography
              sx={{
                width: "fit-content",
                background: themeColors.bookingReverse,
                backgroundClip: "text",
                WebkitBackgroundClip: "text",
                WebkitTextFillColor: "transparent",
                fontSize: "26px",
                fontWeight: 700,
                p: "10px",
              }}
            >
              Guest Information
            </Typography>

            <Box
              sx={{
                width: "100%",
                display: "flex",
                p: "0 10px 0",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: "10px",
                }}
              >
                <Typography
                  sx={{ color: themeColors.btnSecondary, fontSize: "16px" }}
                >
                  Lead guest
                </Typography>
                <Typography sx={{ color: themeColors.black, fontSize: "20px" }}>
                  {data?.nameCustomer}
                </Typography>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: "10px",
                }}
              >
                <Typography
                  sx={{ color: themeColors.btnSecondary, fontSize: "16px" }}
                >
                  Email
                </Typography>
                <Typography sx={{ color: themeColors.black, fontSize: "20px" }}>
                  {data?.emailCustomer}
                </Typography>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: "10px",
                }}
              >
                <Typography
                  sx={{ color: themeColors.btnSecondary, fontSize: "16px" }}
                >
                  Phone
                </Typography>
                <Typography sx={{ color: themeColors.black, fontSize: "20px" }}>
                  {data?.phone}
                </Typography>
              </Box>
            </Box>

            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "10px",
                p: "0 0 10px 10px",
              }}
            >
              <Typography
                sx={{ color: themeColors.btnSecondary, fontSize: "16px" }}
              >
                Maximum number of people in the room
              </Typography>
              <Typography sx={{ color: themeColors.black, fontSize: "20px" }}>
                {data?.numberMember}
              </Typography>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default BookingDetailModal;
