/* eslint-disable react-hooks/exhaustive-deps */
import { CloseOutlined } from "@mui/icons-material";
import {
  Box,
  Button,
  ButtonProps,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  IconButton,
  ListItem,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import "./RoomDetail.Style.scss";
import { useNavigate } from "react-router-dom";
import { checkLogin } from "../../../../utils/helper";
import { routes } from "../../../../routes";
import { getRoomById } from "../../Home.Api";
import { saveRoomById } from "../../Home.Action";
import { useDispatch } from "react-redux";

const RoomDetailDialog = ({ children, data }: { children: any; data: any }) => {
  const services = [
    {
      id: 1,
      title: "Beds and Bedding",
      desc: data?.bedsAndBeding.split(";"),
    },
    {
      id: 2,
      title: "Internet and Phones",
      desc: data?.internetAndPhones.split(";"),
    },
    {
      id: 3,
      title: "Bath and Bathroom Features",
      desc: data?.bathAndBathroom.split(";"),
    },
    {
      id: 4,
      title: "Entertainment",
      desc: data?.entertainment.split(";"),
    },
  ];

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [isOpen, setIsOpen] = useState(false);

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  const handleBooking = async () => {
    const roomId = data?.id;
    const room = await getRoomById(roomId, dispatch);
    if (room && checkLogin()) {
      saveRoomById(room);
      setIsOpen(false);
      navigate(routes.home.checkoutPage);
      document.documentElement.scrollTop = 0;
    } else {
      navigate(routes.home.loginPage);
    }
  };

  useEffect(() => {}, [handleBooking]);

  return (
    <>
      {React.cloneElement(children as React.ReactElement<ButtonProps>, {
        onClick: handleOpen,
      })}
      <Dialog
        open={isOpen}
        onClose={handleClose}
        scroll="paper"
        fullWidth
        maxWidth="md"
      >
        <DialogTitle
          display="flex"
          alignItems="center"
          fontSize="20px"
          justifyContent="space-between"
        >
          Room Detail
          <IconButton onClick={handleClose}>
            <CloseOutlined />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <img
            src={`data:image/*;base64,${data?.image}`}
            alt=""
            style={{
              width: "100%",
              borderRadius: "8px",
              boxShadow: themeColors.boxShadow,
            }}
          />
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            p="15px 20px"
          >
            <Typography
              fontSize="20px"
              color={themeColors.black}
              fontWeight="700"
            >
              {data?.name}
            </Typography>
            <Typography fontSize="20px" color={themeColors.black}>
              Size: {data?.size}
            </Typography>
          </Box>

          <Divider variant="middle" sx={{ bgcolor: themeColors.divider }} />

          <Box className="room-detail--service">
            {services.map((service) => (
              <Box className="service--box" key={service.id}>
                <Typography className="service--title">
                  {service.title}
                </Typography>
                <Stack direction="column" gap="10px">
                  {[...service.desc].map((desc, index) => (
                    <ListItem key={index} disablePadding>
                      <Typography className="service--desc">{desc}</Typography>
                    </ListItem>
                  ))}
                </Stack>
              </Box>
            ))}
          </Box>
        </DialogContent>
        <DialogActions sx={{ justifyContent: "center", gap: "30px" }}>
          <Button
            onClick={handleBooking}
            sx={{
              bgcolor: themeColors.btnPrimary,
              color: themeColors.white,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 50px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                bgcolor: themeColors.thirdary,
              },
            }}
          >
            Book Now
          </Button>
          <Button
            onClick={handleClose}
            sx={{
              bgcolor: themeColors.btnSecondary,
              color: themeColors.black,
              border: `1px solid ${themeColors.black}`,
              fontWeight: "100",
              p: "5px 30px",
              fontSize: "15px",
              borderRadius: "8px",
              "&:hover": {
                bgcolor: themeColors.divider,
              },
            }}
          >
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default RoomDetailDialog;
