import axios from "axios";
import { ApiUrl } from "../../services/ApiUrl";
import {
  saveListImages,
  saveListRoomBySearch,
  saveRoomById,
} from "./Home.Action";
import axiosUser from "../../utils/axiosUser";

export const getAllRooms = async () => {
  try {
    const response = await axiosUser.get(`${ApiUrl.GET_ALL_ROOMS}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const getRoomById = async (roomId: any, dispatch: any) => {
  try {
    const response = await axiosUser.get(
      `${ApiUrl.GET_ROOM_BY_ROOM_ID}?id=${roomId}`
    );
    if (response.data.data) {
      dispatch(saveRoomById(response.data.data));
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
  }
};

export const getRoomBySearchOr = async (searchKey: any, dispatch: any) => {
  try {
    let formData = new FormData();

    formData.append("searchKey", searchKey);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.post(
      "https://eposhdeploy-production.up.railway.app/api/v1/rooms/searchWithCategoryOr",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      dispatch(saveListRoomBySearch(response.data.data));
      return response.data.data;
    } else {
      dispatch(saveListRoomBySearch([]));
      return false;
    }
  } catch (error) {
    dispatch(saveListRoomBySearch([]));
    return false;
  }
};

export const getRoomBySearchAnd = async (
  size: any,
  type: any,
  dispatch: any
) => {
  try {
    let formData = new FormData();

    formData.append("sizeOfRoom", size);
    formData.append("typeOfBed", type);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.post(
      "https://eposhdeploy-production.up.railway.app/api/v1/rooms/searchWithCategoryAnd",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      dispatch(saveListRoomBySearch(response.data.data));
      return response.data.data;
    } else {
      dispatch(saveListRoomBySearch([]));
      return false;
    }
  } catch (error) {
    dispatch(saveListRoomBySearch([]));
    return false;
  }
};

export const getAllGallery = async (dispatch: any) => {
  try {
    const response = await axiosUser.get(`${ApiUrl.GET_ALL_GALLERIES}`);
    if (response.data.data) {
      dispatch(saveListImages(response.data.data));
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
  }
};

export const getAllCategory = async () => {
  try {
    const response = await axiosUser.get(`${ApiUrl.GET_ALL_CATEGORY}`);
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const getProfileByAccountId = async (id: any) => {
  try {
    const response = await axiosUser.get(
      `${ApiUrl.GET_PROFILE_BY_ACCOUNT_ID}?id=${id}`
    );
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    console.log(error);
  }
};

export const updateProfile = async (id: any, data: any) => {
  try {
    const response = await axiosUser.put(
      `${ApiUrl.UPDATE_PROFILE}/${id}`,
      data
    );
    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const changePassword = async (data: any) => {
  try {
    const response = await axiosUser.post(`${ApiUrl.CHANGE_PASSWORD}`, data);
    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const createBooking = async (
  accountID: any,
  roomID: any,
  checkIn: any,
  checkOut: any,
  totalPrice: any
) => {
  try {
    let formData = new FormData();

    formData.append("accountID", accountID);
    formData.append("roomID", roomID);
    formData.append("CheckInDate", checkIn);
    formData.append("CheckOutDate", checkOut);
    formData.append("Totalprice", totalPrice);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };

    const response = await axios.post(
      "https://eposhdeploy-production.up.railway.app/api/v1/booking/createBooking",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const getBookingByAccountId = async (id: any) => {
  try {
    const response = await axiosUser.get(
      `${ApiUrl.GET_BOOKING_BY_ACCOUNT_ID}?accountID=${id}`
    );
    if (response.data.statusCode === 200) {
      return response.data.data;
    } else {
      return [];
    }
  } catch (error) {
    return false;
  }
};
