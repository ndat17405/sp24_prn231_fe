/* eslint-disable react-hooks/exhaustive-deps */
import "./Home.Style.scss";
import FooterComponent from "../../layouts/Footer/FooterComponent";
import SearchContainer from "./components/Search.Container";
import BoxContainer from "../../components/Box/Box.Container";
import NotLoggedContainer from "../../layouts/Header/User/NotLogged/NotLogged.Container";
import { Avatar, Box, Typography } from "@mui/material";
import { AssetImages } from "../../utils/images";
import { useEffect, useState } from "react";
import { customerReviews } from "../../utils/fake/data";
import { checkLogin, filterRoomIsEmptyList } from "../../utils/helper";
import { getAllRooms } from "./Home.Api";
import IsLoggedContainer from "../../layouts/Header/User/IsLogged/IsLogged.Container";
import { Outlet } from "react-router-dom";

const HomePage: React.FC<{}> = () => {
  const [data, setData] = useState([]);
  const listRoomsIsEmpty = filterRoomIsEmptyList(data);
  const sortedRooms = [...listRoomsIsEmpty].sort((a, b) => a.price - b.price);
  const fourCheapestRooms = sortedRooms.slice(0, 4);

  const [index, setIndex] = useState(0);
  const { name, date, subject, content } = customerReviews[index];

  const init = async () => {
    const res = await getAllRooms();
    if (res) {
      setData(res);
    }
  };

  const nextCustomerReview = () => {
    setIndex((prev) => prev + 1);
    if (index >= customerReviews.length - 1) {
      setIndex(0);
    }
  };

  const prevCustomerReview = () => {
    setIndex((prev) => prev - 1);
    if (index <= 0) {
      setIndex(customerReviews.length - 1);
    }
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [fourCheapestRooms, listRoomsIsEmpty]);

  return (
    <BoxContainer property="home--container">
      {checkLogin() ? <IsLoggedContainer /> : <NotLoggedContainer />}
      <BoxContainer property="content--container">
        <SearchContainer />

        <Box className="banner--container">
          <Box className="banner__img">
            <img src={AssetImages.BANNER} alt="" />
          </Box>
          <Box className="banner__text">
            <Typography className="title">eposh hotel</Typography>
            <Typography className="slogan color-change-3x">
              Towering Luxury - Unforgettable Experiences
            </Typography>
          </Box>
        </Box>

        <Box className="rooms--container">
          <Typography className="title">
            A classy experience, an ideal resting place
          </Typography>
          <Box className="rooms__item">
            {fourCheapestRooms?.map((room: any, index: any) => {
              if (index > 4) {
                return <></>;
              } else {
                return (
                  <Box className="item__box" key={index}>
                    <img src={`data:image/*;base64,${room?.images}`} alt="" />
                    <Box className="item__box--infor">
                      <Typography className="name">
                        {room?.roomsName}
                      </Typography>
                      <Typography className="price">
                        ${room?.price}.00
                      </Typography>
                    </Box>
                  </Box>
                );
              }
            })}
          </Box>
        </Box>

        <Box className="customer-reviews--container">
          <Typography className="title">Customer's Reviews</Typography>
          <Box className="customer-reviews__wrapper">
            <Box
              className="customer-reviews__left"
              onClick={prevCustomerReview}
            >
              <img src={AssetImages.ICONS.LEFT} alt="" />
            </Box>

            <Box className="customer-reviews__center" id="splide">
              <Box className="customer__avatar">
                <Avatar
                  src={AssetImages.ICONS.HOME_PAGE.USER}
                  sx={{
                    width: "60px",
                    height: "auto",
                    border: "1px solid black",
                    p: "10px",
                  }}
                />
              </Box>
              <Box className="customer__information">
                <Box className="info__customer">
                  <Typography className="name">{name}</Typography>
                  <Typography className="date">{date}</Typography>
                </Box>
                <Box className="info__reviews">
                  <Typography className="subject">{subject}</Typography>
                  <Typography className="content">{content}</Typography>
                </Box>
              </Box>
            </Box>

            <Box
              className="customer-reviews__right"
              onClick={nextCustomerReview}
            >
              <img src={AssetImages.ICONS.RIGHT} alt="" />
            </Box>
          </Box>
        </Box>
      </BoxContainer>
      <FooterComponent />
      <Outlet />
    </BoxContainer>
  );
};

export default HomePage;
