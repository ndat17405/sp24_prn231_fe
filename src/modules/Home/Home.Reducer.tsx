import { HomeActions } from "../../redux/actions";

export interface HomeState {
  indexSidebar: any | 0;
  room: any | {};
  listImages: any | [];
  listRoomBySearch: any | [];
}

const initialState = {
  indexSidebar: 0,
  room: {},
  listImages: [],
  listRoomBySearch: [],
};

function homeReducer(state = initialState, action: any) {
  switch (action.type) {
    case HomeActions.CHANGE_SIDEBAR:
      return {
        ...state,
        indexSidebar: action.payload,
      };
    case HomeActions.SAVE_ROOM_BY_ID:
      return {
        ...state,
        room: action.payload,
      };
    case HomeActions.SAVE_LIST_IMAGES:
      return {
        ...state,
        listImages: action.payload,
      };
    case HomeActions.SAVE_LIST_ROOM_WITH_SEARCH:
      return {
        ...state,
        listRoomBySearch: action.payload,
      };
    default:
      return state;
  }
}

export default homeReducer;
