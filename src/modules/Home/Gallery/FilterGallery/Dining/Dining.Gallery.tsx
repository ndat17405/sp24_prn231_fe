import { Box, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { themeColors } from "../../../../../themes/schemes/PureLightTheme";
import { filterImageIsDining } from "../../../../../utils/helper";
import { useEffect } from "react";
import { LoadingButton } from "@mui/lab";

const DiningGallery = () => {
  const listImages = useSelector((state: any) => state.home.listImages);
  const listImagesIsDining = filterImageIsDining(listImages);

  useEffect(() => {}, [listImages, listImagesIsDining]);

  return (
    <Box
      display="flex"
      justifyContent="space-between"
      flexWrap="wrap"
      m="0 30px 30px 30px"
      gap="30px"
    >
      {listImagesIsDining.map((item: any) => (
        <Box key={item.id} width="calc(100% / 3.2)">
          <img
            src={`data:image/*;base64,${item?.images}`}
            alt=""
            style={{
              width: "100%",
              minHeight: "300px",
              maxHeight: "300px",
              borderRadius: "8px",
              boxShadow: themeColors.formBoxShadow,
            }}
          />
        </Box>
      ))}
      {listImagesIsDining?.length === 0 && (
        <Box
          width="100%"
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <LoadingButton
            loading
            variant="outlined"
            sx={{ border: "0 !important" }}
          />
          <Typography
            className="color-change-3x"
            sx={{
              fontSize: "20px",
              fontWeight: 700,
            }}
          >
            Loading...
          </Typography>
        </Box>
      )}
    </Box>
  );
};

export default DiningGallery;
