/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Typography } from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import NotLoggedContainer from "../../../layouts/Header/User/NotLogged/NotLogged.Container";
import FooterComponent from "../../../layouts/Footer/FooterComponent";
import "./Gallery.Style.scss";
import { useEffect, useState } from "react";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import HotelViewGallery from "./FilterGallery/HotelView/HotelView.Gallery";
import GuestRoomGallery from "./FilterGallery/GuestRoom/GuestRoom.Gallery";
import SpaGallery from "./FilterGallery/Spa/Spa.Gallery";
import DiningGallery from "./FilterGallery/Dining/Dining.Gallery";
import WeddingGallery from "./FilterGallery/Wedding/Wedding.Gallery";
import { useDispatch } from "react-redux";
import { saveListImages } from "../Home.Action";
import { getAllGallery } from "../Home.Api";
import IsLoggedContainer from "../../../layouts/Header/User/IsLogged/IsLogged.Container";
import { checkLogin } from "../../../utils/helper";

const filterItems = ["Hotel View", "Guest Room", "Spa", "Dining", "Weddings"];

const GalleryPage = () => {
  const dispatch = useDispatch();
  const [selectedFilterItems, setSelectedFilterItems] = useState(0);

  const getListGalleries = async () => {
    const listImages = await getAllGallery(dispatch);
    if (listImages) {
      saveListImages(listImages);
    }
  };

  const handleSelectFilterItem = (index: any) => {
    setSelectedFilterItems(index);
  };

  useEffect(() => {
    getListGalleries();
  }, []);

  useEffect(() => {}, [selectedFilterItems]);

  return (
    <BoxContainer property="gallery--container">
      {checkLogin() ? <IsLoggedContainer /> : <NotLoggedContainer />}
      <BoxContainer property="content--container">
        <Box className="content__title--gallery">Galleries</Box>
        <Box className="content__filter--gallery">
          {filterItems.map((item, index) => (
            <Typography
              key={item}
              className="filter__item"
              onClick={() => handleSelectFilterItem(index)}
              sx={{
                color:
                  index === selectedFilterItems
                    ? themeColors.textNav
                    : themeColors.black,
                fontSize: "1.2rem",
                cursor: "pointer",
                "&:hover": {
                  color: "#1e8bff",
                },
              }}
            >
              {item}
            </Typography>
          ))}
        </Box>
        {selectedFilterItems === 0 ? <HotelViewGallery /> : null}
        {selectedFilterItems === 1 ? <GuestRoomGallery /> : null}
        {selectedFilterItems === 2 ? <SpaGallery /> : null}
        {selectedFilterItems === 3 ? <DiningGallery /> : null}
        {selectedFilterItems === 4 ? <WeddingGallery /> : null}
      </BoxContainer>
      <FooterComponent />
    </BoxContainer>
  );
};

export default GalleryPage;
