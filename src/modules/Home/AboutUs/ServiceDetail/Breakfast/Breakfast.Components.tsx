import { Box, Typography } from "@mui/material";
import { serviceDataBreakfast } from "../../../../../utils/fake/data";

const BreakfastComponents = () => {
  return (
    <>
      {serviceDataBreakfast.map((item) => (
        <Box className="posts__item" key={item.id}>
          <img src={item.img} alt="" />
          <Typography className="item__subject">{item.title}</Typography>
          <Typography className="item__content">{item.description}</Typography>
        </Box>
      ))}
    </>
  );
};

export default BreakfastComponents;
