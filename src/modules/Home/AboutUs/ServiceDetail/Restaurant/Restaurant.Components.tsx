import React from "react";
import { serviceDataRestaurant } from "../../../../../utils/fake/data";
import { Box, Typography } from "@mui/material";

const RestaurantComponents = () => {
  return (
    <>
      {serviceDataRestaurant.map((item) => (
        <Box className="posts__item" key={item.id}>
          <img src={item.img} alt="" />
          <Typography className="item__subject">{item.title}</Typography>
          <Typography className="item__content">{item.description}</Typography>
        </Box>
      ))}
    </>
  );
};

export default RestaurantComponents;
