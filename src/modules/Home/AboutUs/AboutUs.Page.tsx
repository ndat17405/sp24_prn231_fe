import { Box, Typography } from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import NotLoggedContainer from "../../../layouts/Header/User/NotLogged/NotLogged.Container";
import FooterComponent from "../../../layouts/Footer/FooterComponent";
import { AssetImages } from "../../../utils/images";
import "./AboutUs.Style.scss";
import { useRef, useState } from "react";
import IsLoggedContainer from "../../../layouts/Header/User/IsLogged/IsLogged.Container";
import { checkLogin } from "../../../utils/helper";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import {
  detailNav,
  leftService,
  ratingNav,
  reason,
  rightService,
  serviceNav,
} from "../../../utils/fake/data";
import AllProfilesComponents from "./Rating/AllProfiles/AllProfiles.Components";
import CouplesComponents from "./Rating/Couples/Couples.Components";
import FamilyComponents from "./Rating/Family/Family.Components";
import SoloTravelersComponents from "./Rating/SoloTravelers/SoloTravelers.Components";
import BusinessTravelersComponents from "./Rating/BusinessTravelers/BusinessTravelers.Components";
import RestaurantComponents from "./ServiceDetail/Restaurant/Restaurant.Components";
import BarsComponents from "./ServiceDetail/Bars/Bars.Components";
import BreakfastComponents from "./ServiceDetail/Breakfast/Breakfast.Components";
import PoolsComponents from "./ServiceDetail/Pools/Pools.Components";
import FitnessCenterComponents from "./ServiceDetail/FitnessCenter/FitnessCenter.Components";
import SpaComponents from "./ServiceDetail/Spa/Spa.Components";

const AboutUsPage = () => {
  const ratingRef = useRef<HTMLDivElement>(null);
  const serviceRef = useRef<HTMLDivElement>(null);

  const [indexNavDetail, setIndexNavDetail] = useState(0);
  const [indexNavService, setIndexNavService] = useState(0);
  const [indexNavRating, setIndexNavRating] = useState(0);

  const handleChangeNavDetail = (index: number) => {
    setIndexNavDetail(index++);
    if (index === 1) {
      document.documentElement.scrollTop = 300;
    } else if (index === 2) {
      handleScrollService();
    } else if (index === 3) {
      handleScrollRating();
    }
  };

  const handleChangeNavService = (index: number) => {
    setIndexNavService(index++);
  };

  const handleNextNavService = () => {
    setIndexNavService((prev) => prev + 1);
    if (indexNavService >= serviceNav.length - 1) {
      setIndexNavService(0);
    }
  };

  const handlePrevNavService = () => {
    setIndexNavService((prev) => prev - 1);
    if (indexNavService <= 0) {
      setIndexNavService(serviceNav.length - 1);
    }
  };

  const handleChangeNavRating = (index: number) => {
    setIndexNavRating(index++);
  };

  const handleScrollRating = () => {
    if (ratingRef.current) {
      ratingRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  const handleScrollService = () => {
    if (serviceRef.current) {
      serviceRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <BoxContainer property="about-us--container">
      {checkLogin() ? <IsLoggedContainer /> : <NotLoggedContainer />}
      <Box className="about-us--wrapper">
        <Box className="about-us__img">
          <img src={AssetImages.ABOUT_US_1} alt="" />
          <img src={AssetImages.ABOUT_US_2} alt="" />
          <img src={AssetImages.ABOUT_US_3} alt="" />
          <img src={AssetImages.ABOUT_US_4} alt="" />
        </Box>

        <Box className="about-us__hotel-info">
          <img className="logo" src={AssetImages.LOGO_HOTEL} alt="" />
          <Box className="information">
            <Typography className="hotel__name">
              EPOSH Hotel - Can Tho City
            </Typography>
            <Box className="hotel__reviews">
              <Box className="reviews__stars">
                <img src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR} alt="" />
                <img src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR} alt="" />
                <img src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR} alt="" />
                <img src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR} alt="" />
                <img src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.STAR} alt="" />
              </Box>
              <Typography
                className="reviews__member"
                onClick={handleScrollRating}
              >
                1690 reviews
              </Typography>
            </Box>
            <Box className="hotel__location">
              <img
                src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.LOCATION}
                alt=""
              />
              <a
                href="https://www.google.com/maps/place/Bonka+Coffee+T%26T+c%E1%BA%A7n+th%C6%A1/@10.0149354,105.7340544,19.25z/data=!4m6!3m5!1s0x31a089a24765f6bf:0xa5b18a8d368209bc!8m2!3d10.0149195!4d105.7346897!16s%2Fg%2F11tn3x072c?authuser=0&entry=ttu"
                className="location__address"
                target="_blank"
                rel="noreferrer"
              >
                2P7M+XV8, Nguyen Van Cu Noi Dai, An Binh, Ninh Kieu, Can Tho
              </a>
            </Box>
          </Box>
        </Box>

        <Box className="about-us__detail">
          <Box className="detail__nav">
            {detailNav.map((item, index) => (
              <Typography
                onClick={() => handleChangeNavDetail(index)}
                className="nav__item"
                key={item}
                sx={{
                  borderBottom:
                    indexNavDetail === index
                      ? `5px solid ${themeColors.textNav}`
                      : "inherit",
                }}
              >
                {item}
              </Typography>
            ))}
          </Box>

          <Box className="detail__why-choose">
            <Typography className="why-choose__description">
              The EPOSH hotel is a convenient accommodation option close to Truc
              Lam Zen Monastery and the Plum Garden Historical Site. Offering
              free parking and Wi-Fi, along with massage services, a dry sauna
              room, and a fitness center, this hotel is a great choice for
              travelers looking to explore the land of Can Tho.
            </Typography>
            <Box className="why-choose--container">
              <Typography className="why-choose__title">
                Why should you choose eposh?
              </Typography>
              <Box className="why-choose__reason">
                {reason.map((item: any) => (
                  <Box className="reason__item" key={item}>
                    <img
                      src={AssetImages.ICONS.ABOUT_US.HOTEL_INFO.CHECKED}
                      alt=""
                    />
                    <Typography className="item__text">{item}</Typography>
                  </Box>
                ))}
              </Box>
            </Box>
          </Box>
        </Box>

        <div className="divider"></div>

        <Box className="about-us__services" ref={serviceRef}>
          <Typography className="services__title">Hotel Services</Typography>
          <Box className="services__date-time">
            <img src={AssetImages.ICONS.ABOUT_US.HOTEL_SERVICE.CLOCK} alt="" />
            <Typography className="date-time__text">
              Open form 00:00 AM - Closed up to 00:00 AM
            </Typography>
          </Box>
          <Box className="services__wrapper">
            <Typography className="wrapper__title">On site:</Typography>
            <Box className="wrapper__items">
              <Box className="left">
                {leftService.map((item: any) => (
                  <Box className="item__services--left" key={item.id}>
                    <img src={item.img} alt="" />
                    <Typography className="item__name">{item.name}</Typography>
                  </Box>
                ))}
              </Box>

              <Box className="right">
                {rightService.map((item: any) => (
                  <Box className="item__services--right" key={item.id}>
                    <img src={item.img} alt="" />
                    <Typography className="item__name">{item.name}</Typography>
                  </Box>
                ))}
              </Box>
            </Box>
          </Box>

          <Box className="services__detail">
            <Box className="detail__nav">
              {serviceNav.map((item, index) => (
                <Typography
                  onClick={() => handleChangeNavService(index)}
                  className="nav__item"
                  key={item}
                  sx={{
                    borderBottom:
                      indexNavService === index
                        ? `5px solid ${themeColors.textNav}`
                        : "inherit",
                  }}
                >
                  {item}
                </Typography>
              ))}
            </Box>

            <Box className="detail__posts">
              <Box
                className="detail__posts--left"
                onClick={handlePrevNavService}
              >
                <img src={AssetImages.ICONS.LEFT} alt="" />
              </Box>

              <Box className="detail__posts--center">
                {indexNavService === 0 ? <RestaurantComponents /> : null}
                {indexNavService === 1 ? <BarsComponents /> : null}
                {indexNavService === 2 ? <BreakfastComponents /> : null}
                {indexNavService === 3 ? <PoolsComponents /> : null}
                {indexNavService === 4 ? <FitnessCenterComponents /> : null}
                {indexNavService === 5 ? <SpaComponents /> : null}
              </Box>

              <Box
                className="detail__posts--right"
                onClick={handleNextNavService}
              >
                <img src={AssetImages.ICONS.RIGHT} alt="" />
              </Box>
            </Box>
          </Box>
        </Box>

        <div className="divider--service" ref={ratingRef}></div>

        <Box className="about-us__rating">
          <Typography className="rating__title">Hotel-users rating</Typography>
          <Box className="rating__detail">
            <Box className="detail__nav">
              {ratingNav.map((item, index) => (
                <Typography
                  onClick={() => handleChangeNavRating(index)}
                  className="nav__item"
                  key={item}
                  sx={{
                    borderBottom:
                      indexNavRating === index
                        ? `5px solid ${themeColors.textNav}`
                        : "inherit",
                  }}
                >
                  {item}
                </Typography>
              ))}
            </Box>

            <Box className="rating-layout">
              {indexNavRating === 0 ? <AllProfilesComponents /> : null}
              {indexNavRating === 1 ? <CouplesComponents /> : null}
              {indexNavRating === 2 ? <FamilyComponents /> : null}
              {indexNavRating === 3 ? <SoloTravelersComponents /> : null}
              {indexNavRating === 4 ? <BusinessTravelersComponents /> : null}
            </Box>
          </Box>
        </Box>
      </Box>
      <FooterComponent />
    </BoxContainer>
  );
};

export default AboutUsPage;
