import { Box, Typography } from "@mui/material";
import React from "react";
import { MyCustomCSS } from "../../../../../utils/customCSS";
import { ratingDataBusiness } from "../../../../../utils/fake/data";

const BusinessTravelersComponents = () => {
  const dataBusiness = ratingDataBusiness.map((item) => {
    return item;
  });

  return (
    <>
      <Box className="rating-layout--box">
        {dataBusiness[0].leftData?.map((item) => (
          <Box className="rating-layout__item" key={item.id}>
            <Box className="item__info">
              <Typography className="info__name">{item.name}</Typography>
              <Box className="item__info--reviews">
                <Typography className="info__reviews">
                  {item.reviews > 1
                    ? `${item.reviews} reviews`
                    : `${item.reviews} review`}
                </Typography>
                <Typography className="info__percent">
                  {item.percent / 10}/10
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                backgroundColor: "#e8e8e8",
                borderRadius: "16px",
              }}
            >
              <Box
                className="item__percent"
                sx={{ "--percent": `${item.percent}%` } as MyCustomCSS}
              ></Box>
            </Box>
          </Box>
        ))}
      </Box>

      <Box className="rating-layout--box">
        {dataBusiness[1].rightData?.map((item) => (
          <Box className="rating-layout__item" key={item.id}>
            <Box className="item__info">
              <Typography className="info__name">{item.name}</Typography>
              <Box className="item__info--reviews">
                <Typography className="info__reviews">
                  {item.reviews > 1
                    ? `${item.reviews} reviews`
                    : `${item.reviews} review`}
                </Typography>
                <Typography className="info__percent">
                  {item.percent / 10}/10
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                backgroundColor: "#e8e8e8",
                borderRadius: "16px",
              }}
            >
              <Box
                className="item__percent"
                sx={{ "--percent": `${item.percent}%` } as MyCustomCSS}
              ></Box>
            </Box>
          </Box>
        ))}
      </Box>
    </>
  );
};

export default BusinessTravelersComponents;
