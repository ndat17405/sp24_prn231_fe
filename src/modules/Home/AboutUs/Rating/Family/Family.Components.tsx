import { Box, Typography } from "@mui/material";
import { MyCustomCSS } from "../../../../../utils/customCSS";
import { ratingDataFamily } from "../../../../../utils/fake/data";

const FamilyComponents = () => {
  const dataFamily = ratingDataFamily.map((item) => {
    return item;
  });

  return (
    <>
      <Box className="rating-layout--box">
        {dataFamily[0].leftData?.map((item) => (
          <Box className="rating-layout__item" key={item.id}>
            <Box className="item__info">
              <Typography className="info__name">{item.name}</Typography>
              <Box className="item__info--reviews">
                <Typography className="info__reviews">
                  {item.reviews > 1
                    ? `${item.reviews} reviews`
                    : `${item.reviews} review`}
                </Typography>
                <Typography className="info__percent">
                  {item.percent / 10}/10
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                backgroundColor: "#e8e8e8",
                borderRadius: "16px",
              }}
            >
              <Box
                className="item__percent"
                sx={{ "--percent": `${item.percent}%` } as MyCustomCSS}
              ></Box>
            </Box>
          </Box>
        ))}
      </Box>

      <Box className="rating-layout--box">
        {dataFamily[1].rightData?.map((item) => (
          <Box className="rating-layout__item" key={item.id}>
            <Box className="item__info">
              <Typography className="info__name">{item.name}</Typography>
              <Box className="item__info--reviews">
                <Typography className="info__reviews">
                  {item.reviews > 1
                    ? `${item.reviews} reviews`
                    : `${item.reviews} review`}
                </Typography>
                <Typography className="info__percent">
                  {item.percent / 10}/10
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                backgroundColor: "#e8e8e8",
                borderRadius: "16px",
              }}
            >
              <Box
                className="item__percent"
                sx={{ "--percent": `${item.percent}%` } as MyCustomCSS}
              ></Box>
            </Box>
          </Box>
        ))}
      </Box>
    </>
  );
};

export default FamilyComponents;
