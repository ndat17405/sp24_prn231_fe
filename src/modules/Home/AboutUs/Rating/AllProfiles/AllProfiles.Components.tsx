import React from "react";
import { ratingDataAllProfile } from "../../../../../utils/fake/data";
import { Box, Typography } from "@mui/material";
import { MyCustomCSS } from "../../../../../utils/customCSS";

const AllProfilesComponents = () => {
  const dataAllProfile = ratingDataAllProfile.map((item) => {
    return item;
  });

  return (
    <>
      <Box className="rating-layout--box">
        {dataAllProfile[0].leftData?.map((item) => (
          <Box className="rating-layout__item" key={item.id}>
            <Box className="item__info">
              <Typography className="info__name">{item.name}</Typography>
              <Box className="item__info--reviews">
                <Typography className="info__reviews">
                  {item.reviews > 1
                    ? `${item.reviews} reviews`
                    : `${item.reviews} review`}
                </Typography>
                <Typography className="info__percent">
                  {item.percent / 10}/10
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                backgroundColor: "#e8e8e8",
                borderRadius: "16px",
              }}
            >
              <Box
                className="item__percent"
                sx={{ "--percent": `${item.percent}%` } as MyCustomCSS}
              ></Box>
            </Box>
          </Box>
        ))}
      </Box>

      <Box className="rating-layout--box">
        {dataAllProfile[1].rightData?.map((item) => (
          <Box className="rating-layout__item" key={item.id}>
            <Box className="item__info">
              <Typography className="info__name">{item.name}</Typography>
              <Box className="item__info--reviews">
                <Typography className="info__reviews">
                  {item.reviews > 1
                    ? `${item.reviews} reviews`
                    : `${item.reviews} review`}
                </Typography>
                <Typography className="info__percent">
                  {item.percent / 10}/10
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                backgroundColor: "#e8e8e8",
                borderRadius: "16px",
              }}
            >
              <Box
                className="item__percent"
                sx={{ "--percent": `${item.percent}%` } as MyCustomCSS}
              ></Box>
            </Box>
          </Box>
        ))}
      </Box>
    </>
  );
};

export default AllProfilesComponents;
