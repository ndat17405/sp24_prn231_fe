/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import BoxContainer from "../../../components/Box/Box.Container";
import SearchContainer from "../components/Search.Container";
import { Box, Button, Typography } from "@mui/material";
import NotLoggedContainer from "../../../layouts/Header/User/NotLogged/NotLogged.Container";
import FooterComponent from "../../../layouts/Footer/FooterComponent";
import "./Room.Style.scss";
import RoomDetailDialog from "../Modal/RoomDetail/RoomDetail.Dialog";
import { checkLogin, filterRoomIsEmptyList } from "../../../utils/helper";
import { getAllRooms } from "../Home.Api";
import IsLoggedContainer from "../../../layouts/Header/User/IsLogged/IsLogged.Container";
import { useSelector } from "react-redux";

const RoomPage = () => {
  const [data, setData] = useState([]);
  const [roomPage, setRoomPage] = useState(5);

  const listRoomBySearch = useSelector(
    (state: any) => state.home.listRoomBySearch
  );

  const init = async () => {
    const res = await getAllRooms();
    if (filterRoomIsEmptyList(listRoomBySearch).length === 0) {
      setData(filterRoomIsEmptyList(res));
    } else {
      setData(filterRoomIsEmptyList(listRoomBySearch));
    }
  };

  const handleSeeMore = () => {
    setRoomPage((prev) => prev + 8);
  };

  useEffect(() => {
    init();
  }, [data]);

  useEffect(() => {}, [listRoomBySearch]);

  return (
    <BoxContainer property="rooms--container">
      {checkLogin() ? <IsLoggedContainer /> : <NotLoggedContainer />}
      <BoxContainer property="content--container">
        <SearchContainer />
        <Box className="content__title--room">All Rooms</Box>
        <Box className="content__list-rooms--container">
          {data?.map((room: any, index: any) => {
            if (index > roomPage) {
              return null;
            } else if (room?.status) {
              return (
                <Box className="list-rooms__item" key={room?.roomsID}>
                  <img
                    src={`data:image/*;base64,${room?.images}`}
                    alt="room-img"
                  />
                  <Box className="room__info">
                    <Typography className="room__info--name">
                      {room?.roomsName}
                    </Typography>
                    <RoomDetailDialog
                      data={{
                        id: room?.roomsID,
                        name: room?.roomsName,
                        image: room?.images,
                        bedsAndBeding: room?.bedsAndBeding,
                        internetAndPhones: room?.internetAndPhones,
                        bathAndBathroom: room?.bathAndBathroom,
                        entertainment: room?.entertainment,
                        size: room?.category?.sizeOfRooms,
                      }}
                    >
                      <Button className="room__info--detail">
                        View Detail
                      </Button>
                    </RoomDetailDialog>
                  </Box>
                </Box>
              );
            } else {
              return null;
            }
          })}
        </Box>
        <Box className="content__see-more">
          {roomPage < data?.length ? (
            <Box className="see-more__btn" onClick={handleSeeMore}>
              <Typography
                sx={{
                  color: "black",
                  fontSize: "1.2rem",
                  width: "100%",
                  textAlign: "center",
                }}
              >
                See more
              </Typography>
            </Box>
          ) : (
            <Typography
              sx={{
                m: "auto",
                fontSize: "1.2rem",
                color: "black",
                fontWeight: "500",
              }}
            >
              You have just viewed {data?.length}{" "}
              {data?.length > 1 ? "rooms" : "room"}
            </Typography>
          )}
        </Box>
      </BoxContainer>
      <FooterComponent />
    </BoxContainer>
  );
};

export default RoomPage;
