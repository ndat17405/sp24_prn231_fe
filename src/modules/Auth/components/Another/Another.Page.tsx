import { Box, Typography } from "@mui/material";
import { AssetImages } from "../../../../utils/images";
import "./Another.Style.scss";

export default function Another() {
  return (
    <Box className="another">
      <Typography>Or Login with</Typography>
      <Box className="icon">
        <img src={AssetImages.ICONS.FACEBOOK} alt="facebook icon" />
        <img src={AssetImages.ICONS.GOOGLE} alt="facebook icon" />
      </Box>
    </Box>
  );
}
