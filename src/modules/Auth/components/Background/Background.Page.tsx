import { Box } from "@mui/material";
import { AssetImages } from "../../../../utils/images";
import "./Background.Style.scss";

export default function Background() {
  return (
    <Box className="background">
      <img src={AssetImages.BACKGROUND} alt="background" />
    </Box>
  );
}
