/* eslint-disable react-hooks/exhaustive-deps */
import { Alert, AlertTitle, Box, Button, Typography } from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import "./StepTwo.Style.scss";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import { useEffect, useState } from "react";
import Background from "../../components/Background/Background.Page";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../../routes";
import { sendEmail } from "../../Auth.Api";
import secureLocalStorage from "react-secure-storage";

const StepTwoPage = () => {
  const navigate = useNavigate();
  const [otpCode, setOtpCode] = useState("");
  const [show, setShow] = useState(false);
  const [countdown, setCountdown] = useState(60);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayResendSuccess, setDisplayResendSuccess] = useState(false);
  const [displayResendError, setDisplayResendError] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleOtpChange = (e: any) => {
    setCountdown(60);
    setOtpCode(e.target.value);
  };

  const nextStep = () => {
    navigate(routes.home.forgotPassStepThreePage);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleSendOtp();
    }
  };

  const handleSendOtp = async () => {
    if (otpCode === "") {
      setDisplayErrorWhenNull(true);
    } else {
      try {
        if (secureLocalStorage.getItem("otp-code") === otpCode) {
          setDisplaySuccess(true);
          secureLocalStorage.removeItem("otp-code");
          setTimeout(nextStep, 2000);
        } else {
          setDisplayError(true);
        }
      } catch (error) {
        setDisplayError(true);
      }
    }
  };

  console.log(show);

  const handleResendOTP = async () => {
    try {
      setCountdown(60);
      setShow((prev) => !prev);
      const response = await sendEmail(secureLocalStorage.getItem("email"));
      if (response) {
        setDisplayResendSuccess(true);
      } else {
        setDisplayResendError(true);
      }
    } catch (error) {
      setDisplayResendError(true);
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayResendSuccess) {
      setTimeout(() => {
        setDisplayResendSuccess(false);
      }, 3000);
    }
  }, [displayResendSuccess]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayResendError) {
      setTimeout(() => {
        setDisplayResendError(false);
      }, 5000);
    }
  }, [displayResendError]);

  useEffect(() => {
    let timer: NodeJS.Timeout;

    if (countdown > 0 && !show) {
      timer = setTimeout(() => {
        setCountdown((prev) => prev - 1);
      }, 1000);
    } else if (countdown === 0 && !show) {
      secureLocalStorage.removeItem("otp-code");
      setShow(true);
    }

    return () => clearTimeout(timer);
  }, [countdown, show]);

  useEffect(() => {}, [handleSendOtp]);

  return (
    <BoxContainer property="forgot-pass--container">
      <Box className="forgot-pass__form">
        <Alert
          onClose={() => setDisplayResendSuccess(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayResendSuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>
            Resend OTP code success
          </AlertTitle>
          Please check your mail!
        </Alert>

        <Alert
          onClose={() => setDisplayResendError(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayResendError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Resend failure!
        </Alert>

        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          You must enter your OTP code!
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            minWidth: "20%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Your OTP code is incorrect!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Your OTP code is correct!
        </Alert>

        <Background />

        <BoxContainer property="otp--container">
          <form action="" onKeyDown={handleKeyDown}>
            <h1 className="title">Verify your email</h1>
            <Typography className="user-guide">
              OTP Code has sent to your email. Please enter it to verify!
            </Typography>
            <Box className="control">
              <label className="label" htmlFor="email">
                OTP Code
              </label>
              <input
                type="text"
                name="email"
                id="email"
                className="input"
                placeholder="000000"
                maxLength={6}
                size={6}
                autoFocus
                onChange={handleOtpChange}
              />
            </Box>

            <Box sx={{ display: "flex" }}>
              <Button
                onClick={handleSendOtp}
                sx={{
                  margin: "auto",
                  width: "65%",
                  backgroundColor: themeColors.btnPrimary,
                  color: themeColors.white,
                  letterSpacing: "5px",
                  borderRadius: "8px",
                  "&:hover": {
                    backgroundColor: themeColors.thirdary,
                  },
                }}
              >
                CONTINUE
              </Button>
            </Box>
          </form>
          {!show ? (
            <Box
              sx={{
                width: "100%",
                p: "5px 5px 15px 5px",
                textAlign: "center",
              }}
            >
              If your email don't have a message, please wait for{" "}
              <Typography
                className="color-change-3x"
                sx={{ display: "inline" }}
              >
                {countdown}
              </Typography>{" "}
              seconds to resend OTP code.
            </Box>
          ) : (
            <Box
              sx={{
                width: "100%",
                p: "5px 5px 15px 5px",
                textAlign: "center",
              }}
            >
              <Typography
                onClick={handleResendOTP}
                sx={{
                  display: "inline",
                  color: themeColors.textNav,
                  "&:hover": {
                    cursor: "pointer",
                    color: themeColors.thirdary,
                  },
                }}
              >
                resend OTP code
              </Typography>
            </Box>
          )}
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default StepTwoPage;
