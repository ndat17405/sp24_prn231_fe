/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import BoxContainer from "../../../../components/Box/Box.Container";
import { Alert, AlertTitle, Box, Button, Typography } from "@mui/material";
import Background from "../../components/Background/Background.Page";
import { sendEmail } from "../../Auth.Api";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import { useNavigate } from "react-router-dom";
import "./StepOne.Style.scss";
import { routes } from "../../../../routes";
import secureLocalStorage from "react-secure-storage";

const StepOnePage = () => {
  const navigate = useNavigate();
  const regexEmail =
    /^(?=.{1,64}@)[A-Za-z_-]+[.A-Za-z0-9_-]*@[A-Za-z0-9]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/;
  const [email, setEmail] = useState("");

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayValidate, setDisplayValidate] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleEmailChange = (e: any) => {
    setEmail(e.target.value);
  };

  const nextStep = () => {
    navigate(routes.home.forgotPassStepTwoPage);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleSendEmail();
    }
  };

  const handleSendEmail = async () => {
    if (email === "") {
      setDisplayErrorWhenNull(true);
    } else {
      if (!regexEmail.test(email)) {
        setDisplayValidate(true);
      } else {
        try {
          const res = await sendEmail(email);

          if (res) {
            secureLocalStorage.setItem("email", email);
            setDisplaySuccess(true);
            setTimeout(nextStep, 2000);
          } else {
            setDisplayError(true);
          }
        } catch (error) {
          setDisplayError(true);
        }
      }
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayValidate) {
      setTimeout(() => {
        setDisplayValidate(false);
      }, 5000);
    }
  }, [displayValidate]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {}, [handleSendEmail]);

  return (
    <BoxContainer property="forgot-pass--container">
      <Box className="forgot-pass__form">
        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            minWidth: "20%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          You must enter your email!
        </Alert>

        <Alert
          onClose={() => setDisplayValidate(false)}
          sx={{
            minWidth: "20%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayValidate ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Invalid email</AlertTitle>
          Valid email: example@gmail.com
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            minWidth: "20%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Send email failure!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Send otp success. Please check in your mail!
        </Alert>

        <Background />

        <BoxContainer property="send_mail--container">
          <form action="" onKeyDown={handleKeyDown}>
            <h1 className="title">FORGOT PASSWORD</h1>
            <Typography className="user-guide">
              To reset your password, please enter the registered email address
              and we'll send you OTP in your email!
            </Typography>
            <Box className="control">
              <label className="label" htmlFor="email">
                Email
              </label>
              <input
                type="text"
                name="email"
                id="email"
                className="input"
                placeholder="example@gmail.com"
                autoFocus
                onChange={handleEmailChange}
              />
            </Box>

            <Box sx={{ display: "flex" }}>
              <Button
                onClick={handleSendEmail}
                sx={{
                  margin: "auto",
                  width: "65%",
                  backgroundColor: themeColors.btnPrimary,
                  color: themeColors.white,
                  letterSpacing: "5px",
                  borderRadius: "8px",
                  "&:hover": {
                    backgroundColor: themeColors.thirdary,
                  },
                }}
              >
                SEND
              </Button>
            </Box>
          </form>
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default StepOnePage;
