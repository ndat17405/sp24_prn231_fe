/* eslint-disable react-hooks/exhaustive-deps */
import { Alert, AlertTitle, Box, Button, Typography } from "@mui/material";
import BoxContainer from "../../../../components/Box/Box.Container";
import "./StepThree.Style.scss";
import { themeColors } from "../../../../themes/schemes/PureLightTheme";
import { useEffect, useState } from "react";
import Background from "../../components/Background/Background.Page";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../../routes";
import { updateNewPass } from "../../Auth.Api";
import { VisibilityOff, Visibility } from "@mui/icons-material";
import secureLocalStorage from "react-secure-storage";

const StepThreePage = () => {
  const navigate = useNavigate();
  const regexPassword =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,16}$/;
  const [newPass, setNewPass] = useState("");
  const [confPass, setConfPass] = useState("");

  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfPassword, setShowConfPassword] = useState(false);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayValidate, setDisplayValidate] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);
  const [displayErrorConfPass, setDisplayErrorConfPass] = useState(false);

  const handleShowNewPassword = () => {
    setShowNewPassword((showNewPassword) => !showNewPassword);
  };

  const handleShowConfPassword = () => {
    setShowConfPassword((showConfPassword) => !showConfPassword);
  };

  const backToLogin = () => {
    navigate(routes.home.loginPage);
  };

  const handleNewPassChange = (e: any) => {
    setNewPass(e.target.value);
  };

  const handleConfirmPassChange = (e: any) => {
    setConfPass(e.target.value);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleUpdatePassword();
    }
  };

  const handleUpdatePassword = async () => {
    if (newPass === "" || confPass === "") {
      setDisplayErrorWhenNull(true);
    } else {
      try {
        if (!regexPassword.test(newPass) || !regexPassword.test(confPass)) {
          setDisplayValidate(true);
        } else {
          if (newPass.trim() === confPass.trim()) {
            const data = {
              email: secureLocalStorage.getItem("email"),
              newPassWord: newPass,
            };

            const res = await updateNewPass(data);

            if (res) {
              setDisplaySuccess(true);
              setTimeout(backToLogin, 2000);
            } else {
              setDisplayError(true);
            }
          } else {
            setDisplayErrorConfPass(true);
          }
        }
      } catch (error) {
        setDisplayError(true);
      }
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayValidate) {
      setTimeout(() => {
        setDisplayValidate(false);
      }, 6000);
    }
  }, [displayValidate]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayErrorConfPass) {
      setTimeout(() => {
        setDisplayErrorConfPass(false);
      }, 5000);
    }
  }, [displayErrorConfPass]);

  useEffect(() => {}, [handleUpdatePassword]);

  return (
    <BoxContainer property="forgot-pass--container">
      <Box className="forgot-pass__form">
        <Alert
          onClose={() => setDisplayValidate(false)}
          sx={{
            minWidth: "27%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayValidate ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Invalid password</AlertTitle>
          Password must have at least{" "}
          <ul>
            <li>
              <strong>One lowercase letter</strong>
            </li>
            <li>
              <strong>One uppercase letter</strong>
            </li>
            <li>
              <strong>One number</strong>
            </li>
            <li>
              <strong>One special letter</strong>
            </li>
            <li>
              <strong>From 8 to 16 characters</strong>
            </li>
          </ul>
        </Alert>

        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Update new password fail!
        </Alert>

        <Alert
          onClose={() => setDisplayErrorConfPass(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "2%",
            display: displayErrorConfPass ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Confirm password must be equal new password. Please enter again!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Update new password successfully!
        </Alert>

        <Background />

        <BoxContainer property="new_pass--container">
          <form action="" onKeyDown={handleKeyDown}>
            <h1 className="title">Reset Password</h1>
            <Typography className="user-guide">
              Please enter your new password below.
            </Typography>
            <Box className="control control-pass">
              <label className="label" htmlFor="new-pass">
                New Password
              </label>
              <input
                type={showNewPassword ? "text" : "password"}
                name="new-pass"
                id="new-pass"
                className="input"
                placeholder="Please enter new password"
                autoFocus
                onChange={handleNewPassChange}
              />
              <div className="icon" onClick={handleShowNewPassword}>
                {showNewPassword ? <VisibilityOff /> : <Visibility />}
              </div>
            </Box>

            <Box className="control control-pass">
              <label className="label" htmlFor="confirm-pass">
                Confirm Password
              </label>
              <input
                type={showConfPassword ? "text" : "password"}
                name="confirm-pass"
                id="confirm-pass"
                className="input"
                placeholder="Please enter again password"
                onChange={handleConfirmPassChange}
              />
              <div className="icon" onClick={handleShowConfPassword}>
                {showConfPassword ? <VisibilityOff /> : <Visibility />}
              </div>
            </Box>

            <Box sx={{ display: "flex" }}>
              <Button
                onClick={handleUpdatePassword}
                sx={{
                  margin: "auto",
                  width: "65%",
                  backgroundColor: themeColors.btnPrimary,
                  color: themeColors.white,
                  letterSpacing: "5px",
                  borderRadius: "8px",
                  "&:hover": {
                    backgroundColor: themeColors.thirdary,
                  },
                }}
              >
                CONFIRM
              </Button>
            </Box>
          </form>
        </BoxContainer>
      </Box>
    </BoxContainer>
  );
};

export default StepThreePage;
