import { Alert, AlertTitle, Box, Button, Typography } from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import Background from "../components/Background/Background.Page";
import Another from "../components/Another/Another.Page";
import "./SignUp.Style.scss";
import { useEffect, useState } from "react";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../routes";
import { register } from "../Auth.Api";
import { VisibilityOff, Visibility } from "@mui/icons-material";

export default function SignUp() {
  const navigate = useNavigate();
  const regexEmail =
    /^(?=.{1,64}@)[A-Za-z_-]+[.A-Za-z0-9_-]*@[A-Za-z0-9]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/;
  const regexPassword =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,16}$/;
  const [fname, setFname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [showPassword, setShowPassword] = useState(false);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayExists, setDisplayExists] = useState(false);
  const [displayValidateEmail, setDisplayValidateEmail] = useState(false);
  const [displayValidatePassword, setDisplayValidatePassword] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleNameChange = (e: any) => {
    setFname(e.target.value);
  };

  const handleEmailChange = (e: any) => {
    setEmail(e.target.value);
  };

  const handlePassChange = (e: any) => {
    setPassword(e.target.value);
  };

  const navToLogin = () => {
    navigate(routes.home.loginPage);
  };

  const handleShowPassword = () => {
    setShowPassword((showPassword) => !showPassword);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleRegister();
    }
  };

  const handleRegister = async () => {
    if (fname === "" || email === "" || password === "") {
      setDisplayErrorWhenNull(true);
    } else {
      if (!regexEmail.test(email)) {
        setDisplayValidateEmail(true);
      } else if (!regexPassword.test(password)) {
        setDisplayValidatePassword(true);
      } else {
        const data = {
          email: email,
          password: password,
          fullName: fname,
        };

        try {
          const res = await register(data);
          if (res) {
            setDisplaySuccess(true);
            setTimeout(() => {
              navToLogin();
            }, 2000);
          } else {
            setDisplayExists(true);
          }
        } catch (error) {
          setDisplayError(true);
        }
      }
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayValidateEmail) {
      setTimeout(() => {
        setDisplayValidateEmail(false);
      }, 5000);
    }
  }, [displayValidateEmail]);

  useEffect(() => {
    if (displayValidatePassword) {
      setTimeout(() => {
        setDisplayValidatePassword(false);
      }, 6000);
    }
  }, [displayValidatePassword]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayExists) {
      setTimeout(() => {
        setDisplayExists(false);
      }, 5000);
    }
  }, [displayExists]);

  return (
    <BoxContainer property="register--container">
      <Box className="register__form">
        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplayExists(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayExists ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Email is already exists. Please login!
        </Alert>

        <Alert
          onClose={() => setDisplayValidateEmail(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayValidateEmail ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Invalid email</AlertTitle>
          Valid email: example@gmail.com
        </Alert>

        <Alert
          onClose={() => setDisplayValidatePassword(false)}
          sx={{
            minWidth: "27%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayValidatePassword ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Invalid password</AlertTitle>
          Password must have at least{" "}
          <ul>
            <li>
              <strong>One lowercase letter</strong>
            </li>
            <li>
              <strong>One uppercase letter</strong>
            </li>
            <li>
              <strong>One number</strong>
            </li>
            <li>
              <strong>One special letter</strong>
            </li>
            <li>
              <strong>From 8 to 16 characters</strong>
            </li>
          </ul>
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Register fail!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Register successfully!
        </Alert>

        <Background />

        <Box className="register__form--container">
          <form action="" onKeyDown={handleKeyDown}>
            <h1 className="title">SIGN UP</h1>

            <Box className="control">
              <label className="label" htmlFor="full-name">
                Full Name
              </label>
              <input
                type="text"
                name="full-name"
                id="full-name"
                className="input"
                placeholder="Please enter your full name"
                autoFocus
                onChange={handleNameChange}
              />
            </Box>

            <Box className="control">
              <label className="label" htmlFor="email">
                Email
              </label>
              <input
                type="text"
                name="email"
                id="email"
                className="input"
                placeholder="example@gmail.com"
                onChange={handleEmailChange}
              />
            </Box>

            <Box className="control control-pass">
              <label className="label" htmlFor="password">
                Password
              </label>
              <input
                type={showPassword ? "text" : "password"}
                name="password"
                id="password"
                className="input"
                placeholder="Please enter your password"
                onChange={handlePassChange}
              />
              <div className="icon" onClick={handleShowPassword}>
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </div>
            </Box>

            <Box className="btn--register" sx={{ display: "flex" }}>
              <Button
                onClick={handleRegister}
                sx={{
                  margin: "auto",
                  width: "65%",
                  backgroundColor: themeColors.btnPrimary,
                  color: themeColors.white,
                  letterSpacing: "5px",
                  borderRadius: "8px",
                  "&:hover": {
                    backgroundColor: themeColors.thirdary,
                  },
                }}
              >
                SIGN UP
              </Button>
            </Box>

            <Box className="login">
              <Typography className="login__text">
                Already have an account ?
                <span className="login__nav" onClick={navToLogin}>
                  Sign in here
                </span>
              </Typography>
            </Box>

            <Another />
          </form>
        </Box>
      </Box>
    </BoxContainer>
  );
}
