/* eslint-disable react-hooks/exhaustive-deps */
import { Alert, AlertTitle, Box, Button, Typography } from "@mui/material";
import BoxContainer from "../../../components/Box/Box.Container";
import "./SignIn.Style.scss";
import { useNavigate } from "react-router-dom";
import { routes } from "../../../routes";
import { themeColors } from "../../../themes/schemes/PureLightTheme";
import { useEffect, useState } from "react";
import Background from "../components/Background/Background.Page";
import Another from "../components/Another/Another.Page";
import { login } from "../Auth.Api";
import { checkPermission } from "../../../utils/helper";
import { Visibility, VisibilityOff } from "@mui/icons-material";

export default function SignIn() {
  const navigate = useNavigate();
  const regexEmail =
    /^(?=.{1,64}@)[A-Za-z_-]+[.A-Za-z0-9_-]*@[A-Za-z0-9]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/;
  const regexPassword =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,16}$/;
  const [email, setEmail] = useState("");
  const [pass, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const [displaySuccess, setDisplaySuccess] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const [displayLoginFail, setDisplayLoginFail] = useState(false);
  const [displayValidateEmail, setDisplayValidateEmail] = useState(false);
  const [displayValidatePassword, setDisplayValidatePassword] = useState(false);
  const [displayErrorWhenNull, setDisplayErrorWhenNull] = useState(false);

  const handleEmailChange = (e: any) => {
    setEmail(e.target.value);
  };

  const handlePassChange = (e: any) => {
    setPassword(e.target.value);
  };

  const navToRegister = () => {
    navigate(routes.home.registerPage);
  };

  const navToForgot = () => {
    navigate(routes.home.forgotPassStepOnePage);
  };

  const handleShowPassword = () => {
    setShowPassword((showPassword) => !showPassword);
  };

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
      e.preventDefault();
      handleLogin();
    }
  };

  const handleLogin = async () => {
    if (email === "" || pass === "") {
      setDisplayErrorWhenNull(true);
    } else {
      if (!regexEmail.test(email)) {
        setDisplayValidateEmail(true);
      } else if (!regexPassword.test(pass)) {
        setDisplayValidatePassword(true);
      } else {
        const data = {
          email: email,
          password: pass,
        };

        try {
          const res = await login(data);
          if (res) {
            setDisplaySuccess(true);
            setTimeout(() => {
              checkPermission(navigate);
            }, 2000);
          } else {
            setDisplayLoginFail(true);
          }
        } catch (error) {
          setDisplayError(true);
        }
      }
    }
  };

  useEffect(() => {
    if (displaySuccess) {
      setTimeout(() => {
        setDisplaySuccess(false);
      }, 1500);
    }
  }, [displaySuccess]);

  useEffect(() => {
    if (displayErrorWhenNull) {
      setTimeout(() => {
        setDisplayErrorWhenNull(false);
      }, 5000);
    }
  }, [displayErrorWhenNull]);

  useEffect(() => {
    if (displayValidateEmail) {
      setTimeout(() => {
        setDisplayValidateEmail(false);
      }, 6000);
    }
  }, [displayValidateEmail]);

  useEffect(() => {
    if (displayValidatePassword) {
      setTimeout(() => {
        setDisplayValidatePassword(false);
      }, 6000);
    }
  }, [displayValidatePassword]);

  useEffect(() => {
    if (displayError) {
      setTimeout(() => {
        setDisplayError(false);
      }, 5000);
    }
  }, [displayError]);

  useEffect(() => {
    if (displayLoginFail) {
      setTimeout(() => {
        setDisplayLoginFail(false);
      }, 5000);
    }
  }, [displayLoginFail]);

  useEffect(() => {}, [handleLogin]);

  return (
    <BoxContainer property="login--container">
      <Box className="login__form">
        <Alert
          onClose={() => setDisplayValidateEmail(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayValidateEmail ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Invalid email</AlertTitle>
          Valid email: example@gmail.com
        </Alert>

        <Alert
          onClose={() => setDisplayValidatePassword(false)}
          sx={{
            minWidth: "27%",
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayValidatePassword ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Invalid password</AlertTitle>
          Password must have at least{" "}
          <ul>
            <li>
              <strong>One lowercase letter</strong>
            </li>
            <li>
              <strong>One uppercase letter</strong>
            </li>
            <li>
              <strong>One number</strong>
            </li>
            <li>
              <strong>One special letter</strong>
            </li>
            <li>
              <strong>From 8 to 16 characters</strong>
            </li>
          </ul>
        </Alert>

        <Alert
          onClose={() => setDisplayErrorWhenNull(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayErrorWhenNull ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          All field are required!
        </Alert>

        <Alert
          onClose={() => setDisplayError(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayError ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Error</AlertTitle>
          Login fail!
        </Alert>

        <Alert
          onClose={() => setDisplayLoginFail(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displayLoginFail ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="error"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Login Fail</AlertTitle>
          Wrond email or password!
        </Alert>

        <Alert
          onClose={() => setDisplaySuccess(false)}
          sx={{
            position: "absolute",
            right: "1%",
            top: "7%",
            display: displaySuccess ? "flex" : "none",
            alignItems: "center",
            borderRadius: "8px",
            zIndex: "99999",
          }}
          variant="filled"
          severity="success"
        >
          <AlertTitle sx={{ fontWeight: "bold" }}>Success</AlertTitle>
          Login successfully!
        </Alert>

        <Background />

        <Box className="login__form--container">
          <form action="" onKeyDown={handleKeyDown}>
            <h1 className="title">SIGN IN</h1>

            <Box className="control">
              <label className="label" htmlFor="email">
                Email
              </label>
              <input
                type="text"
                name="email"
                id="email"
                className="input"
                placeholder="example@gmail.com"
                autoFocus
                onChange={handleEmailChange}
              />
            </Box>

            <Box className="control control-pass">
              <label className="label" htmlFor="password">
                Password
              </label>
              <input
                type={showPassword ? "text" : "password"}
                name="password"
                id="password"
                className="input"
                placeholder="Please enter your password"
                onChange={handlePassChange}
              />
              <div className="icon" onClick={handleShowPassword}>
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </div>
            </Box>

            <Box className="container__form--remember-forgot">
              <Box className="remember-me">
                <input type="checkbox" name="remember" id="remember" />
                <label htmlFor="remember">Remember Me</label>
              </Box>
              <Box className="forgot-pass">
                <Typography className="forgot-pass__nav" onClick={navToForgot}>
                  Forgot Password ?
                </Typography>
              </Box>
            </Box>

            <Box className="btn--login" sx={{ display: "flex" }}>
              <Button
                onClick={handleLogin}
                sx={{
                  margin: "auto",
                  width: "65%",
                  backgroundColor: themeColors.btnPrimary,
                  color: themeColors.white,
                  letterSpacing: "5px",
                  borderRadius: "8px",
                  "&:hover": {
                    backgroundColor: themeColors.thirdary,
                  },
                }}
              >
                LOGIN
              </Button>
            </Box>

            <Box className="register">
              <Typography className="register__text">
                Do you have an account ?
                <span className="register__nav" onClick={navToRegister}>
                  Sign up here
                </span>
              </Typography>
            </Box>

            <Another />
          </form>
        </Box>
      </Box>
    </BoxContainer>
  );
}
