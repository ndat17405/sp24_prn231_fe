import axios from "axios";
import { ApiUrl } from "../../services/ApiUrl";
import axiosUser from "../../utils/axiosUser";
import secureLocalStorage from "react-secure-storage";

export const login = async (data: any) => {
  try {
    const response = await axiosUser.post(`${ApiUrl.LOGIN}`, data);
    if (response.data.statusCode === 200) {
      localStorage.setItem("token", response.data.token);
      secureLocalStorage.setItem("accountID", response.data.data?.id);
      localStorage.setItem("role", response.data.data?.role);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const register = async (data: any) => {
  try {
    const response = await axiosUser.post(`${ApiUrl.REGISTER}`, data);

    if (response.data.statusCode === 200) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const sendEmail = async (data: any) => {
  try {
    let formData = new FormData();

    formData.append("toEmail", data);

    const headers = {
      "Content-Type": "multipart/form-data",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With",
    };

    const response = await axios.post(
      "https://eposhdeploy-production.up.railway.app/api/v1/account/sendMail",
      formData,
      {
        headers,
      }
    );

    if (response.data.statusCode === 200) {
      secureLocalStorage.setItem("otp-code", response.data.data);
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const updateNewPass = async (data: any) => {
  try {
    const response = await axiosUser.post(`${ApiUrl.UPDATE_NEW_PASS}`, data);

    if (response.data.statusCode === 200) {
      secureLocalStorage.removeItem("email");
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

export const logout = async (id: any) => {
  try {
    const response = await axiosUser.post(`${ApiUrl.LOGOUT}?id=${id}`);
    if (response.data.statusCode === 200) {
      localStorage.removeItem("token");
      secureLocalStorage.removeItem("accountID");
      localStorage.removeItem("role");
      return true;
    } else {
      return false;
    }
  } catch (error) {
    console.log(error);
  }
};
